package rowk.lynx.sales.bundle.provider.agent;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.util.WeakHashMap;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.lynx.sales.bundle.provider.agent.ProviderAgent;
import com.lynx.sales.bundle.provider.agent.exception.AgentConfigException;
import com.lynx.sales.bundle.provider.agent.sp.ContentDelegate;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class KContentDelegate extends ContentDelegate{
	
	private static WeakHashMap<String, WeakReference< ProviderAgent > > agents;
	
	private AgentConfig config;
	
	Context context;
		
	
	public KContentDelegate(){
		agents = new WeakHashMap<String, WeakReference< ProviderAgent > >();
	}
		
	public KContentDelegate( Context context ){
		this();
		this.context = context;
		
		config = AgentLoader.loadConfig(context, AgentConfig.ASSET_CONFIG_FILE );
	}

	
	private ProviderAgent createIfAbsent( String tag ){	
		
		if( null == config )
			return null;
		
		ProviderAgent agent = null;
		boolean needCreate = true;
		
		String agentClass = config.inferAgentByTag( tag );
		
		if( agents.containsKey(agentClass) ){
			WeakReference< ProviderAgent > refProviderAgent = agents.get(agentClass);
			if( null != refProviderAgent.get() ){
				agent = refProviderAgent.get();
				needCreate = false;
			}
		}
		
		if( needCreate ){
			agent = createProviderAgent( agentClass );
			agents.put( tag, new WeakReference< ProviderAgent >( agent ) );
		}
		return agent;	
	}
	
	
	
	private ProviderAgent createProviderAgent( String agentClass ) {
		
		if( null == config || TextUtils.isEmpty(agentClass) )
			return null;
				
			ProviderAgent agent = null;
			
			Class<?> loadedClass = null;
			try {					
				//Log.w(Common.TAG, "Loading service "+agentClass+"..." );
										
				loadedClass = Class.forName( agentClass );
			} catch (ClassNotFoundException e) {
				throw new AgentConfigException( "Agent class: "+agentClass+" Not Found",AgentConfig.class.getCanonicalName(),AgentConfig.ASSET_CONFIG_FILE);
			}
			try {				
				//Log.w(Common.TAG, "Instantiating agent "+agentClass+"..." );
				
				Constructor<?> constructor = loadedClass.getConstructor( Context.class );
				
				if( null == constructor )
					throw new AgentConfigException( "Agents class: "+agentClass+" Don't have Constructor that receive android.app.Context",AgentConfig.class.getCanonicalName(),AgentConfig.ASSET_CONFIG_FILE);
				
				agent = ( ProviderAgent ) constructor.newInstance( context );
				
			} catch (Exception ex) {
				throw new AgentConfigException( ex.toString(),AgentConfig.class.getCanonicalName(),AgentConfig.ASSET_CONFIG_FILE);	
			}
			
			if ( null == agent )				
				Log.w(Common.TAG, "Imposible to resolve agent "+agentClass+"..." );
			
		return agent;
	}	
	
	
	
	
	@Override
	public ProviderAgent lookup( String tag ) {
		return createIfAbsent(tag);
	}

}
