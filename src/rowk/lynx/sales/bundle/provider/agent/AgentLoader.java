package rowk.lynx.sales.bundle.provider.agent;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import android.content.Context;
import android.util.Log;

import com.lynx.sales.bundle.provider.agent.exception.AgentConfigException;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
class AgentLoader {
	
	private static AgentConfig config;

	public static AgentConfig loadConfig(Context cxt, String filename) {
		
		if( config != null )
			return config;
		
		Properties agents = null;
		try{
			
			Log.w(Common.TAG, "Trying to Load agents config file..." );
			
			InputStream stream = cxt.getAssets().open( filename );
			agents = new Properties();
			agents.load(stream);
			
			config = new AgentConfig();
			
			attachProperties( agents );
			
			Log.w(Common.TAG, config.toString() );
			
		}catch( Exception ex ){
			throw new AgentConfigException( "Agents configuration file is missing or  malformed: "+ex.toString(),AgentConfig.class.getCanonicalName(),AgentConfig.ASSET_CONFIG_FILE);
		}
		return config;
	}
	
	
	private static void attachProperties( Properties prop ){
		if( config == null )
			return;
		
		Enumeration< Object > keys = prop.keys();
		
		while( keys.hasMoreElements() ){	
			
			String unformedKey = keys.nextElement().toString();
			String[] keyParts = extractKeyParts( unformedKey );		
			
			if( null != keyParts ){
				for( String key : keyParts ){
					config.addAgentForTag(key, prop.getProperty( unformedKey ) );
				}
			}
		}
	}
	
	
	private static String[] extractKeyParts( String key ){		
		String[] parts =  resolvedTemplate(key).split("\\;");		
		return parts;
	}
	
	
	
	/**
	 * 
	 * @param key large key  as is placed  in agents.properties file, and  return a normalized template,removing
	 * 		  the pattern as {name: and }, and leaving only the regular expression.
	 * @return
	 */
	private static String resolvedTemplate( String key ){
		final String REGEX_SUBSTITUTION = "\\{\\w+:\\s*";
		String resolved = null;
		
		try{			
			resolved = key.replaceAll( REGEX_SUBSTITUTION, "").replaceAll("\\}", "").trim();
		}catch( Exception ex ){
			Log.w( "Exception while resolving template", ""+ex.toString() );			
			return key;
		}		 		 		
		return resolved;		 
	}
	
}



