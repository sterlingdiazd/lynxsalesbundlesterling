package rowk.lynx.sales.bundle.provider.agent;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;

/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
class AgentConfig {	
	
	static final String ASSET_CONFIG_FILE 		= "agents.properties";
	
	
	private Map< String,String > agents;
		
	
	public AgentConfig(){
		agents = new HashMap<String, String>();
	}	
	
	
	/**
	 * 
	 * @param tag refer on any tag that is composed in agent.properties file, is the form as /tag  /tag:(regex) , but instantiation
	 * 		  that is; agents maintain tag as regular expression parsed by AgentLoader, and here is ellegible the most appropiated
	 * 		  regex template
	 * @return
	 */
	public  String inferAgentByTag( String tag ){
		
		for( String keyTemplate : agents.keySet() ){
			Pattern pattern = Pattern.compile( keyTemplate );
			Matcher matcher = pattern.matcher( tag );
			
			if( matcher.matches() )
				return agents.get(keyTemplate);
		}		
		Log.w( Common.TAG,"Can't Resolve template for "+tag );
		
		return agents.get(tag);
	}
	
	public void addAgentForTag( String tag,String clasz ){
		agents.put(tag, clasz);
	}
	
	@Override
	public String toString() {
		return "Agents:{ "+agents.toString()+" }";
	}	
}
