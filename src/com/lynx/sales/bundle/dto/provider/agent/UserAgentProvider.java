package com.lynx.sales.bundle.dto.provider.agent;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.provider.SalesContract;
import com.lynx.sales.bundle.provider.SalesContract.Sales;
import com.lynx.sales.bundle.provider.agent.AbstractProviderAgent;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class UserAgentProvider extends AbstractProviderAgent{

	public static final Uri CONTENT_URI = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).build();
	
	private static final int ALL_USERS  		 = 1;
	private static final int SINGLE_USER 		 = 2;
	private static final int ALL_USER_CENTERS 	 = 3;
	
	private static UriMatcher matcher;
	
	static{
		matcher = new UriMatcher( UriMatcher.NO_MATCH );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "users",   ALL_USERS );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "users/#", SINGLE_USER ); 
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "users/#/centers", ALL_USER_CENTERS );
	}
	
	public UserAgentProvider(Context context) {		
		super(context);
	}
	

	@Override
	public int delete(Uri uri, String selection, String[] seelctionArgs) {
		int deletedId = -1;
		
		SQLiteDatabase db = Database.getWritableDatabase(context);		
				
		if( SINGLE_USER == matcher.match(uri) ){
			deletedId = db.delete( "users", "id='"+  uri.getLastPathSegment() + "'", null);
		} else if( ALL_USER_CENTERS == matcher.match(uri) ){
			deletedId = db.delete( "centers", "id_user='"+  uri.getPathSegments().get(1) + "'", null);
		}
		return deletedId;
	}

	@Override
	public String getType(Uri uri) {
		switch( matcher.match(uri) ){
		
		case ALL_USERS :
			Log.e("*******************", "Resolving ... vnd.android.cursor.dir/vnd.com.lynx.sales.users" );
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.users";
		case SINGLE_USER:
			Log.e("*******************", "Resolving ... vnd.android.cursor.item/vnd.com.lynx.sales.users" );
			return "vnd.android.cursor.item/vnd.com.lynx.sales.users";
		case ALL_USER_CENTERS:
			Log.e("*******************", "Resolving ... vnd.android.cursor.dir/vnd.com.lynx.sales.users.centers" );
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.users.centers";			
		}
		throw new UnsupportedOperationException( "Not supported URI" );
	}

	
	
	@Override
	public Uri insert(Uri uri, ContentValues values) {		
		SQLiteDatabase db = Database.getWritableDatabase( getContext() );

		String table = "";
		if( matcher.match(uri) == ALL_USERS )
			table = "users";
		else if( matcher.match(uri) == ALL_USER_CENTERS )
			table = "centers";
		
		long id = db.insert( table, "name", values);
		if( id > -1 ){			
			Uri insertedId = ContentUris.withAppendedId(CONTENT_URI, id);			
			
			return insertedId;
		}
		return null;	
	}


	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArguments, String sortOrder) {
				
		//Log.e( "User Provider: *******", uri.toString() );
		
		String table = "";
		
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder(); 
		int selectedOption = matcher.match(uri);
		switch( selectedOption ){
		case SINGLE_USER:
				table = "users";
				builder.appendWhere( "id='"+uri.getLastPathSegment()+"'" );
				break;
		case ALL_USER_CENTERS:
				table = "centers";				
				builder.appendWhere( "id_user='"+uri.getPathSegments().get(1)+"'" );
				break;
			default:
				break;
		}
		
		builder.setTables( table );
		return builder.query( Database.getReadableDatabase(getContext()), projection, selection, selectionArguments, null, null, sortOrder);
	}
	
	
	
	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] seelctionArgs) {
		return 0;
	}


	@Override
	public boolean resolve(Uri uri){ 
		int selectedOption = matcher.match(uri);
		return -1 < selectedOption;
	}


	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments, String sortOrder,
			int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

}
