package com.lynx.sales.bundle.dto.provider.agent;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.provider.SalesContract;
import com.lynx.sales.bundle.provider.SalesContract.Sales;
import com.lynx.sales.bundle.provider.agent.AbstractProviderAgent;

public class ScheduledQueueAgentProvider extends AbstractProviderAgent{

	
	public static final Uri CONTENT_URI = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).build();
	
	private static final int ALL_SCHEDULES  		= 1;
	private static final int SINGLE_SCHEDULES		= 2;
	private static final int SCHEDULE_BY_STATUS		= 3;
	
	private static UriMatcher matcher;
	
	static{
		matcher = new UriMatcher( UriMatcher.NO_MATCH );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "schedules",   ALL_SCHEDULES );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "schedules/#", SINGLE_SCHEDULES );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "schedules/by_status/#", SCHEDULE_BY_STATUS );
	}
	
	public ScheduledQueueAgentProvider(Context context) {		
		super(context);
	}
	

	@Override
	public String getType(Uri uri) {
		switch( matcher.match(uri) ){
		
		case ALL_SCHEDULES :
			Log.e("*******************", "Resolving ... vnd.android.cursor.dir/vnd.com.lynx.sales.schedules" );
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.schedules";
		case SINGLE_SCHEDULES:
			Log.e("*******************", "Resolving ... vnd.android.cursor.item/vnd.com.lynx.sales.schedules" );
			return "vnd.android.cursor.item/vnd.com.lynx.sales.schedules";	
		case SCHEDULE_BY_STATUS:
			Log.e("*******************", "Resolving ... vnd.android.cursor.item/vnd.com.lynx.sales.schedules" );
			return "vnd.android.cursor.item/vnd.com.lynx.sales.schedules.by_status";		
		}
		throw new UnsupportedOperationException( "Not supported URI" );
	}

	
	
	
	@Override
	public int delete(Uri uri, String selection, String[] seelctionArgs) {	
		SQLiteDatabase db = Database.getWritableDatabase( getContext() );

		String table = "pending_jobs_queue";
		
		 int rowsDeleted = -1; 
		switch( matcher.match(uri) ){
		case ALL_SCHEDULES:
			 rowsDeleted = db.delete(table, null, null);
			break;
		case SINGLE_SCHEDULES:
			rowsDeleted = db.delete(table, "id="+uri.getLastPathSegment(), null );
			break;
			default:
				break;
		}		
		return rowsDeleted;			
	}
	
	
	@Override
	public Uri insert(Uri uri, ContentValues values) {		
		SQLiteDatabase db = Database.getWritableDatabase( getContext() );

		String table = "pending_jobs_queue";
		
		long id = db.insert( table, null, values);
		if( id > -1 ){			
			Uri insertedId = ContentUris.withAppendedId(CONTENT_URI, id);			
			
			return insertedId;
		}
		return null;	
	}


	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArguments, String sortOrder) {
				
		Log.e( "Products Provider: *******", uri.toString() );
		
		String table = "pending_jobs_queue";
		
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder(); 
		switch( matcher.match(uri) ){
		case ALL_SCHEDULES:				
				break;
		case SINGLE_SCHEDULES:
			builder.appendWhere( "id="+uri.getLastPathSegment() );
			break;
		case SCHEDULE_BY_STATUS:
			builder.appendWhere( "status='"+uri.getLastPathSegment()+"'" );
			break;			
			default:
				break;
		}
		
		builder.setTables( table );
		return builder.query( Database.getReadableDatabase(getContext()), projection, selection, selectionArguments, null, null, sortOrder);
	}
	
	
	
	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] seelctionArgs) {
		SQLiteDatabase db = Database.getWritableDatabase( getContext() );

		String table = "pending_jobs_queue";
		
		int rowsUpdated = -1; 
		switch( matcher.match(uri) ){
		case SINGLE_SCHEDULES:
			rowsUpdated = db.update(table, values, "id="+uri.getLastPathSegment(), null );
			break;
			default:
				break;
		}		
		return rowsUpdated;	
	}


	@Override
	public boolean resolve(Uri uri){ 		
		return -1 < matcher.match(uri);
	}


	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments, String sortOrder,
			int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
