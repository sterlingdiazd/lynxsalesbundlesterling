package com.lynx.sales.bundle.dto.provider.agent;

import java.io.Serializable;

import com.lynx.sales.bundle.dto.model.ScheduleDataModel;


/**
 * <p>
 * Since the state of the application must be preserved no matter
 * what is the state  of the phone, jobs need to be scheduled for
 * the synchronization process. Jobs could be a {@link Type#BILL} by default 
 * or defined new {@link Type} and is attached to specific <code>state</code>,
 * possible status are:
 * <ul>
 * <li>{@link Status#PENDING} indicating that the jobs is considered for the next time the synchronization process take place.</li>
 * <li>{@link Status#PROCCESED} indicate that the synchronization process take that jobs and executed.</li> 
 * <li>{@link Status#REJECTED} the synchronization process fail to perform correctly that job, and is considered to rescheduled.</li>
 * </ul>
 * </p>
 * <p>
 * Schedules jobs content provider is registered in <code>agents.properties</code>
 * configuration file, who is the encourage to handle all operation on schedules job. 
 * Each job is registered as needed, and retrieved in synchronization process where is 
 * Processed all pending jobs that have state as {@link Status#PENDING} 
 * </p>
 * <p>
 * Scheduled Jobs content provider respond to following URIs
 * <ul>
 * <li><i>/schedules</i> to act on all schedules jobs.</li>
 * <li><i>/schedules/{id\:(\\d+)}</i> to act on specific schedules jobs.</li>
 * <li><i>/schedules/by_status/{status\:(\\w+)}</i> to act on specific schedules jobs identified by it status.</li>
 * </ul>
 * </p>
 * <p>
 * Jobs are handled by it model {@link ScheduleDataModel} that know how to manage 
 * plain representation of jobs, thats means you only need to create {@link ScheduleJob} 
 * and pass to {@link ScheduleDataModel} on specifics methods.
 * </p>
 * 
 * <blockquote>
 * <pre>
 *		ScheduleJob job = ScheduleJob.builder()
 *				.setDate( new Date().toString() )
 *				.setQuery( strUrl )
 *				.setType( type )
 *				.setResponsible( responsible )
 *				.setStatus( ScheduleJob.Status.PENDING )
 *				.build();
 *		
 *		ScheduleDataModel.saveScheduleJob( context, job); 		
 * </pre>
 * </blockquote>
 * 
 * 
 * @see Type
 * @see Status
 * @see Builder
 * @see ScheduleDataModel
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public class ScheduleJob implements Serializable {
	
	public enum Type{
		BILL;
	};
		
	public enum Status{
		PENDING,PROCCESED,REJECTED, WARNING
	};
	
	private int id;
	private String responsible;
	private Type type;
	private String query;
	private String date;
	private Status status;
	private String response;
	private String guardLimit;
	private String networkMode;
	
	
	private ScheduleJob(){
	}
	
	private ScheduleJob( Builder builder ){
		this.id  = builder.id;
		this.response = builder.response;
		this.responsible = builder.responsible;
		this.type = builder.type;
		this.query = builder.query;
		this.status = builder.status;
		this.date = builder.date;
		this.guardLimit = builder.guardLimit;
		this.networkMode = builder.networkMode;
	}
	
	public static Builder builder(){
		return new Builder();
	}
	
	
	public int getId() {
		return id;
	}

	public String getResponsible() {
		return responsible;
	}

	public Type getType() {
		return type;
	}

	public String getQuery() {
		return query;
	}

	public String getDate() {
		return date;
	}

	public Status getStatus() {
		return status;
	}

	public String getResponse() {
		return response;
	}

	public String getGuardLimit() {
		return guardLimit;
	}

	public String getNetworkMode() {
		return networkMode;
	}
	/**
	 * Class That create Schedule jobs as assembly parts 
	 * 
	 * @author Jansel
	 *
	 */
	public static class Builder{
		int id;
		String responsible;
		Type type;
		String query;
		String date;
		Status status;
		String response;
		String guardLimit;
		String networkMode;

		public ScheduleJob build(){
			return new ScheduleJob( this );
		}
		
		public Builder setId(int id) {
			this.id = id;
			return this;
		}

		public Builder setResponsible(String responsible) {
			this.responsible = responsible;
			return this;
		}

		public Builder setType(Type type) {
			this.type = type;
			return this;
		}

		public Builder setQuery(String query) {
			this.query = query;
			return this;
		}

		public Builder setDate(String date) {
			this.date = date;
			return this;
		}

		public Builder setStatus(Status status) {
			this.status = status;
			return this;
		}

		public Builder setResponse(String response) {
			this.response = response;
			return this;
		}
		
		public  Builder setGuardLimit(String guardLimit) {
			this.guardLimit = guardLimit;
			return this;
		}
		
		public  Builder setNetworkMode(String networkMode) {
			this.networkMode = networkMode;
			return this;
		}

		
	}
}
