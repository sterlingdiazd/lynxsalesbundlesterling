package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.*;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.entities.Archive;
import com.lynx.sales.bundle.entities.CenterEntity;
import com.lynx.sales.bundle.entities.DocTypeEntity;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynx.sales.bundle.http.request.ResponseBinder;
import com.lynx.sales.bundle.http.request.ResponseStub;
import com.lynx.sales.bundle.provider.SalesContract;

public final class UserDataModel {
	
	public static void saveUser( Context cxt,String strUser )
	{
		UserEntity user = ResponseBinder.bindUser( new ResponseStub(strUser) );
		UserEntity savedUser = getUser(cxt, user.getId());

		if(savedUser.getId() == null || !savedUser.getId().equalsIgnoreCase(user.getId()))
		{
			save( cxt, user);
		} 
		else 
		{
			ContentResolver resolver = cxt.getContentResolver();
			
			Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).appendPath( user.getId() ).build();
			resolver.delete( uri, null, null);
			
			Uri uriCenter = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).appendPath( user.getId() ).appendPath( "centers" ).build();
			resolver.delete( uriCenter, null, null);
			
			
			
			save( cxt, user);
		}
		
			
	}
	
	public static void save( Context cxt, UserEntity user)
	{
		ContentResolver resolver = cxt.getContentResolver();
		
		ContentValues values = new ContentValues();
		values.put( ID, user.getId() );
		values.put( USUARIO, user.getUser() );
		values.put( NAME, user.getName() );
		values.put( SALES_ORG, user.getSalesOrganization() );
		values.put( TEL, user.getCel() );
		values.put( CEL, user.getTel());
		
		resolver.insert( SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).build(), values);
		
		ArrayList<DocTypeEntity> docTypes = (ArrayList<DocTypeEntity>) user.getDocTypes();
		
		if(docTypes != null)
		{
			SQLiteDatabase db = Database.getWritableDatabase( cxt );
			
			db.execSQL("delete from doctypes");
			
			for(int x = 0; x < docTypes.size(); x++)
			{
				ContentValues docValues = new ContentValues();
				DocTypeEntity doc = docTypes.get(x);
				docValues.put( DOC_TYPE, doc.getDocType() );
				docValues.put( DESCRIPTION, doc.getDescription() );
				long id = db.insert( "doctypes", null, docValues);	
				if(id != -1)
				{
					Log.e("e", "exito");
				}
				
				
			}
		}
		
		if( null != user.getCenters() ){
			ContentValues[] centerValues = new ContentValues[ user.getCenters().size() ];
			for( int i=0; user.getCenters().size() >i; ++i ){
				
				CenterEntity center = user.getCenters().get(i);
				
				centerValues[ i ] = new ContentValues();
				//centerValues[ i ].put( ID, 0 );
				centerValues[ i ].put( ID_USER, user.getId() );
				centerValues[ i ].put( NAME	 , center.getName() );
				centerValues[ i ].put( DESCRIPTION	 , center.getDescription() );
				centerValues[ i ].put( STORE  , center.getStore() );
				centerValues[ i ].put( DOC_TYPE, center.getDocType() );
			}
			Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).appendPath( user.getId() ).appendPath( "centers" ).build();
			resolver.bulkInsert( uri , centerValues);
		}
	}
	
	public static void saveUser( Context cxt, UserEntity user  )
	{
		if(user.getId() != null)
		{
			ContentResolver resolver = cxt.getContentResolver();
			
			ContentValues values = new ContentValues();
			values.put( ID, user.getId() );
			values.put( USUARIO, (user.getUser() != null) ? user.getUser() : ""  );
			values.put( NAME, user.getName() );
			values.put( SALES_ORG, user.getSalesOrganization() );
			values.put( CEL, user.getCel() );
			values.put( TEL, user.getTel());
			
			resolver.insert( SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).build(), values);
			
			ArrayList<DocTypeEntity> docTypes = (ArrayList<DocTypeEntity>) user.getDocTypes();
			String table = "doctypes";
			
			if(docTypes != null)
			{
				SQLiteDatabase db = Database.getWritableDatabase( cxt );
				
				for(int x = 0; x < docTypes.size(); x++)
				{
					ContentValues docValues = new ContentValues();
					DocTypeEntity doc = docTypes.get(x);
					docValues.put( DOC_TYPE, doc.getDocType() );
					docValues.put( DESCRIPTION, doc.getDescription() );
					long id = db.insert( table, null, docValues);	
					if(id != -1)
					{
						Log.e("e", "exito");
					}
					db.close();
				}
			}
			
			if( null != user.getCenters() ){
				ContentValues[] centerValues = new ContentValues[ user.getCenters().size() ];
				for( int i=0; user.getCenters().size() >i; ++i ){
					
					CenterEntity center = user.getCenters().get(i);
					
					centerValues[ i ] = new ContentValues();
					//centerValues[ i ].put( ID, 0 );
					centerValues[ i ].put( ID_USER, user.getId() );
					centerValues[ i ].put( NAME	 , center.getName() );
					centerValues[ i ].put( STORE  , center.getStore() );
					centerValues[ i ].put( DOC_TYPE, center.getDocType() );
				}
				Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).appendPath( user.getId() ).appendPath( "centers" ).build();
				resolver.bulkInsert( uri , centerValues);
			}
		}
		
			
	}
	
	public static final UserEntity getUser(Context context, String userId)
	{
		UserEntity user = new UserEntity();
		
		ContentResolver resolver = context.getContentResolver();
		
		String[] projection = {ID,NAME,SALES_ORG, CEL, TEL};
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).appendPath( userId ).build();
		Cursor cursor = resolver.query(uri, projection, null, null, null );

		
		if( cursor.moveToFirst() ){
			
			user.setId( cursor.getString( cursor.getColumnIndexOrThrow( ID ) ) );
			user.setName( cursor.getString( cursor.getColumnIndexOrThrow( NAME ) ) );
			user.setSalesOrganization( cursor.getString( cursor.getColumnIndexOrThrow( SALES_ORG ) ) );
			
			
			SQLiteDatabase db = Database.getReadableDatabase(context);
	        Cursor cursorDocType = db.query("doctypes", new String[]{"doc_type", "description"}, null , null, null, null, null);
	        List< DocTypeEntity > docTypes = new ArrayList<DocTypeEntity>();
	        while(cursorDocType.moveToNext())
	        {
	        	String doc = cursorDocType.getString( cursorDocType.getColumnIndexOrThrow( "doc_type"));
	        	String des = cursorDocType.getString( cursorDocType.getColumnIndexOrThrow( DESCRIPTION) );
	        	docTypes.add( new DocTypeEntity( doc, des  ) );
	        }
	        
			user.setDocTypes(docTypes);
			user.setCel(cursor.getString( cursor.getColumnIndexOrThrow( CEL) ) );
			user.setTel(cursor.getString( cursor.getColumnIndexOrThrow( TEL) ));
			cursor.close();
		}
		return user;
	}
	/*
	public static final UserEntity getUser(Context context)
	{
		UserEntity user = new UserEntity();
		
		ContentResolver resolver = context.getContentResolver();
		
		String[] projection = {ID,NAME,SALES_ORG,DOC_TYPE, CEL, TEL};
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).build();
		
		Cursor cursor = resolver.query(uri, projection, null, null, null );
		if( cursor.moveToFirst() ){
			
			user.setId( cursor.getString( cursor.getColumnIndexOrThrow( ID ) ) );
			user.setName( cursor.getString( cursor.getColumnIndexOrThrow( NAME ) ) );
			user.setSalesOrganization( cursor.getString( cursor.getColumnIndexOrThrow( SALES_ORG ) ) );
			user.setDocType(cursor.getString( cursor.getColumnIndexOrThrow( DOC_TYPE) ) );	
			user.setCel(cursor.getString( cursor.getColumnIndexOrThrow( CEL) ) );
			user.setTel(cursor.getString( cursor.getColumnIndexOrThrow( TEL) ));
			cursor.close();
		}
		return user;
	}
	*/
}
