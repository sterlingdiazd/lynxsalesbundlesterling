package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.ADDRESS;
import static com.lynx.sales.bundle.dto.model.Constants.FAX;
import static com.lynx.sales.bundle.dto.model.Constants.ID;
import static com.lynx.sales.bundle.dto.model.Constants.NAME;
import static com.lynx.sales.bundle.dto.model.Constants.PHONE;
import static com.lynx.sales.bundle.dto.model.Constants.RNC;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.db.SqliteOpenHelper;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.entities.Company;
import com.lynx.sales.bundle.http.request.ResponseBinder;
import com.lynx.sales.bundle.provider.SalesContract;

/**
 * 
 * @see ScheduleJob
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public final class CompanyDataModel {

	public static final Company getCompany( Context context ){
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "company" ).build();
		return getCompanyByUri(context, uri);
	}

	public static void saveCompany( Context cxt,String jsonResponse  )
	{
		Company company = ResponseBinder.bindCompany( jsonResponse );
		
		ContentResolver resolver = cxt.getContentResolver();
		
		ContentValues values = new ContentValues();
		values.put( ID, company.getId());
		values.put( NAME, company.getName());
		values.put( ADDRESS, company.getAddress());
		values.put( PHONE, company.getPhone());
		values.put( RNC, company.getRnc());
		values.put( FAX, company.getFax());
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "company" ).appendPath( company.getId() ).build();
		Company savedCompany = CompanyDataModel.getCompanyByUri(cxt, uri);
		if(savedCompany != null)
		{
			CompanyDataModel.deleteCompany(cxt, savedCompany.getId());
		}
		resolver.insert( SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "company" ).build(), values);		
	}
	
	public static void saveCompany( Context cxt,  Company company )
	{		
		ContentResolver resolver = cxt.getContentResolver();
		
		ContentValues values = new ContentValues();
		values.put( ID, company.getId());
		values.put( NAME, company.getName());
		values.put( ADDRESS, company.getAddress());
		values.put( PHONE, company.getPhone());
		values.put( RNC, company.getRnc());
		values.put( FAX, company.getFax());
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "company" ).appendPath( company.getId() ).build();
		Company savedCompany = CompanyDataModel.getCompanyByUri(cxt, uri);
		if(savedCompany != null)
		{
			CompanyDataModel.deleteCompany(cxt, savedCompany.getId());
		}
		resolver.insert( SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "company" ).build(), values);		
	}
	
	public static final void deleteCompany( Context context, String companyName )
	{
		ContentResolver resolver = context.getContentResolver(); 
		resolver.delete(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "company" ).appendPath( companyName ).build(),  null, null);
	}
	
	public static final Company getCompanyByUri( Context context,Uri uri )
	{
		ContentResolver resolver = context.getContentResolver();
		
		String[] projection = { ID, NAME, ADDRESS, PHONE, RNC, FAX};
		Cursor cursor = resolver.query(uri, projection, null, null, null);
		
		Company company = null;
		
		if( cursor.moveToFirst() )
		{
			do
			{	String id = cursor.getString( cursor.getColumnIndexOrThrow( ID ) );
				String name = cursor.getString( cursor.getColumnIndexOrThrow( NAME ) );
				String address = cursor.getString( cursor.getColumnIndexOrThrow( ADDRESS) );
				String phone = cursor.getString( cursor.getColumnIndexOrThrow( PHONE ) );
				String rnc = cursor.getString( cursor.getColumnIndexOrThrow( RNC ) );
				String fax = cursor.getString( cursor.getColumnIndexOrThrow( FAX ) );
				
				company = new Company( id, name, address, phone, rnc, fax);
				
			}while( cursor.moveToNext() );		
		}
		cursor.close();
		
		return company;
	}
	
	public static void onUpgradeDatabase( Context context ){
		SqliteOpenHelper helper = new SqliteOpenHelper( context, null );
		helper.onUpgrade( Database.getWritableDatabase( context ),1,0 );
	}
}
