package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.ID;
import static com.lynx.sales.bundle.dto.model.Constants.JOB_TYPE;
import static com.lynx.sales.bundle.dto.model.Constants.QUERY;
import static com.lynx.sales.bundle.dto.model.Constants.RESPONSE;
import static com.lynx.sales.bundle.dto.model.Constants.RESPONSIBLE;
import static com.lynx.sales.bundle.dto.model.Constants.SCHEDULED_DATE;
import static com.lynx.sales.bundle.dto.model.Constants.STATUS;
import static com.lynx.sales.bundle.dto.model.Constants.GUARD_LIMIT;
import static com.lynx.sales.bundle.dto.model.Constants.NETWORK_MODE;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.db.SqliteOpenHelper;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.provider.SalesContract;

/**
 * 
 * @see ScheduleJob
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public final class ScheduleDataModel {

	public static final int saveScheduleJob( Context context, ScheduleJob job ){
		Uri uriInserted = null;
		if( null != job ){
			ContentResolver resolver = context.getContentResolver();			
			ContentValues values = new ContentValues();		
						
			Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).build();
			values.put( RESPONSIBLE, job.getResponsible() );
			values.put( JOB_TYPE, job.getType().toString() );
			values.put( SCHEDULED_DATE, job.getDate() );
			values.put( STATUS, job.getStatus().toString() );
			values.put( RESPONSE, "" );
			values.put( QUERY, job.getQuery() );
			values.put( GUARD_LIMIT, job.getGuardLimit() );
			values.put( NETWORK_MODE, job.getNetworkMode() );
			
			uriInserted = resolver.insert(uri, values);
			
		}
		return Integer.valueOf( uriInserted.getLastPathSegment() );
	}
	
	public static final void saveScheduleJobs( Context context, List< ScheduleJob > jobs )
	{
		if( null != jobs )
		{
			ContentResolver resolver = context.getContentResolver();			
			ContentValues[] values = new ContentValues[jobs.size()];
			
			for (int i = 0; jobs.size() > i; ++i) 
			{
				ScheduleJob job = jobs.get(i);
				values[i] = new ContentValues();
				
				values[i].put( RESPONSIBLE, job.getResponsible() );
				values[i].put( JOB_TYPE, job.getType().toString() );
				values[i].put( SCHEDULED_DATE, job.getDate() );
				values[i].put( STATUS, job.getStatus().toString() );
				values[i].put( RESPONSE, "" );
				values[i].put( QUERY, job.getQuery() );
				values[i].put( GUARD_LIMIT, job.getGuardLimit() );
				values[i].put( NETWORK_MODE, job.getNetworkMode() );
			}
			
			Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).build();
			resolver.bulkInsert(uri, values);
		}
	}
	
	public static final void deleteScheduleJob( Context context, int jobId ){		
		ContentResolver resolver = context.getContentResolver();
		resolver.delete(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).appendPath( ""+jobId ).build(),  null, null);
	}
	
	public static final void registerResponseForSchedule( Context context, int jobId,String status,String response ){
		ContentResolver resolver = context.getContentResolver();
		ContentValues values = new ContentValues();
		values.put( RESPONSE, response );
		values.put( STATUS, status );
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).appendPath( ""+jobId ).build();
		resolver.update(uri, values, null, null);
	}
	
	
	
	
	public static final List< ScheduleJob > getShedulesJobs( Context context ){
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).build();
		return getSchedulesByUri(context, uri);
	}
	
	
	
	public static final List< ScheduleJob > getShedulesJobsByStatus( Context context, ScheduleJob.Status status ){
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).appendPath("by_status").appendPath( status.toString() ).build();
		return getSchedulesByUri(context, uri);
	}
	
	
	
	private static final List< ScheduleJob > getSchedulesByUri( Context context,Uri uri ){
		ContentResolver resolver = context.getContentResolver();
		
		String[] projection = { ID,RESPONSIBLE,SCHEDULED_DATE,JOB_TYPE,QUERY,STATUS,RESPONSE, GUARD_LIMIT, NETWORK_MODE };
		Cursor cursor = resolver.query(uri, projection, null, null, null);
		
		List< ScheduleJob > jobs = new ArrayList<ScheduleJob>();
		
		if( cursor.moveToFirst() ){
			do{
				ScheduleJob job = ScheduleJob.builder()
						.setId( cursor.getInt( cursor.getColumnIndexOrThrow( ID ) ) )
						.setDate( cursor.getString( cursor.getColumnIndexOrThrow( SCHEDULED_DATE ) ) )
						.setQuery( cursor.getString( cursor.getColumnIndexOrThrow( QUERY ) ) )
						.setResponse( cursor.getString( cursor.getColumnIndexOrThrow( RESPONSE ) ) )
						.setType( ScheduleJob.Type.valueOf( cursor.getString( cursor.getColumnIndexOrThrow( JOB_TYPE ) ) ) )
						.setResponsible( cursor.getString( cursor.getColumnIndexOrThrow( RESPONSIBLE ) ) )
						.setStatus( ScheduleJob.Status.valueOf( cursor.getString( cursor.getColumnIndexOrThrow(  STATUS ) ) ))
						.setGuardLimit( cursor.getString( cursor.getColumnIndexOrThrow( GUARD_LIMIT ) ))
						.setNetworkMode( cursor.getString( cursor.getColumnIndexOrThrow( NETWORK_MODE ) ) )
						.build();
				
				jobs.add(job);
			}while( cursor.moveToNext() );		
		}
		cursor.close();
		
		return jobs;
	}
	
	
	
	
	public static void onUpgradeDatabase( Context context ){
		SqliteOpenHelper helper = new SqliteOpenHelper( context, null );
		helper.onUpgrade( Database.getWritableDatabase( context ),0,1 );
	}
}
