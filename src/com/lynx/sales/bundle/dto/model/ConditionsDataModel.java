package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.*;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.entities.ConditionEntity;
import com.lynx.sales.bundle.entities.ItineraryEntity;
import com.lynx.sales.bundle.entities.RouteEntity;
import com.lynx.sales.bundle.http.request.ResponseBinder;
import com.lynx.sales.bundle.provider.SalesContract;

public final class ConditionsDataModel {

	
	public static final void saveConditions(Context context, List<ConditionEntity> conditions ){
		
		ContentResolver resolver = context.getContentResolver();
		ContentValues[] values = null;
		if(conditions != null)
		{
			values = new ContentValues[ conditions.size() ];
			
			for(int x = 0; x < conditions.size();x++)
			{	
				
				values[ x ] = new ContentValues();
				
				ConditionEntity entity = conditions.get(x);
				
				values[ x ].put( COND_TABLE, entity.getCondTable() );
				values[ x ].put( COND_TYPE, entity.getKschl()); 
				values[ x ].put( TABLE_NAME, entity.getTableName());
				values[ x ].put( SEQUENCE, entity.getSequence());
				values[ x ].put( VKORG,entity.getVkorg());
				values[ x ].put( VTWEG,entity.getVtweg());
				values[ x ].put( SPART,entity.getSpart());
				values[ x ].put( KDGRP, entity.getKdgrp());
				values[ x ].put( BRSCH,entity.getBrsch() );
				values[ x ].put( PRODH,entity.getProdh() );
				values[ x ].put( KUNNR,entity.getKunnr() );
				values[ x ].put( MATNR,entity.getMatnr() );
				values[ x ].put( DATBI, entity.getDatbi() );
				values[ x ].put( KNUMH, entity.getKnumh() );			
				values[ x ].put( KBETR,entity.getKbetr() );
				values[ x ].put( KNRMM, entity.getKnrmm() );
				values[ x ].put( KNRNM,entity.getKnrnm() );
				values[ x ].put( KNRME,entity.getKnrme() );
				values[ x ].put( KNRZM, entity.getKnrzm() );
				values[ x ].put( KNREZ, entity.getKnrez() );
				values[ x ].put( KNRMAT, entity.getKnrmat() );
				
			}
			
		}
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( CONDITIONS ).build();
		resolver.bulkInsert(uri , values );
	}
	
	
	public static final List<ConditionEntity> getSellerConditions( String sellerId, Context context )
	{
		List<ConditionEntity> conditions = new ArrayList<ConditionEntity>();
		
		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( CONDITIONS ).build();
		String[] projection = {	COND_TABLE , COND_TYPE , TABLE_NAME , SEQUENCE ,VKORG, VTWEG ,SPART, KDGRP , BRSCH, PRODH ,KUNNR, MATNR, DATBI , KNUMH , KBETR , KNRMM, KNRNM , KNRME,KNRZM ,KNREZ, KNRMAT };
		
		Cursor cursor = resolver.query(uri, projection, null, null, null );
		
		if( cursor != null && cursor.moveToFirst() )
		{
			do
			{
				ConditionEntity condition = new ConditionEntity();
				if(condition != null)
				{
					condition.setCondTable( 	cursor.getString( cursor.getColumnIndexOrThrow( COND_TABLE)));
					condition.setKschl( 		cursor.getString( cursor.getColumnIndexOrThrow( COND_TYPE) ) );
					condition.setTableName( 		cursor.getString( cursor.getColumnIndexOrThrow( TABLE_NAME)) );
					condition.setSequence( 		cursor.getString( cursor.getColumnIndexOrThrow( SEQUENCE)) );
					condition.setVkorg( 		cursor.getString( cursor.getColumnIndexOrThrow( VKORG)) );
					condition.setVtweg( 	cursor.getString( cursor.getColumnIndexOrThrow( VTWEG)) );
					condition.setSpart( 		cursor.getString( cursor.getColumnIndexOrThrow( SPART)) );
					condition.setKdgrp( 	cursor.getString( cursor.getColumnIndexOrThrow( KDGRP)));
					condition.setBrsch( 	cursor.getString( cursor.getColumnIndexOrThrow( BRSCH)) );
					condition.setProdh(  cursor.getString( cursor.getColumnIndexOrThrow( PRODH)) );
					condition.setKunnr(  cursor.getString( cursor.getColumnIndexOrThrow( KUNNR)) );
					condition.setMatnr(  cursor.getString( cursor.getColumnIndexOrThrow( MATNR)) );
					condition.setDatbi( 	cursor.getString( cursor.getColumnIndexOrThrow( DATBI)) );	
					condition.setKnumh( 		cursor.getString( cursor.getColumnIndexOrThrow( KNUMH)) );
					condition.setKbetr( 	cursor.getString( cursor.getColumnIndexOrThrow( KBETR)) );
					condition.setKnrmm( 	cursor.getString( cursor.getColumnIndexOrThrow( KNRMM)));
					condition.setKnrnm( 	cursor.getString( cursor.getColumnIndexOrThrow( KNRNM)) );
					condition.setKnrme(  cursor.getString( cursor.getColumnIndexOrThrow( KNRME)) );
					condition.setKnrzm( 	cursor.getString( cursor.getColumnIndexOrThrow( KNRZM)) );	
					condition.setKnrez( 	cursor.getString( cursor.getColumnIndexOrThrow( KNREZ)) );	
					condition.setKnrmat( 	cursor.getString( cursor.getColumnIndexOrThrow( KNRMAT)) );	
					
					conditions.add(condition);
				}
				
			}
			while( cursor.moveToNext() );
			cursor.close();
		}
		
		
			
		return conditions;
	}
	
	public static final void deleteConditions(Context context) 
	{		
		SQLiteDatabase db = Database.getWritableDatabase( context );		
		db.execSQL("delete from " + CONDITIONS);	
	}

}
















