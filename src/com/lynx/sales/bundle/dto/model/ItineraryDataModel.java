package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.*;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.lynx.sales.bundle.entities.ItineraryEntity;
import com.lynx.sales.bundle.entities.RouteEntity;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.ResponseBinder;
import com.lynx.sales.bundle.provider.SalesContract;

public final class ItineraryDataModel {

	public static final void saveItinerary(Context context, String sellerId, String jsonResponse, PhaseHandler handler) {

		ItineraryEntity entity = ResponseBinder.bindItinerary(jsonResponse, handler);

		ContentResolver resolver = context.getContentResolver();
		ContentValues values = new ContentValues();

		if(entity != null)
		{
			ItineraryEntity itenEnt = ItineraryDataModel.getItinerary(entity.getId(), context);
			String savedItinerary = null;
			if (itenEnt.getId() != null)
				savedItinerary = itenEnt.getId();

			String newItinerary = entity.getId();

			if (savedItinerary != null) {
				if (savedItinerary.equalsIgnoreCase(newItinerary)) {
					ItineraryDataModel.deleteItineraryData(context, sellerId, savedItinerary);
				}
			}

			values.put(ID, entity.getId());
			values.put(SELLER_ID, sellerId);
			values.put(CREATION_DATE, entity.getCreationDate());
			values.put(DATE_FROM, entity.getDateFrom());
			values.put(DATE_TO, entity.getDateTo());
			values.put(DAY_OF_WEEK, entity.getDaysOfWeek());
			values.put(CREATED_BY, entity.getCreatedBy());
			values.put(MODIFIED_BY, entity.getModifiedBy());
			values.put(MODIFIED_HOUR, entity.getModifiedHour());
			values.put(DESCRIPTION, entity.getDescription());

			resolver.insert(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("itineraries").build(), values);

			if (null != entity.getRoutes()) {
				ContentValues[] aValues = new ContentValues[entity.getRoutes().size()];

				for (int i = 0; entity.getRoutes().size() > i; ++i) {
					RouteEntity route = entity.getRoutes().get(i);
					aValues[i] = new ContentValues();

					aValues[i].put(ID, route.getIdRoute());
					aValues[i].put(LAZZY_CLIENT_ID, route.getLazzyClientId());
					aValues[i].put(CLIENT_NAME, route.getLazzyClientName());
					aValues[i].put(ITINERARY_ID, entity.getId().trim());
					aValues[i].put(CREATED_BY, route.getCreatedBy());
					aValues[i].put(CREATED_DATE, route.getCreatedDate());
				}
				Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("itineraries").appendPath("routes")
						.appendPath(entity.getId()).build();
				resolver.bulkInsert(uri, aValues);
			}
		}
		

	}

	public static final void saveItinerary(Context context, String sellerId, List<ItineraryEntity> itineraries) {

		for (int x = 0; x < itineraries.size(); x++) {
			ContentResolver resolver = context.getContentResolver();
			ContentValues values = new ContentValues();

			ItineraryEntity entity = itineraries.get(x);

			values.put(ID, entity.getId());
			values.put(SELLER_ID, sellerId);
			values.put(CREATION_DATE, entity.getCreationDate());
			values.put(DATE_FROM, entity.getDateFrom());
			values.put(DATE_TO, entity.getDateTo());
			values.put(DAY_OF_WEEK, entity.getDaysOfWeek());
			values.put(CREATED_BY, entity.getCreatedBy());
			values.put(MODIFIED_BY, entity.getModifiedBy());
			values.put(MODIFIED_HOUR, entity.getModifiedHour());
			values.put(DESCRIPTION, entity.getDescription());

			resolver.insert(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("itineraries").build(), values);

			if (null != entity.getRoutes()) {
				ContentValues[] aValues = new ContentValues[entity.getRoutes().size()];

				for (int i = 0; entity.getRoutes().size() > i; ++i) {
					RouteEntity route = entity.getRoutes().get(i);
					aValues[i] = new ContentValues();

					aValues[i].put(ID, route.getId());
					aValues[i].put(LAZZY_CLIENT_ID, route.getLazzyClientId());
					aValues[i].put(CLIENT_NAME, route.getLazzyClientName());
					aValues[i].put(ITINERARY_ID, entity.getId().trim());
					aValues[i].put(CREATED_BY, route.getCreatedBy());
					aValues[i].put(CREATED_DATE, route.getCreatedDate());
				}
				Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("itineraries").appendPath("routes")
						.appendPath(entity.getId()).build();
				resolver.bulkInsert(uri, aValues);
			}
		}

	}

	public static final ItineraryEntity getItinerary(String itinId, Context context) {
		ItineraryEntity itinerary = new ItineraryEntity();

		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("itineraries").appendPath(itinId).build();
		String[] projection = { ID, SELLER_ID, CREATION_DATE, DATE_FROM, DATE_TO, DAY_OF_WEEK, CREATED_BY, MODIFIED_BY,
				MODIFIED_HOUR, DESCRIPTION };

		Cursor cursor = resolver.query(uri, projection, null, null, null);

		if (cursor.moveToFirst()) {
			itinerary.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
			itinerary.setCreationDate(cursor.getString(cursor.getColumnIndexOrThrow(CREATION_DATE)));
			itinerary.setDateFrom(cursor.getString(cursor.getColumnIndexOrThrow(DATE_FROM)));
			itinerary.setDateTo(cursor.getString(cursor.getColumnIndexOrThrow(DATE_TO)));
			itinerary.setDaysOfWeek(cursor.getString(cursor.getColumnIndexOrThrow(DAY_OF_WEEK)));
			itinerary.setCreatedBy(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_BY)));
			itinerary.setModifiedBy(cursor.getString(cursor.getColumnIndexOrThrow(MODIFIED_BY)));
			itinerary.setModifiedHour(cursor.getString(cursor.getColumnIndexOrThrow(MODIFIED_HOUR)));
			itinerary.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)));

			if (itinerary.getId() != null) {
				itinerary.setRoutes(getRoutes(context, itinerary.getId()));
			}

		}
		cursor.close();
		return itinerary;
	}

	public static final List<ItineraryEntity> getSellerItineraries(String sellerId, Context context, int start, int limit) {
		List<ItineraryEntity> itineraries = new ArrayList<ItineraryEntity>();

		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("itineraries").appendPath("seller")
				.appendPath(sellerId).build();
		String[] projection = { ID, SELLER_ID, CREATION_DATE, DATE_FROM, DATE_TO, DAY_OF_WEEK, CREATED_BY, MODIFIED_BY,
				MODIFIED_HOUR, DESCRIPTION };

		Cursor cursor = resolver.query(uri, projection, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {
			do {
				ItineraryEntity itinerary = new ItineraryEntity();
				if (itinerary != null) {
					itinerary.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
					itinerary.setCreationDate(cursor.getString(cursor.getColumnIndexOrThrow(CREATION_DATE)));
					itinerary.setDateFrom(cursor.getString(cursor.getColumnIndexOrThrow(DATE_FROM)));
					itinerary.setDateTo(cursor.getString(cursor.getColumnIndexOrThrow(DATE_TO)));
					itinerary.setDaysOfWeek(cursor.getString(cursor.getColumnIndexOrThrow(DAY_OF_WEEK)));
					itinerary.setCreatedBy(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_BY)));
					itinerary.setModifiedBy(cursor.getString(cursor.getColumnIndexOrThrow(MODIFIED_BY)));
					itinerary.setModifiedHour(cursor.getString(cursor.getColumnIndexOrThrow(MODIFIED_HOUR)));
					itinerary.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)));

					if (itinerary.getId() != null) {
						String itinID = itinerary.getId();
						List<RouteEntity> routes = getRoutes(context, itinID, start, limit);
						itinerary.setRoutes(routes);
					}
					itineraries.add(itinerary);
				}

			} while (cursor.moveToNext());
			cursor.close();
		}

		return itineraries;
	}

	public static final void deleteItineraryData(Context context, String sellerId, String itinerary_id) {
		ContentResolver resolver = context.getContentResolver();
		resolver.delete(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("itineraries").appendPath("routes")
				.appendPath("" + itinerary_id).build(), null, null);
		resolver.delete(
				SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("itineraries").appendPath("" + itinerary_id)
						.build(), null, null);
	}

	public static final List<RouteEntity> getRoutes(Context context, String itineraryId) {
		List<RouteEntity> routes = new ArrayList<RouteEntity>();

		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon()
				.appendPath("itineraries")
				.appendPath("routes")
				.appendPath(itineraryId)
				.build();

		String[] projection = { ID, ITINERARY_ID, LAZZY_CLIENT_ID, CLIENT_NAME, CREATED_BY, CREATED_DATE };
		Cursor cursor = resolver.query(uri, projection, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					RouteEntity route = new RouteEntity();
					route.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
					route.setLazzyClientId(cursor.getString(cursor.getColumnIndexOrThrow(LAZZY_CLIENT_ID)));
					route.setLazzyClientName(cursor.getString(cursor.getColumnIndexOrThrow(CLIENT_NAME)));
					route.setCreatedBy(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_BY)));
					route.setCreatedDate(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_DATE)));
					
					routes.add(route);
				} while (cursor.moveToNext());

				cursor.close();
			}
		}

		return routes;
	}
	
	public static final List<RouteEntity> getRoutes(Context context, String itineraryId, int start, int limit) {
		List<RouteEntity> routes = new ArrayList<RouteEntity>();

		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon()
				.appendPath("itineraries")
				.appendPath("routes")
				.appendPath(itineraryId)
				.appendPath(String.valueOf(limit))
				.appendPath(String.valueOf(start))
				.build();

		String[] projection = { ID, ITINERARY_ID, LAZZY_CLIENT_ID, CLIENT_NAME, CREATED_BY, CREATED_DATE };
		Cursor cursor = resolver.query(uri, projection, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					RouteEntity route = new RouteEntity();
					route.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
					route.setLazzyClientId(cursor.getString(cursor.getColumnIndexOrThrow(LAZZY_CLIENT_ID)));
					route.setLazzyClientName(cursor.getString(cursor.getColumnIndexOrThrow(CLIENT_NAME)));
					route.setCreatedBy(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_BY)));
					route.setCreatedDate(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_DATE)));
					
					routes.add(route);
				} while (cursor.moveToNext());

				cursor.close();
			}
		}

		return routes;
	}
	
	public static final List<RouteEntity> getRoutesByIdOrName(Context context, String itineraryId, String clientIdOrName) {
		List<RouteEntity> routes = new ArrayList<RouteEntity>();

		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon()
				.appendPath("itineraries")
				.appendPath("routes")
				.appendPath(itineraryId)
				.appendPath(clientIdOrName)
				.build();

		String[] projection = { ID, ITINERARY_ID, LAZZY_CLIENT_ID, CLIENT_NAME, CREATED_BY, CREATED_DATE };
		Cursor cursor = resolver.query(uri, projection, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					RouteEntity route = new RouteEntity();
					route.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
					route.setLazzyClientId(cursor.getString(cursor.getColumnIndexOrThrow(LAZZY_CLIENT_ID)));
					route.setLazzyClientName(cursor.getString(cursor.getColumnIndexOrThrow(CLIENT_NAME)));
					route.setCreatedBy(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_BY)));
					route.setCreatedDate(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_DATE)));
					
					routes.add(route);
				} while (cursor.moveToNext());

				cursor.close();
			}
		}

		return routes;
	}

}