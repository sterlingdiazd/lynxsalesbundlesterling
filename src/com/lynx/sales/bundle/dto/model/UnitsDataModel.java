package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.*;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.entities.ConditionEntity;
import com.lynx.sales.bundle.entities.ItineraryEntity;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.entities.RouteEntity;
import com.lynx.sales.bundle.entities.UnitEntity;
import com.lynx.sales.bundle.http.request.ResponseBinder;
import com.lynx.sales.bundle.provider.SalesContract;

public final class UnitsDataModel {

	public static final List<UnitEntity> getUnitsByProduct(Context context, String material) 
	{
		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath(UNITS).appendPath(material).build();

		String[] projection = { ID, MATNR, MEINH };

		List<UnitEntity> units = new ArrayList<UnitEntity>();
		Cursor cursor = null;
		try
		{
			cursor = resolver.query(uri, projection, null, null, null);
			if (cursor.moveToNext()) {
				do 
				{
					UnitEntity unit = new UnitEntity();

					unit.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
					unit.setMatnr(cursor.getString(cursor.getColumnIndexOrThrow(MATNR)));
					unit.setMeinh(cursor.getString(cursor.getColumnIndexOrThrow(MEINH)));
					units.add(unit);
				} 
				while (cursor.moveToNext());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		} 
		finally 
		{
			if (cursor != null) 
			{
				cursor.close();
			}
		}
		
		return units;
	}

	
	public static final void deleteUnits(Context context) 
	{		
		SQLiteDatabase db = Database.getWritableDatabase( context );		
		db.execSQL("delete from " + UNITS);	
	}

}
















