package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.*;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.db.SqliteOpenHelper;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.entities.Archive;
import com.lynx.sales.bundle.provider.SalesContract;

/**
 * 
 * @see ScheduleJob
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public final class ArchiveDataModel {
	
	

	public static final void saveArchives( Context context, List<Archive> archives )
	{
		if( null != archives )
		{
			ContentResolver resolver = context.getContentResolver();			
			ContentValues[] values = new ContentValues[archives.size()];
			
			for (int i = 0; archives.size() > i; ++i) 
			{
				Archive archive = archives.get(i);
				values[i] = new ContentValues();
				
				values[i].put( ARCHIVE_TYPE, 		archive.getArchiveType() );
				values[i].put( OWNER, 				archive.getOwner() );
				values[i].put( STATE, 				"");
				values[i].put( DATE, 				archive.getDate() );
				values[i].put( OWNER_NAME, 			archive.getName());				
			}
			
			Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "archives" ).build();
			resolver.bulkInsert(uri, values);
		}
	}
	
	/*
	
	public static final void deleteScheduleJob( Context context, int jobId ){		
		ContentResolver resolver = context.getContentResolver();
		resolver.delete(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).appendPath( ""+jobId ).build(),  null, null);
	}
	
	
	
	public static final void registerResponseForSchedule( Context context, int jobId,String status,String response ){
		ContentResolver resolver = context.getContentResolver();
		ContentValues values = new ContentValues();
		values.put( RESPONSE, response );
		values.put( STATUS, status );
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).appendPath( ""+jobId ).build();
		resolver.update(uri, values, null, null);
	}
	*/
	
	
	
	public static final List< Archive > getArchives( Context context ){
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "archives" ).build();
		return getArchivesByUri(context, uri);
	}
	
	
	/*
	public static final List< ScheduleJob > getShedulesJobsByStatus( Context context, ScheduleJob.Status status ){
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "schedules" ).appendPath("by_status").appendPath( status.toString() ).build();
		return getSchedulesByUri(context, uri);
	}
	*/
	
	
	public static final List< Archive > getArchivesByUri( Context context,Uri uri ){
		ContentResolver resolver = context.getContentResolver();
		
		String[] projection = { ID,ARCHIVE_TYPE,OWNER,DATE, OWNER_NAME };
		Cursor cursor = resolver.query(uri, projection, null, null, null);
		
		List< Archive > archives = new ArrayList<Archive>();
		
		if( cursor.moveToFirst() ){
			do{
				Archive archive = Archive.builder()
						.setId( cursor.getInt( cursor.getColumnIndexOrThrow( ID ) ) )
						.setArchiveType( cursor.getString( cursor.getColumnIndexOrThrow( ARCHIVE_TYPE) ) )
						.setOwner( cursor.getString( cursor.getColumnIndexOrThrow( OWNER ) ) )
						.setDate( cursor.getString( cursor.getColumnIndexOrThrow( DATE ) ) )
						.setName( cursor.getString( cursor.getColumnIndexOrThrow( OWNER_NAME ) ) )
						.build();
				
				archives.add(archive);
			}while( cursor.moveToNext() );		
		}
		cursor.close();
		
		return archives;
	}
	
	
	
	
	public static void onUpgradeDatabase( Context context ){
		SqliteOpenHelper helper = new SqliteOpenHelper( context, null );
		helper.onUpgrade( Database.getWritableDatabase( context ),0,0 );
	}
}
