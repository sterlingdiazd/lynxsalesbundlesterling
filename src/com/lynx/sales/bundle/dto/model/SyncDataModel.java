package com.lynx.sales.bundle.dto.model;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.db.SqliteOpenHelper;
import com.lynx.sales.bundle.entities.Sync;
import com.lynx.sales.bundle.provider.SalesContract;
import com.lynx.sales.bundle.provider.SalesContract.Sales;
import java.util.ArrayList;
import java.util.List;

public final class SyncDataModel
{
  public static final void deleteSync(Context paramContext, String paramString)
  {
    paramContext.getContentResolver().delete(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("sync").appendPath(paramString).build(), null, null);
  }
  
  public static final List<Sync> getAllSync(Context paramContext)
  {
    ContentResolver localContentResolver = paramContext.getContentResolver();
    String[] arrayOfString = { "id", "date", "entity", "status" };
    Cursor localCursor = localContentResolver.query(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("sync").build(), arrayOfString, null, null, null);
    ArrayList<Sync> localArrayList = new ArrayList<Sync>();
    if (localCursor.moveToFirst()) {
      do
      {
    	  Sync sync = new Sync();
    	  sync.setId(localCursor.getString(localCursor.getColumnIndexOrThrow("id")));
		  sync.setDate(localCursor.getString(localCursor.getColumnIndexOrThrow("date")));
		  sync.setEntity(localCursor.getString(localCursor.getColumnIndexOrThrow("entity")));
		  sync.setStatus(localCursor.getString(localCursor.getColumnIndexOrThrow("status")));
		  localArrayList.add(sync);
      } 
      while (localCursor.moveToNext());
    }
    localCursor.close();
    return localArrayList;
  }
  
	public static final Sync getSyncEntity(Context paramContext, Uri paramUri) 
	{
		Sync localSync = null;
		Cursor localCursor = paramContext.getContentResolver().query(paramUri,
				new String[] { "id", "date", "entity", "status" }, null, null, null);

		if (localCursor != null && localCursor.moveToFirst()) 
		{
			do {
				localSync = new Sync(localCursor.getString(localCursor.getColumnIndexOrThrow("id")),
						localCursor.getString(localCursor.getColumnIndexOrThrow("date")),
						localCursor.getString(localCursor.getColumnIndexOrThrow("entity")),
						localCursor.getString(localCursor.getColumnIndexOrThrow("status")));
			} while (localCursor.moveToNext());

			if (localCursor != null) {
				localCursor.close();
			}

		}

		return localSync;
	}
  
  public static void onUpgradeDatabase(Context paramContext)
  {
    new SqliteOpenHelper(paramContext, null).onUpgrade(Database.getWritableDatabase(paramContext), 1, 0);
  }
  
  public static void setSync(Context paramContext, Sync paramSync)
  {
    ContentResolver localContentResolver = paramContext.getContentResolver();
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("id", paramSync.getId());
    localContentValues.put("date", paramSync.getDate());
    localContentValues.put("entity", paramSync.getEntity());
    localContentValues.put("status", paramSync.getStatus());
    Sync localSync = getSyncEntity(paramContext, SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("sync").appendPath(paramSync.getEntity()).build());
    if (localSync != null) {
      deleteSync(paramContext, localSync.getId());
    }
    localContentResolver.insert(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("sync").build(), localContentValues);
  }
}
