package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.*;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

import com.lynx.sales.bundle.provider.SalesContract;
import com.lynxsales.util.Utility;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ClientInfo;
import com.lynx.sales.bundle.entities.SalesArea;
import com.lynx.sales.bundle.http.request.Common;
import com.lynx.sales.bundle.http.request.ResponseBinder;



public final class ClientDataModel  {

	public static boolean clientSincronized = false;

	public static final void saveClient( Context context, String jsonResponse)
	{
		ClientEntity client = ResponseBinder.bindClient(jsonResponse);	
		
		ClientEntity savedClient = getClient(context,client.getKunnr());
		
		if(savedClient.getId() == null || !savedClient.getKunnr().equalsIgnoreCase(client.getKunnr()))
		{
			clientSincronized = true;
			
			ContentResolver resolver = context.getContentResolver();
			ContentValues values = new ContentValues();
			values.put( KUNNR, 			client.getKunnr() );
			values.put( NAME, 				client.getName() );
			values.put( RNC,				client.getRNC());
			values.put( INTERLOCUTOR, 		client.getInterlocutor());
			values.put( PAYMENT_COND, 		client.getPayment_cond());
			values.put( CONTACT_NAME, 		client.getContactName() );
			values.put( CONTACT_LAST_NAME, client.getContactLastName());
			values.put( ADDRESS_1,  		client.getAddress1());
			values.put( ADDRESS_2,  		client.getAddress2());
			values.put( LOCATION,   		client.getLocation());
			values.put( PHONE,   			client.getPhone());						
			values.put( AVAILABLE,   		client.getAvailable());
			values.put( TODAY_DATE,   		client.getTodayDate());			
			values.put( RAMO,  			client.getRamo());
			values.put( CANAL,   			client.getCanal());					
			values.put( SECTOR,   			client.getSector());
			values.put( ORGANIZACION,   	client.getOrganizacion());
			values.put( STATUS,   			"SINCRONIZADO");
			
			resolver.insert( SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).build(),values );
			
		}
		
	}
	
	
	public static final int saveClient( Context context, ClientEntity client ){
			
		ContentResolver resolver = context.getContentResolver();
		
		ContentValues values = new ContentValues();
		values.put( KUNNR, 			client.getKunnr() );
		values.put( NAME, 				client.getName() );
		values.put( RNC,				client.getRNC());
		values.put( INTERLOCUTOR, 		client.getInterlocutor());
		values.put( PAYMENT_COND, 		client.getPayment_cond());
		values.put( CONTACT_NAME, 		client.getContactName() );
		values.put( CONTACT_LAST_NAME, client.getContactLastName());
		values.put( ADDRESS_1,  		client.getAddress1());
		values.put( ADDRESS_2,  		client.getAddress2());
		values.put( LOCATION,   		client.getLocation());
		values.put( PHONE,   			client.getPhone());						
		values.put( AVAILABLE,   		client.getAvailable());
		values.put( TODAY_DATE,   		client.getTodayDate());			
		values.put( RAMO,  			client.getRamo());
		values.put( CANAL,   			client.getCanal());					
		values.put( SECTOR,   			client.getSector());
		values.put( ORGANIZACION,   	client.getOrganizacion());
		values.put( STATUS,   			client.getStatus());
		
		Uri uriInserted = resolver.insert( SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).build(),values );
		return Integer.valueOf( uriInserted.getLastPathSegment() );
	}
	
	public static final int saveClient( Context context,List<ClientEntity> clients )
	{
		ContentResolver resolver = context.getContentResolver();
		ContentValues[] values = null;
		if(clients != null)
		{
			values = new ContentValues[ clients.size() ];
			
			for( int i=0; clients.size() >i; ++i )
			{
				ClientEntity client = clients.get(i);
				values[ i ] = new ContentValues();
				
				values[ i ].put( KUNNR, 			client.getKunnr() );
				values[ i ].put( NAME, 				client.getName() );
				values[ i ].put( RNC,				client.getRNC());
				values[ i ].put( INTERLOCUTOR, 		client.getInterlocutor());
				values[ i ].put( PAYMENT_COND, 		client.getPayment_cond());
				values[ i ].put( CONTACT_NAME, 		client.getContactName() );
				values[ i ].put( CONTACT_LAST_NAME, client.getContactLastName());
				values[ i ].put( ADDRESS_1,  		client.getAddress1());
				values[ i ].put( ADDRESS_2,  		client.getAddress2());
				values[ i ].put( LOCATION,   		client.getLocation());
				values[ i ].put( PHONE,   			client.getPhone());						
				values[ i ].put( AVAILABLE,   		client.getAvailable());
				values[ i ].put( TODAY_DATE,   		client.getTodayDate());			
				values[ i ].put( RAMO,  			client.getRamo());
				values[ i ].put( CANAL,   			client.getCanal());					
				values[ i ].put( SECTOR,   			client.getSector());
				values[ i ].put( ORGANIZACION,   	client.getOrganizacion());
				values[ i ].put( STATUS,   			"SINCRONIZADO");

			}
			
		}
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).build();
		int uriInserted = resolver.bulkInsert(uri , values );
		return uriInserted;
	}
	
	public static final ClientEntity getClient( Context context,String id ){
		
		ClientEntity client = new ClientEntity();
		
		ContentResolver resolver = context.getContentResolver();
		String[] projection = { ID, KUNNR, NAME, RNC, INTERLOCUTOR,PAYMENT_COND, CONTACT_NAME, CONTACT_LAST_NAME, ADDRESS_1,ADDRESS_2, LOCATION, PHONE,CREDIT,OPEN,AVAILABLE,TODAY_DATE, RAMO, CANAL, SECTOR, ORGANIZACION,  STATUS };
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).appendPath( id ).build();
		
		Cursor cursor = resolver.query(uri, projection, null, null, null );
		if( cursor != null && cursor.moveToFirst() )
		{	
			client.setId( cursor.getString( cursor.getColumnIndexOrThrow( 			ID ) ) );
			client.setKunnr( cursor.getString( cursor.getColumnIndexOrThrow( 		KUNNR) ) );
			client.setName( cursor.getString( cursor.getColumnIndexOrThrow( 		NAME) ) );
			client.setRNC( cursor.getString( cursor.getColumnIndexOrThrow( 			RNC) ) );
			client.setInterlocutor(cursor.getString( cursor.getColumnIndexOrThrow(  INTERLOCUTOR )));
			client.setPayment_cond(cursor.getString( cursor.getColumnIndexOrThrow(  PAYMENT_COND )));
			client.setContactName( cursor.getString( cursor.getColumnIndexOrThrow( 	CONTACT_NAME ) ) );
			client.setContactName( cursor.getString( cursor.getColumnIndexOrThrow( 	CONTACT_LAST_NAME ) ) );
			client.setAddress1( cursor.getString( cursor.getColumnIndexOrThrow( 	ADDRESS_1 ) ) );
			client.setAddress2( cursor.getString( cursor.getColumnIndexOrThrow( 	ADDRESS_2 ) ) );
			client.setLocation( cursor.getString( cursor.getColumnIndexOrThrow( 	LOCATION ) ) );
			client.setPhone( cursor.getString( cursor.getColumnIndexOrThrow( 		PHONE ) ) );
			client.setCredit( cursor.getString( cursor.getColumnIndexOrThrow( 		CREDIT ) ) );
			client.setOpen( cursor.getString( cursor.getColumnIndexOrThrow( 		OPEN ) ) );
			client.setAvailable( cursor.getString( cursor.getColumnIndexOrThrow( 	AVAILABLE ) ) );
			client.setTodayDate( cursor.getString( cursor.getColumnIndexOrThrow( 	TODAY_DATE) ) );
			client.setRamo( cursor.getString( cursor.getColumnIndexOrThrow( 		RAMO ) ) );
			client.setCanal( cursor.getString( cursor.getColumnIndexOrThrow( 		CANAL) ) );
			client.setSector( cursor.getString( cursor.getColumnIndexOrThrow( 		SECTOR  ) ) );
			client.setOrganizacion( cursor.getString( cursor.getColumnIndexOrThrow( ORGANIZACION) ) );
			client.setStatus( cursor.getString( cursor.getColumnIndexOrThrow( 		STATUS) ) );
			 
		}
		
		if( cursor != null )
		{
			cursor.close();
		}
		


		return client;
	}
	
	public static final List<ClientEntity> getClients( Context context){
		
		
		
		ContentResolver resolver = context.getContentResolver();
		String[] projection = { ID, KUNNR, NAME, RNC, INTERLOCUTOR,PAYMENT_COND, CONTACT_NAME, CONTACT_LAST_NAME, ADDRESS_1,ADDRESS_2, LOCATION, PHONE,CREDIT,OPEN,AVAILABLE,TODAY_DATE, RAMO, CANAL, SECTOR, ORGANIZACION,  STATUS };
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).build();
		
		ArrayList<ClientEntity> clients = new ArrayList<ClientEntity>();
		
		Cursor cursor = resolver.query(uri, projection, null, null, null );
		while (cursor.moveToNext()) 
		{	
			ClientEntity client = new ClientEntity();
			client.setId( cursor.getString( cursor.getColumnIndexOrThrow( 			ID ) ) );
			client.setKunnr( cursor.getString( cursor.getColumnIndexOrThrow( 		KUNNR) ) );
			client.setName( cursor.getString( cursor.getColumnIndexOrThrow( 		NAME) ) );
			client.setRNC( cursor.getString( cursor.getColumnIndexOrThrow( 			RNC) ) );
			client.setInterlocutor(cursor.getString( cursor.getColumnIndexOrThrow(  INTERLOCUTOR )));
			client.setPayment_cond(cursor.getString( cursor.getColumnIndexOrThrow(  PAYMENT_COND )));
			client.setContactName( cursor.getString( cursor.getColumnIndexOrThrow( 	CONTACT_NAME ) ) );
			client.setContactName( cursor.getString( cursor.getColumnIndexOrThrow( 	CONTACT_LAST_NAME ) ) );
			client.setAddress1( cursor.getString( cursor.getColumnIndexOrThrow( 	ADDRESS_1 ) ) );
			client.setAddress2( cursor.getString( cursor.getColumnIndexOrThrow( 	ADDRESS_2 ) ) );
			client.setLocation( cursor.getString( cursor.getColumnIndexOrThrow( 	LOCATION ) ) );
			client.setPhone( cursor.getString( cursor.getColumnIndexOrThrow( 		PHONE ) ) );
			client.setCredit( cursor.getString( cursor.getColumnIndexOrThrow( 		CREDIT ) ) );
			client.setOpen( cursor.getString( cursor.getColumnIndexOrThrow( 		OPEN ) ) );
			client.setAvailable( cursor.getString( cursor.getColumnIndexOrThrow( 	AVAILABLE ) ) );
			client.setTodayDate( cursor.getString( cursor.getColumnIndexOrThrow( 	TODAY_DATE) ) );
			client.setRamo( cursor.getString( cursor.getColumnIndexOrThrow( 		RAMO ) ) );
			client.setCanal( cursor.getString( cursor.getColumnIndexOrThrow( 		CANAL) ) );
			client.setSector( cursor.getString( cursor.getColumnIndexOrThrow( 		SECTOR  ) ) );
			client.setOrganizacion( cursor.getString( cursor.getColumnIndexOrThrow( ORGANIZACION) ) );
			client.setStatus( cursor.getString( cursor.getColumnIndexOrThrow( 		STATUS) ) );
			clients.add(client);
		}
		cursor.close();
		
		
		return clients;
	}
	
	public static final ArrayList<String> getIDClients( Context context ){
		
		ContentResolver resolver = context.getContentResolver();
		String[] projection = {  KUNNR };
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).build();
		
		ArrayList<String> clients = new ArrayList<String>();
		Cursor cursor = resolver.query(uri, projection, null, null, null );
		if( cursor.moveToFirst() )
		{	
			String id =  cursor.getString( cursor.getColumnIndexOrThrow( KUNNR ) );
			clients.add(id);
		}
		
		return clients;
	}
	

	public static final ClientEntity getClientByExternalID( Context context,String id ){
		
		ClientEntity client = new ClientEntity();
		
		ContentResolver resolver = context.getContentResolver();
		String[] projection = { ID, KUNNR, NAME, RNC, INTERLOCUTOR,PAYMENT_COND, CONTACT_NAME, CONTACT_LAST_NAME, ADDRESS_1,ADDRESS_2, LOCATION, PHONE,CREDIT,OPEN,AVAILABLE,TODAY_DATE, RAMO, CANAL, SECTOR, ORGANIZACION,  STATUS };
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).appendPath("external").appendPath( id ).build();
		
		Cursor cursor = resolver.query(uri, projection, null, null, null );
		if( cursor != null && cursor.moveToFirst() )
		{	
			client.setKunnr( cursor.getString( cursor.getColumnIndexOrThrow( 		KUNNR) ) );
			client.setName( cursor.getString( cursor.getColumnIndexOrThrow( 		NAME) ) );
			client.setRNC( cursor.getString( cursor.getColumnIndexOrThrow( 			RNC) ) );
			client.setInterlocutor(cursor.getString( cursor.getColumnIndexOrThrow(  INTERLOCUTOR )));
			client.setPayment_cond(cursor.getString( cursor.getColumnIndexOrThrow(  PAYMENT_COND )));
			client.setContactName( cursor.getString( cursor.getColumnIndexOrThrow( 	CONTACT_NAME ) ) );
			client.setContactName( cursor.getString( cursor.getColumnIndexOrThrow( 	CONTACT_LAST_NAME ) ) );
			client.setAddress1( cursor.getString( cursor.getColumnIndexOrThrow( 	ADDRESS_1 ) ) );
			client.setAddress2( cursor.getString( cursor.getColumnIndexOrThrow( 	ADDRESS_2 ) ) );
			client.setLocation( cursor.getString( cursor.getColumnIndexOrThrow( 	LOCATION ) ) );
			client.setPhone( cursor.getString( cursor.getColumnIndexOrThrow( 		PHONE ) ) );
			client.setCredit( cursor.getString( cursor.getColumnIndexOrThrow( 		CREDIT ) ) );
			client.setOpen( cursor.getString( cursor.getColumnIndexOrThrow( 		OPEN ) ) );
			client.setAvailable( cursor.getString( cursor.getColumnIndexOrThrow( 	AVAILABLE ) ) );
			client.setTodayDate( cursor.getString( cursor.getColumnIndexOrThrow( 	TODAY_DATE) ) );
			client.setRamo( cursor.getString( cursor.getColumnIndexOrThrow( 		RAMO ) ) );
			client.setCanal( cursor.getString( cursor.getColumnIndexOrThrow( 		CANAL) ) );
			client.setSector( cursor.getString( cursor.getColumnIndexOrThrow( 		SECTOR  ) ) );
			client.setOrganizacion( cursor.getString( cursor.getColumnIndexOrThrow( ORGANIZACION) ) );
			client.setStatus( cursor.getString( cursor.getColumnIndexOrThrow( 		STATUS) ) );
		}
		
		if( cursor != null )
		{	
			cursor.close();
		}
		
		
		return client;
	}
	
	public static final List<ClientEntity> getNewClients( Context context ){
		
		List<ClientEntity> clients = new ArrayList<ClientEntity>();
		
		ContentResolver resolver = context.getContentResolver();
		String[] projection = { ID, KUNNR, NAME, RNC, INTERLOCUTOR,PAYMENT_COND, CONTACT_NAME, CONTACT_LAST_NAME, ADDRESS_1,ADDRESS_2, LOCATION, PHONE,CREDIT,OPEN,AVAILABLE,TODAY_DATE, RAMO, CANAL, SECTOR, ORGANIZACION,  STATUS };
		
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).build();		
		Cursor cursor = resolver.query(uri, projection, null, null, null );		
		while (cursor.moveToNext()) 
		{
			ClientEntity client = new ClientEntity();
			client.setId( cursor.getString( cursor.getColumnIndexOrThrow( 			ID ) ) );
			client.setKunnr( cursor.getString( cursor.getColumnIndexOrThrow( 		KUNNR) ) );
			client.setName( cursor.getString( cursor.getColumnIndexOrThrow( 		NAME) ) );
			client.setRNC( cursor.getString( cursor.getColumnIndexOrThrow( 			RNC) ) );
			client.setInterlocutor(cursor.getString( cursor.getColumnIndexOrThrow(  INTERLOCUTOR )));
			client.setPayment_cond(cursor.getString( cursor.getColumnIndexOrThrow(  PAYMENT_COND )));
			client.setContactName( cursor.getString( cursor.getColumnIndexOrThrow( 	CONTACT_NAME ) ) );
			client.setContactName( cursor.getString( cursor.getColumnIndexOrThrow( 	CONTACT_LAST_NAME ) ) );
			client.setAddress1( cursor.getString( cursor.getColumnIndexOrThrow( 	ADDRESS_1 ) ) );
			client.setAddress2( cursor.getString( cursor.getColumnIndexOrThrow( 	ADDRESS_2 ) ) );
			client.setLocation( cursor.getString( cursor.getColumnIndexOrThrow( 	LOCATION ) ) );
			client.setPhone( cursor.getString( cursor.getColumnIndexOrThrow( 		PHONE ) ) );
			client.setCredit( cursor.getString( cursor.getColumnIndexOrThrow( 		CREDIT ) ) );
			client.setOpen( cursor.getString( cursor.getColumnIndexOrThrow( 		OPEN ) ) );
			client.setAvailable( cursor.getString( cursor.getColumnIndexOrThrow( 	AVAILABLE ) ) );
			client.setTodayDate( cursor.getString( cursor.getColumnIndexOrThrow( 	TODAY_DATE) ) );
			client.setRamo( cursor.getString( cursor.getColumnIndexOrThrow( 		RAMO ) ) );
			client.setCanal( cursor.getString( cursor.getColumnIndexOrThrow( 		CANAL) ) );
			client.setSector( cursor.getString( cursor.getColumnIndexOrThrow( 		SECTOR  ) ) );
			client.setOrganizacion( cursor.getString( cursor.getColumnIndexOrThrow( ORGANIZACION) ) );
			client.setStatus( cursor.getString( cursor.getColumnIndexOrThrow( 		STATUS) ) );
			clients.add(client);   
		}
		cursor.close();		
		return clients;
	}
	
	
	public static final void saveClientInfo( Context context,String clientId,String jsonResponse )
	{
		ClientInfo info = ResponseBinder.bindClientInfo( jsonResponse );		
		ContentResolver resolver = context.getContentResolver();		
		String societies = Common.Functions.join(",", info.getSocieties() );
		
		if( null != info.getSalesArea() ){
			ContentValues[] values = new ContentValues[ info.getSalesArea().size() ];
			
			for( int i=0; info.getSalesArea().size()>i;++i ){
				SalesArea area = info.getSalesArea().get( i );
				values[ i ] = new ContentValues();
				
				values[ i ].put( ID_CLIENT, clientId );
				values[ i ].put( SOCIETIES, societies );
				values[ i ].put( SALES_ORG_2, area.getSalesOrg() );
				values[ i ].put( DIST_CHANNEL, area.getDistChannel() );
				values[ i ].put( SECTOR, area.getSector() );
				values[ i ].put( PAYMENT_COND, area.getPaymentCond() );
				values[ i ].put( INTERLOCUTORS, Common.Functions.join(",", area.getInterlocutors() ) );
			}
			
			Uri uri =  SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).appendPath( clientId ).appendPath( "set" ).appendPath("info").build();
			resolver.bulkInsert( uri, values);			
		}				
	}
	
	public static final void deleteClients(Context context) 
	{
		ContentResolver resolver = context.getContentResolver();
		resolver.delete(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("clients").build(), null, null);
	}
	
	public static final ClientInfo getClientInfo( Context context,String clientId ){
		ClientInfo info = new ClientInfo();
		
		ContentResolver resolver = context.getContentResolver();
		String[] projection = {
				SOCIETIES,SALES_ORG_2,DIST_CHANNEL,SECTOR,PAYMENT_COND,INTERLOCUTORS
		};
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).appendPath( clientId ).appendPath("get").appendPath("info").build();
		
		Cursor cursor = resolver.query(uri, projection, null, null, null );
		
		if( cursor != null && cursor.moveToFirst() ){
			
			String societies = cursor.getString( cursor.getColumnIndexOrThrow( SOCIETIES ) );
			if( null != societies )
				info.setSocieties( societies.split( "\\,"  ));
				
			List< SalesArea > areas = new ArrayList<SalesArea>();
			
			do{	
				SalesArea area = new SalesArea();
				String interlocutors = cursor.getString( cursor.getColumnIndexOrThrow( INTERLOCUTORS ) );
				if( null != interlocutors )
					area.setInterlocutors( interlocutors.split( "\\," ) );
					
				area.setSalesOrg( cursor.getString( cursor.getColumnIndexOrThrow( SALES_ORG_2 ) ) );
				area.setDistChannel( cursor.getString( cursor.getColumnIndexOrThrow( DIST_CHANNEL ) ) );
				area.setSector( cursor.getString( cursor.getColumnIndexOrThrow( SECTOR ) ) );
				area.setPaymentCond( cursor.getString( cursor.getColumnIndexOrThrow( PAYMENT_COND ) ) );
				
				areas.add(area);
				
			}while( cursor.moveToNext() );
			
			info.setSalesArea(areas);
			cursor.close();
		}
		return info;
	}
	
	


}
