package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.*;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.lynx.sales.bundle.entities.CenterEntity;
import com.lynx.sales.bundle.entities.DocTypeEntity;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynx.sales.bundle.provider.SalesContract;

public final class UserRetrieval {
	
	
	public static UserEntity getUser( Context cxt )
	{
		UserEntity user = new UserEntity();
		
		ContentResolver resolver = cxt.getContentResolver();
		String userId = UserPreferences.getUserLogged(cxt);
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).appendPath( userId ).build();
		
		Cursor cursor = resolver.query(uri, new String[]{ "id","name","sales_organization", "cel", "tel" }, null,null, null);
		if( null != cursor && cursor.moveToFirst() ){
			do{
				user.setId( cursor.getString( cursor.getColumnIndexOrThrow( "id" ) ) );
				user.setName( cursor.getString( cursor.getColumnIndexOrThrow( "name" ) ) );
				user.setSalesOrganization( cursor.getString( cursor.getColumnIndexOrThrow( "sales_organization" ) ) );
				
				
				SQLiteDatabase db = Database.getReadableDatabase(cxt);
		        Cursor cursorDocType = db.query("doctypes", new String[]{"doc_type", "description"}, null , null, null, null, null);
		        List< DocTypeEntity > docTypes = new ArrayList<DocTypeEntity>();
		        while(cursorDocType.moveToNext())
		        {
		        	String doc = cursorDocType.getString( cursorDocType.getColumnIndexOrThrow( "doc_type"));
		        	String des = cursorDocType.getString( cursorDocType.getColumnIndexOrThrow( DESCRIPTION) );
		        	docTypes.add( new DocTypeEntity( doc, des  ) );
		        }
		        
				user.setDocTypes(docTypes);
				user.setCel(cursor.getString( cursor.getColumnIndexOrThrow( "cel" ) ));
				user.setTel(cursor.getString( cursor.getColumnIndexOrThrow( "tel" ) ));
			}while( cursor.moveToNext() );
		}	
		if(cursor != null)
		{
			cursor.close();
		}
		

		user.setCenters( getCenters(cxt, user.getId() ) );		
		return user;
	}
	
	
	
	
	
	public static List< CenterEntity > getCenters( Context cxt,String userId ){
		
		List< CenterEntity > centers = new ArrayList<CenterEntity>();
		
		ContentResolver resolver = cxt.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "users" ).appendPath( userId ).appendPath("centers").build();
		
		Cursor cursor = resolver.query(uri, new String[]{ "id_user","name","description", "store","doc_type" }, null,null, null);
		if( null != cursor)
		{
			if(cursor.moveToFirst() ){
				do{
					CenterEntity center = new CenterEntity();
					center.setName( cursor.getString( cursor.getColumnIndexOrThrow( "name" ) ) );
					center.setDescription( cursor.getString( cursor.getColumnIndexOrThrow( "description" ) ) );
					center.setStore( cursor.getString( cursor.getColumnIndexOrThrow( "store" ) ) );
					center.setDocType( cursor.getString( cursor.getColumnIndexOrThrow( "doc_type" ) ) );
					centers.add(center);
					
				}while( cursor.moveToNext() );
			}
			cursor.close();
		}
		return centers;
	}
	

}
