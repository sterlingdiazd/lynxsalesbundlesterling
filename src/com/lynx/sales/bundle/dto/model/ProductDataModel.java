package com.lynx.sales.bundle.dto.model;

import static com.lynx.sales.bundle.dto.model.Constants.*;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.lynx.sales.bundle.entities.CenterEntity;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.Material;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.entities.UnitEntity;
import com.lynx.sales.bundle.http.request.ErrorStub;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.ResponseBinder;
import com.lynx.sales.bundle.http.request.ResponseStub;
import com.lynx.sales.bundle.provider.SalesContract;

public class ProductDataModel {

	public static final void bulkSaveProducts(Context context, List<Material> materials, String center) {

		if (!materials.isEmpty()) 
		{
			ContentResolver resolver = context.getContentResolver();
			ContentValues[] values = new ContentValues[materials.size()];

			for (int i = 0; materials.size() > i; ++i) 
			{
				Material material = materials.get(i);
				values[i] = new ContentValues();

				values[i].put(MATNR, material.getMatnr());
				values[i].put(MAKTX, material.getMaktx());
				values[i].put(VKORG, material.getVkorg());
				values[i].put(VRKME, material.getVrkme());
				values[i].put(MEINS, material.getMeins());
				values[i].put(LABST, material.getLabst());
				values[i].put(PRODH, material.getProdh());
				values[i].put(LGORT, material.getLgort());
				values[i].put(WERKS, center);
				
				List<UnitEntity> units = material.getUnidades();
				ContentValues[] unitValues = new ContentValues[units.size()];
				
				
				for( int x = 0; x < units.size(); x++)
				{
					UnitEntity unidad = units.get(x);
					unitValues[x] = new ContentValues();
					unitValues[x].put(MATNR, material.getMatnr());
					unitValues[x].put(MEINH, unidad.getMeinh());
				}
				Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath(UNITS).build();
				resolver.bulkInsert(uri, unitValues);
			}

			Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").build();
			resolver.bulkInsert(uri, values);
		}
	}

	public static final void bulkSaveAllProducts(Context context, List<ProductEntity> products) {

		ContentResolver resolver = context.getContentResolver();
		ContentValues[] values = new ContentValues[products.size()];

		for (int i = 0; products.size() > i; ++i) {
			ProductEntity product = products.get(i);
			values[i] = new ContentValues();

			values[i].put(MATNR, product.getMaterial());
			values[i].put(MAKTX, product.getDescription());
			values[i].put(VKORG, product.getOrganization());
			values[i].put(VRKME, product.getSalesUnit());
			values[i].put(MEINS, product.getBaseUnit());
			values[i].put(LABST, product.getStock());
			values[i].put(WERKS, product.getCenter());
			values[i].put(PRODH, product.getProdh());
			values[i].put(LGORT, product.getLgort());
		}

		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").build();
		resolver.bulkInsert(uri, values);
	}

	public static final void bulkSaveProductsByCenterAndStore(Context context, List<ProductEntity> products) 
	{
		ContentResolver resolver = context.getContentResolver();
		//resolver.delete(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").appendPath("center").appendPath("" + products.get(0).getCenter()).build(), null, null);

		ContentValues[] values = new ContentValues[products.size()];

		for (int i = 0; products.size() > i; ++i) {
			ProductEntity product = products.get(i);
			values[i] = new ContentValues();

			values[i].put(MATNR, product.getMaterial());
			values[i].put(MAKTX, product.getDescription());
			values[i].put(VKORG, product.getOrganization());
			values[i].put(VRKME, product.getSalesUnit());
			values[i].put(MEINS, product.getBaseUnit());
			values[i].put(LABST, product.getStock());
			values[i].put(WERKS, product.getCenter());
			values[i].put(PRODH, product.getProdh());
			values[i].put(LGORT, product.getLgort());
		}

		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").build();
		resolver.bulkInsert(uri, values);
	}

	public static final List<ProductEntity> getProductsByCenterAndStore(Context context, CenterEntity center, ClientEntity client) 
	{
		ContentResolver resolver = context.getContentResolver();
		

		String centerName = center.getName();
		String store = center.getStore();
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products")
				.appendPath("center").appendPath(centerName)
				.appendPath("store").appendPath(store)
				.build();

		String[] projection = { ID, MATNR, MAKTX, VKORG, VRKME, MEINS, LABST, WERKS, PRODH, LGORT };

		List<ProductEntity> products = new ArrayList<ProductEntity>();
		Cursor cursor = resolver.query(uri, projection, null, null, null);
		if (cursor.moveToNext()) {
			do {
				ProductEntity product = new ProductEntity();

				product.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
				product.setMaterial(cursor.getString(cursor.getColumnIndexOrThrow(MATNR)));
				product.setThumb(product.getMaterial() + ".png");
				product.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(MAKTX)));
				product.setOrganization(cursor.getString(cursor.getColumnIndexOrThrow(VKORG)));
				product.setSalesUnit(cursor.getString(cursor.getColumnIndexOrThrow(VRKME)));
				product.setBaseUnit(cursor.getString(cursor.getColumnIndexOrThrow(MEINS)));
				product.setStock(cursor.getString(cursor.getColumnIndexOrThrow(LABST)));
				product.setCenter(cursor.getString(cursor.getColumnIndexOrThrow(WERKS)));
				product.setProdh(cursor.getString(cursor.getColumnIndexOrThrow(PRODH)));
				product.setLgort(cursor.getString(cursor.getColumnIndexOrThrow(LGORT)));
				products.add(product);

			} while (cursor.moveToNext());
		}
		cursor.close();
		return products;
	}

	public static final List<ProductEntity> getProductsByCenterAndStore(Context context, CenterEntity center, ClientEntity client, int start, int limit) 
	{
		ContentResolver resolver = context.getContentResolver();
		

		String centerName = center.getName();
		String store = center.getStore();
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products")
				.appendPath("center").appendPath(centerName)
				.appendPath("store").appendPath(store)
				.appendPath(String.valueOf(start))
				.appendPath(String.valueOf(limit))
				.build();

		String[] projection = { ID, MATNR, MAKTX, VKORG, VRKME, MEINS, LABST, WERKS, PRODH, LGORT };

		List<ProductEntity> products = new ArrayList<ProductEntity>();
		Cursor cursor = resolver.query(uri, projection, null, null, null);
		if (cursor.moveToNext()) {
			do {
				ProductEntity product = new ProductEntity();

				product.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
				product.setMaterial(cursor.getString(cursor.getColumnIndexOrThrow(MATNR)));
				product.setThumb(product.getMaterial() + ".png");
				product.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(MAKTX)));
				product.setOrganization(cursor.getString(cursor.getColumnIndexOrThrow(VKORG)));
				product.setSalesUnit(cursor.getString(cursor.getColumnIndexOrThrow(VRKME)));
				product.setBaseUnit(cursor.getString(cursor.getColumnIndexOrThrow(MEINS)));
				product.setStock(cursor.getString(cursor.getColumnIndexOrThrow(LABST)));
				product.setCenter(cursor.getString(cursor.getColumnIndexOrThrow(WERKS)));
				product.setProdh(cursor.getString(cursor.getColumnIndexOrThrow(PRODH)));
				product.setLgort(cursor.getString(cursor.getColumnIndexOrThrow(LGORT)));
				products.add(product);

			} while (cursor.moveToNext());
		}
		cursor.close();
		return products;
	}

	public static final List<ProductEntity> getAllProducts(Context context) {
		/*
		 * List<ProductEntity> allProducts = new ArrayList<ProductEntity>();
		 * List<CenterEntity> centers = userEntity.getCenters(); for
		 * (CenterEntity center : centers) { List<ProductEntity> products =
		 * ProductDataModel.getProductsByCenter(context, center.getName());
		 * 
		 * }
		 */
		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").build();

		String[] projection = { ID, MATNR, MAKTX, VKORG, VRKME, MEINS, LABST, WERKS, PRODH, LGORT };

		List<ProductEntity> products = new ArrayList<ProductEntity>();
		Cursor cursor = resolver.query(uri, projection, null, null, null);
		
		if (cursor.moveToNext()) 
		{
			do 
			{
				ProductEntity product = new ProductEntity();

				product.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
				product.setMaterial(cursor.getString(cursor.getColumnIndexOrThrow(MATNR)));
				product.setThumb(product.getMaterial() + ".png");
				product.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(MAKTX)));
				product.setOrganization(cursor.getString(cursor.getColumnIndexOrThrow(VKORG)));
				product.setSalesUnit(cursor.getString(cursor.getColumnIndexOrThrow(VRKME)));
				product.setBaseUnit(cursor.getString(cursor.getColumnIndexOrThrow(MEINS)));
				product.setStock(cursor.getString(cursor.getColumnIndexOrThrow(LABST)));
				product.setCenter(cursor.getString(cursor.getColumnIndexOrThrow(WERKS)));
				product.setProdh(cursor.getString(cursor.getColumnIndexOrThrow(PRODH)));
				product.setLgort(cursor.getString(cursor.getColumnIndexOrThrow(LGORT)));
				
				products.add(product);

			} while (cursor.moveToNext());
		}
		cursor.close();
		return products;
	}

	public static final ProductEntity getProductsByCenterAndMaterial(Context context, CenterEntity center, String material) 
	{
		ContentResolver resolver = context.getContentResolver();
		
		String centerName = center.getName();
		String store = center.getStore();
		
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon()
				.appendPath("products")
				.appendPath("center").appendPath(centerName)
				.appendPath("store").appendPath(store)
				.appendPath("matnr").appendPath(material).build();

		String[] projection = { ID, MATNR, MAKTX, VKORG, VRKME, MEINS, LABST, WERKS, PRODH, LGORT };
		ProductEntity product = new ProductEntity();

		Cursor cursor = resolver.query(uri, projection, null, null, null);
		if (cursor != null && cursor.moveToNext()) {
			do {

				product.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
				product.setMaterial(cursor.getString(cursor.getColumnIndexOrThrow(MATNR)));
				product.setThumb(product.getMaterial() + ".png");
				product.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(MAKTX)));
				product.setOrganization(cursor.getString(cursor.getColumnIndexOrThrow(VKORG)));
				product.setSalesUnit(cursor.getString(cursor.getColumnIndexOrThrow(VRKME)));
				product.setBaseUnit(cursor.getString(cursor.getColumnIndexOrThrow(MEINS)));
				product.setStock(cursor.getString(cursor.getColumnIndexOrThrow(LABST)));
				product.setCenter(cursor.getString(cursor.getColumnIndexOrThrow(WERKS)));
				product.setProdh(cursor.getString(cursor.getColumnIndexOrThrow(PRODH)));
				product.setLgort(cursor.getString(cursor.getColumnIndexOrThrow(LGORT)));
				
			} while (cursor.moveToNext());
		}
		if(cursor != null)
		{
			cursor.close();
		} 
			
		return product;
	}

    public static final List<ProductEntity> getProductsByIdOrDescription(Context context, CenterEntity centerEntity, ClientEntity clientEntity, String productIdOrName)
    {
	ContentResolver contentResolver = context.getContentResolver();
	String string2 = centerEntity.getName();
	String string3 = centerEntity.getStore();
	Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").appendPath("center")
		.appendPath(string2).appendPath("store").appendPath(string3).appendPath(productIdOrName).build();
	String[] arrstring = new String[]
	{ "id", "matnr", "maktx", "vkorg", "vrkme", "meins", "labst", "werks", "prodh", "lgort" };
	ArrayList<ProductEntity> arrayList = new ArrayList<ProductEntity>();
	Cursor cursor = contentResolver.query(uri, arrstring, null, null, null);
	if (cursor != null && cursor.moveToNext())
	{
	    do
	    {
		ProductEntity productEntity = new ProductEntity();
		productEntity.setId(cursor.getString(cursor.getColumnIndexOrThrow("id")));
		productEntity.setMaterial(cursor.getString(cursor.getColumnIndexOrThrow("matnr")));
		productEntity.setThumb(String.valueOf((Object) productEntity.getMaterial()) + ".png");
		productEntity.setDescription(cursor.getString(cursor.getColumnIndexOrThrow("maktx")));
		productEntity.setOrganization(cursor.getString(cursor.getColumnIndexOrThrow("vkorg")));
		productEntity.setSalesUnit(cursor.getString(cursor.getColumnIndexOrThrow("vrkme")));
		productEntity.setBaseUnit(cursor.getString(cursor.getColumnIndexOrThrow("meins")));
		productEntity.setStock(cursor.getString(cursor.getColumnIndexOrThrow("labst")));
		productEntity.setCenter(cursor.getString(cursor.getColumnIndexOrThrow("werks")));
		productEntity.setProdh(cursor.getString(cursor.getColumnIndexOrThrow("prodh")));
		productEntity.setLgort(cursor.getString(cursor.getColumnIndexOrThrow("lgort")));
		arrayList.add(productEntity);
	    } while (cursor.moveToNext());
	}
	cursor.close();
	return arrayList;
    }
	
	private static PhaseHandler handler = new PhaseHandler() {
		@Override
		public void onFeedback(String feed) {
		}

		@Override
		public void onError(ErrorStub stub) {
		}

		@Override
		public void onCompleted(ResponseStub response) {

			boolean result = ResponseBinder.bindDelete(response);

			if (result) {
				// tracking de lo sincronizado, para recuperar el proceso.
				// guardar en db el cliente completamente sincronizado, y segun
				// un orden, seguir desde ese en adelante.
			}

		}

		@Override
		public void onBegan() {

		}
	};

	public static final boolean checkClientProducts(Context context, String clientId) {

		boolean clientExists = false;

		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").appendPath(clientId).build();

		String[] projection = { ID_CLIENT };
		List<ProductEntity> products = new ArrayList<ProductEntity>();

		Cursor cursor = resolver.query(uri, projection, null, null, null);
		if (cursor != null && cursor.moveToFirst()) {
			ProductEntity product = new ProductEntity();

			String savedClientID = cursor.getString(cursor.getColumnIndexOrThrow(ID_CLIENT));
			if (clientId.equalsIgnoreCase(savedClientID)) {
				clientExists = true;
				// ProductDataModel.deleteClientProducts(context,
				// savedClientID);
			}
			cursor.close();
		}

		return clientExists;

	}

	public static final boolean checkProductOffer(Context context, String productId) {
		boolean hasOffer = false;
		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").appendPath("offer")
				.appendPath(productId).build();

		String[] projection = { ID, ID_CLIENT, NAME, THUMB, SALES_ORG, DIST_CHANNEL_PRODUCTS, PRICE, SALES_UNIT,
				QUANTITY_IN_SALES, UNIT_BASE, "text", STOCK, CREDIT, OFFER, CENTER, LGORT  };

		Cursor cursor = resolver.query(uri, projection, null, null, null);
		if (cursor != null && cursor.moveToFirst()) {
			String offer = cursor.getString(cursor.getColumnIndexOrThrow(OFFER));
			if (offer.length() > 0) {
				hasOffer = true;
			}
			cursor.close();
		}
		return hasOffer;
	}

	public static final void deleteClientProducts(Context context, String clientId) {
		ContentResolver resolver = context.getContentResolver();
		resolver.delete(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").appendPath("" + clientId)
				.build(), null, null);
	}

	public static final void deleteProducts(Context context) {
		ContentResolver resolver = context.getContentResolver();
		resolver.delete(SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").build(), null, null);
	}
	/*
	 * public static final void bulkSaveClients(Context context, String
	 * jsonResponse) { final List<ClientProduct> clientsProducts =
	 * ResponseBinder.bindClientProducts(jsonResponse); List<ProductEntity>
	 * productsToDelete = new ArrayList<ProductEntity>(); List<ProductEntity>
	 * productsToJson = new ArrayList<ProductEntity>();
	 * 
	 * if (!clientsProducts.isEmpty()) {
	 * 
	 * ContentResolver resolver = context.getContentResolver(); for
	 * (ClientProduct client : clientsProducts) { if
	 * (!client.getProducts().isEmpty()) { ContentValues[] values = new
	 * ContentValues[client.getProducts().size()];
	 * 
	 * for (int i = 0; client.getProducts().size() > i; ++i) { ProductEntity
	 * product = client.getProducts().get(i);
	 * 
	 * ProductEntity productToDelete = new ProductEntity();
	 * 
	 * productToDelete.setId(product.getId());
	 * productToDelete.setIdClient(client.getClientId());
	 * productToDelete.setCenter(product.getCenter()); //
	 * productToDelete.setNombreVendedor(seller.getName()); //
	 * productToDelete.setSalesOrganization(product.getSalesOrganization()); //
	 * productToDelete.setDistributionChannel(product.getDistributionChannel());
	 * // productToDelete.setSector(product.getSector()); //
	 * productToDelete.setIdVendedor(seller.getId()); //
	 * productToDelete.setRamo(product.getRamo());
	 * 
	 * productsToDelete.add(productToDelete);
	 * 
	 * values[i] = new ContentValues();
	 * 
	 * values[i].put(ID, product.getId()); values[i].put(ID_CLIENT,
	 * client.getClientId()); values[i].put(NAME, product.getName());
	 * values[i].put(THUMB, product.getThumb()); values[i].put(SALES_ORG,
	 * product.getSalesOrganization()); values[i].put(DIST_CHANNEL_PRODUCTS,
	 * product.getDistributionChannel()); values[i].put(RAMO,
	 * product.getRamo()); values[i].put(PRICE, product.getPrice());
	 * values[i].put(SALES_UNIT, product.getSalesUnit());
	 * values[i].put(QUANTITY_IN_SALES, product.getQuantityInSalesUnit());
	 * values[i].put(UNIT_BASE, product.getUnitBase()); values[i].put(STOCK,
	 * product.getStock()); values[i].put(CREDIT, product.getCredit());
	 * values[i].put(SPART, product.getSector()); values[i].put(OFFER,
	 * product.getOffer()); values[i].put(CENTER, product.getCenter());
	 * 
	 * // No suppported yet values[i].put(EANCODE, ""); values[i].put(UNITS,
	 * ""); }
	 * 
	 * Uri uri =
	 * SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products"
	 * ).build(); resolver.bulkInsert(uri, values); } } }
	 * 
	 * }
	 */
	/*
	 * public static final void singleSaveClients(Context context, String
	 * jsonResponse) { final List<ClientProduct> clientsProducts =
	 * ResponseBinder.bindClientProducts(jsonResponse);
	 * 
	 * if (!clientsProducts.isEmpty()) { ContentResolver resolver =
	 * context.getContentResolver(); List<ProductEntity> productsToDelete = new
	 * ArrayList<ProductEntity>(); List<ProductEntity> productsToJson = new
	 * ArrayList<ProductEntity>();
	 * 
	 * for (ClientProduct client : clientsProducts) { if
	 * (!client.getProducts().isEmpty()) { for (int i = 0;
	 * client.getProducts().size() > i; ++i) { ProductEntity product =
	 * client.getProducts().get(i); ContentValues values = new ContentValues();
	 * 
	 * values.put(ID, product.getId()); values.put(ID_CLIENT,
	 * client.getClientId()); values.put(NAME, product.getName());
	 * values.put(THUMB, product.getThumb()); values.put(SALES_ORG,
	 * product.getSalesOrganization()); values.put(DIST_CHANNEL_PRODUCTS,
	 * product.getDistributionChannel()); values.put(RAMO, product.getRamo());
	 * values.put(PRICE, product.getPrice()); values.put(SALES_UNIT,
	 * product.getSalesUnit()); values.put(QUANTITY_IN_SALES,
	 * product.getQuantityInSalesUnit()); values.put(UNIT_BASE,
	 * product.getUnitBase()); values.put(STOCK, product.getStock());
	 * values.put(CREDIT, product.getCredit()); values.put(SPART,
	 * product.getSector()); values.put(OFFER, product.getOffer());
	 * values.put(CENTER, product.getCenter());
	 * 
	 * // No suppported yet values.put(EANCODE, ""); values.put(UNITS, "");
	 * 
	 * String material = product.getId(); String idClient =
	 * client.getClientId(); String center = product.getCenter();
	 * 
	 * if (material != "" && idClient != "" && center != "") { try { int
	 * deletedRows = resolver.delete(
	 * SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products")
	 * .appendPath(idClient).appendPath(material).appendPath(center).build(),
	 * null, null);
	 * 
	 * ProductEntity productToDelete = new ProductEntity();
	 * 
	 * UserEntity seller =
	 * UserRetrieval.getUser(context.getApplicationContext());
	 * 
	 * productToDelete.setId(material); productToDelete.setIdClient(idClient);
	 * productToDelete.setCenter(center); //
	 * productToDelete.setNombreVendedor(seller.getName()); //
	 * productToDelete.setSalesOrganization(product.getSalesOrganization()); //
	 * productToDelete.setDistributionChannel(product.getDistributionChannel());
	 * // productToDelete.setSector(product.getSector()); //
	 * productToDelete.setIdVendedor(seller.getId()); //
	 * productToDelete.setRamo(product.getRamo());
	 * 
	 * productsToDelete.add(productToDelete);
	 * 
	 * Uri uri =
	 * SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products"
	 * ).build(); resolver.insert(uri, values); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * } else { Log.e("error insercion material", "e"); } } }
	 * 
	 * }
	 * 
	 * try {
	 * 
	 * int productSize = productsToDelete.size(); int cantidadProductos = 0; int
	 * range = 0; int productsLeft;
	 * 
	 * int loops = 0; if (productSize > 100) { cantidadProductos = (productSize
	 * / 10); loops = 10; } else { cantidadProductos = productSize; loops = 1; }
	 * 
	 * for (int x = 0; x < loops; x++) { range += cantidadProductos; if (loops
	 * != 1 && (x + 1) == loops) { productsLeft = productSize - range; range =
	 * range + productsLeft; }
	 * 
	 * try { int initialRange = (x * cantidadProductos); productsToJson =
	 * productsToDelete.subList(initialRange, range);
	 * 
	 * ServiceStub stub = ServiceLocator.lookup(ServiceName.DELETE); Gson gson =
	 * new Gson(); String json = gson.toJson(productsToJson); json =
	 * json.replaceAll("(, |,| ,| , )", ";");
	 * 
	 * ArrayList<NameValuePair> parametros = new ArrayList<NameValuePair>();
	 * parametros.add(new BasicNameValuePair("table_name", "zsd_sincro"));
	 * parametros.add(new BasicNameValuePair("products", json));
	 * 
	 * String host = stub.getUrl(); HttpPost post = new HttpPost(host); try {
	 * post.setEntity(new UrlEncodedFormEntity(parametros)); } catch
	 * (UnsupportedEncodingException e) { e.printStackTrace(); }
	 * 
	 * HttpClient httpClient = new DefaultHttpClient(); HttpResponse resp =
	 * null; try { resp = httpClient.execute(post); int status =
	 * resp.getStatusLine().getStatusCode();
	 * 
	 * if (status == 200) { HttpEntity httpEntity = resp.getEntity();
	 * 
	 * InputStream is = null; try { is = httpEntity.getContent(); } catch
	 * (IOException e) { e.printStackTrace(); } if (is != null) { BufferedReader
	 * rd = null; try { rd = new BufferedReader(new InputStreamReader(is,
	 * "iso-8859-1"), 8); } catch (UnsupportedEncodingException e) {
	 * e.printStackTrace(); } StringBuilder sb = new StringBuilder(); String
	 * line = ""; try { while ((line = rd.readLine()) != null) { sb.append(line
	 * + "\n"); } is.close();
	 * 
	 * String result = sb.toString(); JSONObject o = new JSONObject(result);
	 * 
	 * } catch (IOException e) { e.printStackTrace(); } } } else {
	 * 
	 * } } catch (IOException e) { e.printStackTrace(); }
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); Log.e("ServicioRest",
	 * "Error: " + ex.getMessage() + ""); }
	 * 
	 * }
	 * 
	 * } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * } }
	 */

	/*
	 * public static final List<ProductEntity> getClientProducts(Context
	 * context, String clientId) { if
	 * (clientId.equals(ProductDataModel.clientId) && null !=
	 * lruClientsProducts) return new
	 * ArrayList<ProductEntity>(lruClientsProducts);
	 * 
	 * ContentResolver resolver = context.getContentResolver();
	 * 
	 * Uri uri =
	 * SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products"
	 * ).appendPath(clientId).build(); String[] projection = { ID, NAME, THUMB,
	 * SALES_ORG, DIST_CHANNEL_PRODUCTS, PRICE, SALES_UNIT, QUANTITY_IN_SALES,
	 * UNIT_BASE, STOCK, CREDIT, OFFER, CENTER };
	 * 
	 * List<ProductEntity> products = new ArrayList<ProductEntity>();
	 * 
	 * Cursor cursor = resolver.query(uri, projection, null, null, null); if
	 * (cursor.moveToNext()) { do { ProductEntity product = new ProductEntity();
	 * 
	 * product.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
	 * product.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME)));
	 * product.setThumb(cursor.getString(cursor.getColumnIndexOrThrow(THUMB)));
	 * product
	 * .setSalesOrganization(cursor.getString(cursor.getColumnIndexOrThrow
	 * (SALES_ORG)));
	 * product.setDistributionChannel(cursor.getString(cursor.getColumnIndexOrThrow
	 * (DIST_CHANNEL_PRODUCTS)));
	 * product.setPrice(cursor.getString(cursor.getColumnIndexOrThrow(PRICE)));
	 * product
	 * .setSalesUnit(cursor.getString(cursor.getColumnIndexOrThrow(SALES_UNIT
	 * )));
	 * product.setQuantityInSalesUnit(cursor.getString(cursor.getColumnIndexOrThrow
	 * (QUANTITY_IN_SALES)));
	 * product.setUnitBase(cursor.getString(cursor.getColumnIndexOrThrow
	 * (UNIT_BASE)));
	 * product.setStock(cursor.getString(cursor.getColumnIndexOrThrow(STOCK)));
	 * product
	 * .setCredit(cursor.getString(cursor.getColumnIndexOrThrow(CREDIT)));
	 * product.setOffer(cursor.getString(cursor.getColumnIndexOrThrow(OFFER)));
	 * product
	 * .setCenter(cursor.getString(cursor.getColumnIndexOrThrow(CENTER)));
	 * 
	 * products.add(product);
	 * 
	 * } while (cursor.moveToNext()); } cursor.close();
	 * 
	 * return products; }
	 */

	/*
	 * public static final ProductEntity getClientProduct(Context context,
	 * String clientId, String productId) { ContentResolver resolver =
	 * context.getContentResolver();
	 * 
	 * Uri uri =
	 * SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products"
	 * ).appendPath(clientId).build(); String[] projection = { ID, NAME, THUMB,
	 * SALES_ORG, DIST_CHANNEL_PRODUCTS, PRICE, SALES_UNIT, QUANTITY_IN_SALES,
	 * UNIT_BASE, STOCK, CREDIT, OFFER, CENTER }; ProductEntity product = null;
	 * 
	 * Cursor cursor = resolver.query(uri, projection, "id='" + productId + "'",
	 * null, null); if (cursor.moveToNext()) { do { product = new
	 * ProductEntity();
	 * 
	 * product.setId(cursor.getString(cursor.getColumnIndexOrThrow(ID)));
	 * product.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME)));
	 * product.setThumb(cursor.getString(cursor.getColumnIndexOrThrow(THUMB)));
	 * product
	 * .setSalesOrganization(cursor.getString(cursor.getColumnIndexOrThrow
	 * (SALES_ORG)));
	 * product.setDistributionChannel(cursor.getString(cursor.getColumnIndexOrThrow
	 * (DIST_CHANNEL_PRODUCTS)));
	 * product.setPrice(cursor.getString(cursor.getColumnIndexOrThrow(PRICE)));
	 * product
	 * .setSalesUnit(cursor.getString(cursor.getColumnIndexOrThrow(SALES_UNIT
	 * )));
	 * product.setQuantityInSalesUnit(cursor.getString(cursor.getColumnIndexOrThrow
	 * (QUANTITY_IN_SALES)));
	 * product.setUnitBase(cursor.getString(cursor.getColumnIndexOrThrow
	 * (UNIT_BASE)));
	 * product.setStock(cursor.getString(cursor.getColumnIndexOrThrow(STOCK)));
	 * product
	 * .setCredit(cursor.getString(cursor.getColumnIndexOrThrow(CREDIT)));
	 * product.setOffer(cursor.getString(cursor.getColumnIndexOrThrow(OFFER)));
	 * product
	 * .setCenter(cursor.getString(cursor.getColumnIndexOrThrow(CENTER)));
	 * 
	 * } while (cursor.moveToNext());
	 * 
	 * cursor.close(); }
	 * 
	 * return product; }
	 */
}
