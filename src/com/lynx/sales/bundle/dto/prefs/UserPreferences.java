package com.lynx.sales.bundle.dto.prefs;

import android.content.Context;

import com.lynx.sales.bundle.prefs.utils.PreferenceHelper;

public final class UserPreferences{
	
	private static final String PREF_NAME;
	
	private static final String KEY_LOGGED 					= "logged";
	private static final String KEY_ACCEPTED_CONDITIONS 	= "accepted_conditions";
	private static final String KEY_LICENSED			 	= "licensed";	
	private static final String KEY_USER_STRING			 	= "user_string_id";
	private static final String KEY_SYNCHRONIZED			= "synchronized";	
	
	
	
	
	static{
		PREF_NAME = String.valueOf( "com.lynx.sales.bundle.dto.prefs".hashCode() );
	}	
		
	public static boolean isLogged( Context cx ){
		return PreferenceHelper.getBoolean(cx, PREF_NAME, KEY_LOGGED);
	}
		
	public static void setLogged( Context cxt,boolean flag ){
		PreferenceHelper.setBoolean( cxt, PREF_NAME, KEY_LOGGED, flag );
	}
	
	public static boolean acceptedConditions( Context cxt ){
		return PreferenceHelper.getBoolean(cxt, PREF_NAME, KEY_ACCEPTED_CONDITIONS ); 
	}
	
	public static void setAcceptedCondition( Context cxt,boolean flag ){
		PreferenceHelper.setBoolean( cxt, PREF_NAME, KEY_ACCEPTED_CONDITIONS, flag );
	}
	
	public static boolean isLicensed( Context cxt ){
		return PreferenceHelper.getBoolean(cxt, PREF_NAME, KEY_LICENSED ); 
	}
	
	public static void setLicensed( Context cxt,boolean flag ){
		PreferenceHelper.setBoolean( cxt, PREF_NAME, KEY_LICENSED, flag );
	}
		

	public static void saveUserLogged( Context cxt,String user ){
		PreferenceHelper.setString(cxt, PREF_NAME, KEY_USER_STRING, user );
	}
	
	public static String getUserLogged( Context cxt ){
		return PreferenceHelper.getString(cxt, PREF_NAME, KEY_USER_STRING);
	}
	
	public static boolean isSynchronized( Context cxt ){
		return PreferenceHelper.getBoolean(cxt, PREF_NAME, KEY_SYNCHRONIZED );
	}
	
	public static void setSynchronized( Context cxt,boolean sync ){
		PreferenceHelper.setBoolean(cxt, PREF_NAME, KEY_SYNCHRONIZED, sync );
	}
}
