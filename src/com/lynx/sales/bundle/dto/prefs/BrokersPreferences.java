package com.lynx.sales.bundle.dto.prefs;

import android.content.Context;

import com.lynx.sales.bundle.prefs.utils.PreferenceHelper;

public final class BrokersPreferences {
	private static final String PREF_NAME;
	
	public static final String KEY_PROVIDER_RESOLVED 					= "providers_resolved";
	public static final String KEY_PROVIDER_DEFAULT 					= "providers_default";
	
	static{
		PREF_NAME = String.valueOf( "com.lynx.sales.bundle.dto.prefs.BrokersPreferences".hashCode() );
	}	
		
	public static boolean isBrokersResolved( Context cx ){
		return PreferenceHelper.getBoolean(cx, PREF_NAME, KEY_PROVIDER_RESOLVED);
	}
		
	public static void setBrokersResolved( Context cxt,boolean flag ){
		PreferenceHelper.setBoolean( cxt, PREF_NAME, KEY_PROVIDER_RESOLVED, flag );
	}	
	
	public static String getDefaultBrokerService( Context cx ){
		return PreferenceHelper.getString(cx, PREF_NAME, KEY_PROVIDER_DEFAULT);
	}
		
	public static void setDefaultBrokerService( Context cxt,String broker ){
		PreferenceHelper.setString( cxt, PREF_NAME, KEY_PROVIDER_DEFAULT, broker );
	}	
	 
}
