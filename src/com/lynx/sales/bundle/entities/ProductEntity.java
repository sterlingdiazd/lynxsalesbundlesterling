package com.lynx.sales.bundle.entities;

import static com.lynx.sales.bundle.dto.model.Constants.ID;
import static com.lynx.sales.bundle.dto.model.Constants.LABST;
import static com.lynx.sales.bundle.dto.model.Constants.MAKTX;
import static com.lynx.sales.bundle.dto.model.Constants.MATNR;
import static com.lynx.sales.bundle.dto.model.Constants.MEINS;
import static com.lynx.sales.bundle.dto.model.Constants.SPART;
import static com.lynx.sales.bundle.dto.model.Constants.VKORG;
import static com.lynx.sales.bundle.dto.model.Constants.VRKME;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ProductEntity implements Serializable{


	private String id;
	private String material;
	private String thumb;
	private String description;
	private String organization;
	private String salesUnit;
	private List<UnitEntity> salesUnits;
	private UnitEntity selectedSalesUnit;
	private String baseUnit;
	private String stock;
	private String center;
	private String offer;
	private String price;
	private String discount;
	private String prodh;
	private String lgort;
	private String knrmat;
	
	public String getLgort() {
		return lgort;
	}



	public void setLgort(String lgort) {
		this.lgort = lgort;
	}



	private String amount;
	private boolean amountVisible;

	
	public UnitEntity getSelectedSalesUnit() {
		return selectedSalesUnit;
	}



	public void setSelectedSalesUnit(UnitEntity selectedSalesUnit) {
		this.selectedSalesUnit = selectedSalesUnit;
	}



	public List<UnitEntity> getSalesUnits() {
		return salesUnits;
	}



	public void setSalesUnits(List<UnitEntity> salesUnits) {
		this.salesUnits = salesUnits;
	}




	
	public ProductEntity() {
	}
	


	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}

	public String getMaterial() {
		return material;
	}



	public void setMaterial(String material) {
		this.material = material;
	}

	public String getThumb() {
		return thumb;
	}



	public void setThumb(String thumb) {
		this.thumb = thumb;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getOrganization() {
		return organization;
	}



	public void setOrganization(String organization) {
		this.organization = organization;
	}






	public String getBaseUnit() {
		return baseUnit;
	}



	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}



	public String getStock() {
		return stock;
	}



	public void setStock(String stock) {
		this.stock = stock;
	}



	public String getCenter() {
		return center;
	}



	public void setCenter(String centro) {
		this.center = centro;
	}



	public String getOffer() {
		return offer;
	}



	public void setOffer(String offer) {
		this.offer = offer;
	}



	public String getPrice() {
		return price;
	}



	public void setPrice(String price) {
		this.price = price;
	}



	public boolean isAmountVisible() {
		return amountVisible;
	}



	public void setAmountVisible(boolean amountVisible) {
		this.amountVisible = amountVisible;
	}



	public String getAmount() {
		return amount;
	}



	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getProdh() {
		return prodh;
	}

	public void setProdh(String prodh) {
		this.prodh = prodh;
	}

	@Override
	public boolean equals(Object o) {
		if( !(o  instanceof ProductEntity ) )
			return false;
		ProductEntity other = ( ProductEntity ) o;		
		return ( null != id ? id.equals( other.id ) : (( null != other.id ) ? other.id.equals( id ) : true ) );
	}
	
	@Override
	public int hashCode() {
		int hashcode = null != id ? Integer.valueOf(id) : 0;
		return 31 * hashcode + ( null != id ? id.hashCode() : 0 );
	}
	
	public String getDiscount() {
		return discount;
	}



	public void setDiscount(String discount) {
		this.discount = discount;
	}



	public String getSalesUnit() {
		return salesUnit;
	}



	public void setSalesUnit(String salesUnit) {
		this.salesUnit = salesUnit;
	}



	@Override
	public String toString() {
		return id;
	}



	public String getKnrmat() {
		return knrmat;
	}



	public void setKnrmat(String knrmat) {
		this.knrmat = knrmat;
	}
}
