package com.lynx.sales.bundle.entities;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ClientEntity implements Serializable {

	private String id;
	private String kunnr;
	private String name;
	private String RNC;
	private String interlocutor;
	private String paymentCond;
	private String contactName;
	private String contactLastName;
	private String address1;
	private String address2;
	private String location;
	private String phone;
	private String credit;
	private String open;
	private String available;
	private String todayDate;
	private String ramo;
	private String canal;
	private String sector;
	private String organizacion;
	private String kdgrp;
	
	public String getKdgrp() {
		return kdgrp;
	}

	public void setKdgrp(String kdgrp) {
		this.kdgrp = kdgrp;
	}

	private String status;
	private ClientInfo clientInfo;
	
	public ClientInfo getClientInfo() {
		return clientInfo;
	}

	public void setClientInfo(ClientInfo clientInfo) {
		this.clientInfo = clientInfo;
	}

	public ClientEntity() {
		super();
	}
	public String getInterlocutor() {
		return interlocutor;
	}

	public void setInterlocutor(String interlocutor) {
		this.interlocutor = interlocutor;
	}

	public String getPayment_cond() {
		return paymentCond;
	}

	public void setPayment_cond(String payment_cond) {
		this.paymentCond = payment_cond;
	}

	public ClientEntity(String id, String kunnr, String name, String rNC, String interlocutor, String payment_cond,
			String contactName, String contactLastName, String address1, String address2, String location,
			String phone, String credit, String open, String available, String todayDate, String ramo, String canal,
			String sector, String organizacion, String status, ClientInfo clientInfo) {
		super();
		this.id = id;
		this.kunnr = kunnr;
		this.name = name;
		RNC = rNC;
		this.interlocutor = interlocutor;
		this.paymentCond = payment_cond;
		this.contactName = contactName;
		this.contactLastName = contactLastName;
		this.address1 = address1;
		this.address2 = address2;
		this.location = location;
		this.phone = phone;
		this.credit = credit;
		this.open = open;
		this.available = available;
		this.todayDate = todayDate;
		this.ramo = ramo;
		this.canal = canal;
		this.sector = sector;
		this.organizacion = organizacion;
		this.status = status;
		this.clientInfo = clientInfo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKunnr() {
		return kunnr;
	}

	public void setKunnr(String kunnr) {
		this.kunnr = kunnr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRNC() {
		return RNC;
	}

	public void setRNC(String rNC) {
		RNC = rNC;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public String getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(String todayDate) {
		this.todayDate = todayDate;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
