package com.lynx.sales.bundle.entities;

import java.io.Serializable;


@SuppressWarnings("serial")
public class RouteEntity implements Serializable{
	
	private String id;
	private String idRoute;
	private String createdBy;
	private String createdDate;
	private String address;	
	private String lazzyClientId;	
	private String lazzyClientName;
	
	public RouteEntity() {
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

	public String getIdRoute() {
		return idRoute;
	}


	public void setIdRoute(String idRoute) {
		this.idRoute = idRoute;
	}

	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public String getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getLazzyClientId() {
		return lazzyClientId;
	}


	public void setLazzyClientId(String lazzyClientId) {
		this.lazzyClientId = lazzyClientId;
	}

	
	

	public String getLazzyClientName() {
		return lazzyClientName;
	}


	public void setLazzyClientName(String lazzyClientName) {
		this.lazzyClientName = lazzyClientName;
	}


	public RouteEntry toEntry()
	{
		 return new RouteEntry(id,lazzyClientId,lazzyClientName, createdBy, createdDate);
	}
	
}
