package com.lynx.sales.bundle.entities;

public class Company {

	private String id;
	private String name;
	private String address;
	private String phone;
	private String rnc;
	private String fax;

	public Company(String id, String name, String address, String phone, String rnc,
			String fax) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.rnc = rnc;
		this.fax = fax;
	}
	
	public Company() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getName() {
		return name;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRnc() {
		return rnc;
	}
	public void setRnc(String rnc) {
		this.rnc = rnc;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}


	
}
