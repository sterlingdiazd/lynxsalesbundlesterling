package com.lynx.sales.bundle.entities;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SalesArea implements Serializable{

	
	private String salesOrg;
	private String distChannel;
	private String sector;
	private String paymentCond;
	private String[] interlocutors;
	
	public SalesArea() {
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}

	public String getDistChannel() {
		return distChannel;
	}

	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getPaymentCond() {
		return paymentCond;
	}

	public void setPaymentCond(String paymentCond) {
		this.paymentCond = paymentCond;
	}

	public String[] getInterlocutors() {
		return interlocutors;
	}

	public void setInterlocutors(String[] interlocutors) {
		this.interlocutors = interlocutors;
	}
	
	
	
}
