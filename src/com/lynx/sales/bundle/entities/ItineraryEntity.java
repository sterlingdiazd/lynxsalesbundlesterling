package com.lynx.sales.bundle.entities;

import java.io.Serializable;
import java.util.List;


@SuppressWarnings("serial")
public class ItineraryEntity implements Serializable{
	
	private String id;
	private String creationDate;
	private String dateFrom;
	private String dateTo;
	private String daysOfWeek;
	private String createdBy;
	private String modifiedBy;
	private String modifiedHour;
	private String description;
	
	
	private List< RouteEntity > routes;
	
	public ItineraryEntity() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getDaysOfWeek() {
		return daysOfWeek;
	}

	public void setDaysOfWeek( String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public List<RouteEntity> getRoutes() {
		return routes;
	}

	public void setRoutes(List<RouteEntity> routes) {
		this.routes = routes;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedHour() {
		return modifiedHour;
	}

	public void setModifiedHour(String modifiedHour) {
		this.modifiedHour = modifiedHour;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
