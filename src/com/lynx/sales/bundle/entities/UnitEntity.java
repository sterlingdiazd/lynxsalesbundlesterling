package com.lynx.sales.bundle.entities;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class UnitEntity implements Serializable {

	public UnitEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String id;
	private String matnr;
	public String getMatnr() {
		return matnr;
	}

	public void setMatnr(String matnr) {
		this.matnr = matnr;
	}

	private String meinh;

	public String getMeinh() {
		return meinh;
	}

	public void setMeinh(String meinh) {
		this.meinh = meinh;
	}

	public UnitEntity(String meinh) {
		super();
		this.meinh = meinh;
	}
	



	
	
}
