package com.lynx.sales.bundle.entities;

public class RouteEntry {
	
	private String id;
	private String idRoute;
	private String clientId;
	private String clientName;
	private String createdBy;
	private String createdAt;
	
	

	public RouteEntry( String id,String clientId,String lazzyClientName,String createdBy,String createdAt ){
		this.id = id;
		this.clientName = lazzyClientName;
		this.createdBy = createdBy;
		this.createdAt = createdAt;	
		this.clientId = clientId;
	}


	public String getId() {
		return id;
	}


	public String getClientId() {
		return clientId;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public String getCreatedAt() {
		return createdAt;
	}


	public String getClientName() {
		return clientName;
	}
		
	
}
