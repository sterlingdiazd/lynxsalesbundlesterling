package com.lynx.sales.bundle.entities;




public class LoginResult {

	private boolean logged;
	private UserEntity user;
	
	
	public LoginResult( boolean logged,String id,String name,String organization ){
		this.logged = logged;
		user = new UserEntity( id,name,organization );
	}
	
	public boolean isLogged(){
		return logged;
	}
	
	void setUser(UserEntity user) {
		this.user = user;
	}
	
	public UserEntity getUser() {
		return user;
	}
}


