package com.lynx.sales.bundle.entities;

import java.io.Serializable;

import android.text.TextUtils;

@SuppressWarnings("serial")
public class CenterEntity implements Serializable {

	private String name;
	private String description;
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private String store;
	private String docType;



	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public CenterEntity() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}
	
	public String toString() {
		return name + (!TextUtils.isEmpty( store ) ? ":"+store : "" );
	};
}
