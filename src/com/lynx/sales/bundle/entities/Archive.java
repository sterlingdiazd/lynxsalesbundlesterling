package com.lynx.sales.bundle.entities;


public class Archive {
	
	public enum Type{
		;
	};
		
	public enum Status{
		PENDING,PROCCESED,REJECTED
	};
	
	private int id;
	private String archiveType;
	private String owner;
	private String name;
	private String date;	
	
	private Archive(){
	}
	
	private Archive( Builder builder ){
		this.id  = builder.id;
		this.archiveType = builder.archiveType;
		this.owner = builder.owner;
		this.date = builder.date;
		this.name = builder.name;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public static Builder builder(){
		return new Builder();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getArchiveType() {
		return archiveType;
	}

	public void setArchiveType(String archiveType) {
		this.archiveType = archiveType;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setId(int id) {
		this.id = id;
	}




	/**
	 * Class That create Schedule jobs as assembly parts 
	 * 
	 * @author Jansel
	 *
	 */
	public static class Builder{
		int id;
		String archiveType;
		String owner;
		String date;
		String name;

		public Archive build(){
			return new Archive( this );
		}
		
		public Builder setArchiveType(String archiveType) {
			this.archiveType = archiveType;
			return this;
		}

		public Builder setOwner(String owner) {
			this.owner = owner;
			return this;
		}

		public Builder setDate(String date) {
			this.date = date;
			return this;
		}

		public Builder setId(int id) {
			this.id = id;
			return this;
		}	
		
		public Builder setName(String name) {
			this.name = name;
			return this;
		}

	}
}

