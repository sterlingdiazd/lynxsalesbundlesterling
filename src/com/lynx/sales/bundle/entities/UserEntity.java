package com.lynx.sales.bundle.entities;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class UserEntity implements Serializable {

	private String id;
	private String user;
	private String name;
	private String salesOrganization;
	private List<DocTypeEntity> docTypes;
	private List<CenterEntity> centers;
	private CenterEntity defaultCenter;
	private String cel;
	private String tel;
	
	public UserEntity() {
	}

	public UserEntity(String id, String name, String organization) {
		this.id = id;
		this.name = name;
		this.salesOrganization = organization;
	}

	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	
	public String getCel() {
		return cel;
	}

	public void setCel(String cel) {
		this.cel = cel;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSalesOrganization() {
		return salesOrganization;
	}


	public List<DocTypeEntity> getDocTypes() {
		return docTypes;
	}

	public void setDocTypes(List<DocTypeEntity> docTypes) {
		this.docTypes = docTypes;
	}

	public UserEntity(String id, String user, String name, String salesOrganization, List<DocTypeEntity> docTypes,
			List<CenterEntity> centers, CenterEntity defaultCenter, String cel, String tel) {
		super();
		this.id = id;
		this.user = user;
		this.name = name;
		this.salesOrganization = salesOrganization;
		this.docTypes = docTypes;
		this.centers = centers;
		this.defaultCenter = defaultCenter;
		this.cel = cel;
		this.tel = tel;
	}

	public List<CenterEntity> getCenters() {
		return centers;
	}

	public void setCenters(List<CenterEntity> centers) {
		this.centers = centers;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSalesOrganization(String salesOrganization) {
		this.salesOrganization = salesOrganization;
	}

	public CenterEntity getDefaultCenter() {
		return defaultCenter;
	}

	public void setDefaultCenter(CenterEntity defaultCenter) {
		this.defaultCenter = defaultCenter;
	}

	
	@Override
	public String toString() {
		return "{ id:"+id+",name:"+name+" }";
	}
}
