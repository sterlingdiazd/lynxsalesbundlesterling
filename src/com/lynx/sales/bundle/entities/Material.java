package com.lynx.sales.bundle.entities;

import java.util.List;

public class Material {

	private String matnr;
	private String maktx;
	private String vkorg;
	private String vrkme;
	private String meins;
	private String labst;
	private String werks;
	private String prodh;
	private String lgort;

	public Material(String matnr, String maktx, String vkorg, String vrkme, String meins, String labst, String werks,
			String prodh, String lgort, List<UnitEntity> unidades) {
		super();
		this.matnr = matnr;
		this.maktx = maktx;
		this.vkorg = vkorg;
		this.vrkme = vrkme;
		this.meins = meins;
		this.labst = labst;
		this.werks = werks;
		this.prodh = prodh;
		this.lgort = lgort;
		this.unidades = unidades;
	}
	
	public String getLgort() {
		return lgort;
	}
	
	public void setLgort(String lgort) {
		this.lgort = lgort;
	}
	private List<UnitEntity> unidades;
	

	public List<UnitEntity> getUnidades() {
		return unidades;
	}
	public void setUnidades(List<UnitEntity> unidades) {
		this.unidades = unidades;
	}
	public String getProdh() {
		return prodh;
	}
	public void setProdh(String prodh) {
		this.prodh = prodh;
	}
	public String getMatnr() {
		return matnr;
	}
	public void setMatnr(String matnr) {
		this.matnr = matnr;
	}
	public String getMaktx() {
		return maktx;
	}
	public void setMaktx(String maktx) {
		this.maktx = maktx;
	}
	public String getVkorg() {
		return vkorg;
	}
	public void setVkorg(String vkorg) {
		this.vkorg = vkorg;
	}
	public String getVrkme() {
		return vrkme;
	}
	public void setVrkme(String vrkme) {
		this.vrkme = vrkme;
	}
	public String getMeins() {
		return meins;
	}
	public void setMeins(String meins) {
		this.meins = meins;
	}
	public String getLabst() {
		return labst;
	}
	public void setLabst(String labst) {
		this.labst = labst;
	}
	public String getWerks() {
		return werks;
	}
	public void setWerks(String werks) {
		this.werks = werks;
	}




	
	
}
