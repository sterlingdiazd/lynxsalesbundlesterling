package com.lynx.sales.bundle.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@SuppressWarnings("serial")
public class ClientInfo implements Serializable{
	
	private String[] societies = new String[0];
	
	@SuppressWarnings("unchecked")
	private List< SalesArea > salesArea = Collections.EMPTY_LIST;
	
	public ClientInfo() {
	}
	
	public void setSalesArea(List<SalesArea> salesArea) {
		this.salesArea = salesArea;
	}
	
	public List<SalesArea> getSalesArea() {
		return salesArea;
	}
	
	 public void setSocieties(String[] societies) {
		this.societies = societies;
	}
	 
	 public String[] getSocieties() {
		return societies;
	}
	 
	 public List< String > getChannelAsResume() {			 
		 List< String > channels = new ArrayList<String>();
		 if( salesArea != null && !salesArea.isEmpty() ){			 
			 for( SalesArea area : salesArea ){
				 channels.add( area.getDistChannel() );
			 }
		 }
		return channels;
	 }
	 
	 
	 public List< String > getSalesResume(){
		 List< String > sales = new ArrayList<String>();
		 if( salesArea != null && !salesArea.isEmpty() ){
			 for( SalesArea area : salesArea ){
				 if( ! sales.contains( area.getSalesOrg() ) )
					 sales.add( area.getSalesOrg() );
			 }
		 }
		 return sales;
	 }
}
