package com.lynx.sales.bundle.entities;

import java.io.Serializable;
import java.util.List;


@SuppressWarnings("serial")
public class ConditionEntity implements Serializable{
	

	
	private String mandt;
	private String condTable;
	private String kschl;
	private String tableName;
	private String sequence;
	private String vkorg;
	private String vtweg;
	private String spart;
	private String kdgrp;
	private String brsch;
	private String prodh;
	private String kunnr;
	private String matnr;
	private String datbi;
	private String knumh;
	private String kbetr;
	private String knrmm;
	private String knrnm;
	private String knrme;
	private String knrzm;
	private String knrez;
	private String knrmat;
	
	
	
	public ConditionEntity() {
	}




	public ConditionEntity(String mandt, String condTable, String kschl, String tableName, String sequence,
			String vkorg, String vtweg, String spart, String kdgrp, String brsch, String prodh, String kunnr,
			String matnr, String datbi, String knumh, String kbetr, String knrmm, String knrnm, String knrme,
			String knrzm, String knrez, String knrmat) {
		super();
		this.mandt = mandt;
		this.condTable = condTable;
		this.kschl = kschl;
		this.tableName = tableName;
		this.sequence = sequence;
		this.vkorg = vkorg;
		this.vtweg = vtweg;
		this.spart = spart;
		this.kdgrp = kdgrp;
		this.brsch = brsch;
		this.prodh = prodh;
		this.kunnr = kunnr;
		this.matnr = matnr;
		this.datbi = datbi;
		this.knumh = knumh;
		this.kbetr = kbetr;
		this.knrmm = knrmm;
		this.knrnm = knrnm;
		this.knrme = knrme;
		this.knrzm = knrzm;
		this.knrez = knrez;
		this.knrmat = knrmat;
	}




	public String getMandt() {
		return mandt;
	}


	public void setMandt(String mandt) {
		this.mandt = mandt;
	}


	public String getCondTable() {
		return condTable;
	}


	public void setCondTable(String condTable) {
		this.condTable = condTable;
	}


	public String getKschl() {
		return kschl;
	}


	public void setKschl(String kschl) {
		this.kschl = kschl;
	}


	public String getTableName() {
		return tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getSequence() {
		return sequence;
	}


	public void setSequence(String sequence) {
		this.sequence = sequence;
	}


	public String getVkorg() {
		return vkorg;
	}


	public void setVkorg(String vkorg) {
		this.vkorg = vkorg;
	}


	public String getVtweg() {
		return vtweg;
	}


	public void setVtweg(String vtweg) {
		this.vtweg = vtweg;
	}


	public String getSpart() {
		return spart;
	}


	public void setSpart(String spart) {
		this.spart = spart;
	}


	public String getKdgrp() {
		return kdgrp;
	}


	public void setKdgrp(String kdgrp) {
		this.kdgrp = kdgrp;
	}


	public String getBrsch() {
		return brsch;
	}


	public void setBrsch(String brsch) {
		this.brsch = brsch;
	}


	public String getProdh() {
		return prodh;
	}


	public void setProdh(String prodh) {
		this.prodh = prodh;
	}


	public String getKunnr() {
		return kunnr;
	}


	public void setKunnr(String kunnr) {
		this.kunnr = kunnr;
	}


	public String getMatnr() {
		return matnr;
	}


	public void setMatnr(String matnr) {
		this.matnr = matnr;
	}


	public String getDatbi() {
		return datbi;
	}


	public void setDatbi(String datbi) {
		this.datbi = datbi;
	}


	public String getKnumh() {
		return knumh;
	}


	public void setKnumh(String knumh) {
		this.knumh = knumh;
	}


	public String getKbetr() {
		return kbetr;
	}


	public void setKbetr(String kbetr) {
		this.kbetr = kbetr;
	}


	public String getKnrmm() {
		return knrmm;
	}


	public void setKnrmm(String knrmm) {
		this.knrmm = knrmm;
	}


	public String getKnrnm() {
		return knrnm;
	}


	public void setKnrnm(String knrnm) {
		this.knrnm = knrnm;
	}


	public String getKnrme() {
		return knrme;
	}


	public void setKnrme(String knrme) {
		this.knrme = knrme;
	}


	public String getKnrzm() {
		return knrzm;
	}


	public void setKnrzm(String knrzm) {
		this.knrzm = knrzm;
	}


	public String getKnrez() {
		return knrez;
	}


	public void setKnrez(String knrez) {
		this.knrez = knrez;
	}


	public String getKnrmat() {
		return knrmat;
	}


	public void setKnrmat(String knrmat) {
		this.knrmat = knrmat;
	}


	
}
