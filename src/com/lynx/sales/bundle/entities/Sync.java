package com.lynx.sales.bundle.entities;

import java.io.Serializable;

public class Sync
  implements Serializable
{
  private String date;
  private String entity;
  private String id;
  private String status;
  
  public Sync() {}
  
  public Sync(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    this.id = paramString1;
    this.date = paramString2;
    this.entity = paramString3;
    this.status = paramString4;
  }
  
  public String getDate()
  {
    return this.date;
  }
  
  public String getEntity()
  {
    return this.entity;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setDate(String paramString)
  {
    this.date = paramString;
  }
  
  public void setEntity(String paramString)
  {
    this.entity = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setStatus(String paramString)
  {
    this.status = paramString;
  }
}

