package com.lynx.sales.bundle.entities;

import java.io.Serializable;

import android.text.TextUtils;

@SuppressWarnings("serial")
public class DocTypeEntity implements Serializable {

	private String docType;
	private String description;


	
	public DocTypeEntity() {
		super();
		// TODO Auto-generated constructor stub
	}



	public DocTypeEntity(String docType, String description) {
		super();
		this.docType = docType;
		this.description = description;
	}



	public String getDocType() {
		return docType;
	}



	public void setDocType(String docType) {
		this.docType = docType;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}

}
