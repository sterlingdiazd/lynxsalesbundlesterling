package com.lynx.sales.bundle.mn.connection;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public enum ConnectionState {
	CONNECTED,DISCONNECTED;
}
