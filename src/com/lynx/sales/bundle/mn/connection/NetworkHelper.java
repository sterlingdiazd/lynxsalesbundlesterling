package com.lynx.sales.bundle.mn.connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public final class NetworkHelper {

	
	private static boolean isWifiConnected(Context context) {
		ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = null;
		if (conManager != null) {
			netInfo = conManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		}
		return netInfo == null ? false : netInfo.isConnected();
	}

	
	private static boolean isMobileConnected(Context context) {
		ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = null;
		if (conManager != null) {
			netInfo = conManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		}
		return netInfo == null ? false : netInfo.isConnected();
	}

	
	
	public static boolean isDataOperational(Context context) {
		return isWifiConnected(context) || isMobileConnected(context);
	}

}
