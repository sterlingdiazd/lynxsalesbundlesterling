package com.lynx.sales.bundle.mn.connection;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
/**
 * This class provides an easy way to track the network status of the device.
 * 
 * @see ConnectionListener
 * 
 * @since 1.0
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public final class ConnectionManager {

	private static class ConnectionBroadcast extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {			
			ConnectionState state = ConnectionState.DISCONNECTED;
			
			if( NetworkHelper.isDataOperational( context ) )
				state = ConnectionState.CONNECTED;				
			
			notifyListeners( state );
		}
	}
	
	private static List< ConnectionListener > listeners;
	
	private static ConnectionBroadcast broadcast;
		
	
	static{
		broadcast = new ConnectionBroadcast();
		listeners = new ArrayList< ConnectionListener >();
	}
	
	
	/**
	 * This method is needed to be called on activity or application start up time.
	 * 
	 * @param context
	 */
	public static void initWatch( Context context ){
		IntentFilter filter = new IntentFilter( ConnectivityManager.CONNECTIVITY_ACTION );
		context.registerReceiver(broadcast, filter);
	}
	
	
	/**
	 * This method is needed to be called on activity or application shutdown time ,to avoid linckage. 
	 * @param context
	 */
	public static void stopWatch( Context context ){
		context.unregisterReceiver(broadcast);
	}
	
	
	
	public static void registerConenctionListener( ConnectionListener listener ){
		listeners.add(listener);
	}
	
	
	public static void unregisterConnectionListener( ConnectionListener listener ){
		listeners.remove( listener );
	}
	
	
	private static void notifyListeners( ConnectionState state ){
		for( ConnectionListener listener : listeners ){
			listener.onConnectionChange( state );
		}
	}
	
}
