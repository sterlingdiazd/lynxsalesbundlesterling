package com.lynx.sales.bundle.mn.connection;
/**
 * 
 * <p>
 * So the device can be in connection network or not, 
 * this class provides an interface to receive notification 
 * in the status change of the network device
 * </p>
 * 
 * <blockquote>
 * <pre>
 * 	ConnectionListener listener = new ConnectionListener(){
 * <code>@override</code>
 * public  void onConnectionChange( ConnectionState state ){
 * if( ConnectionState.CONNECTED == state ){
 * ...
 * }
 * else{
 * ...
 * }
 * }
 * }
 * ConnectionManager.registerConenctionListener( listener );
 * </pre>
 * </blockquote>
 * 
 * @see ConnectionManager
 * 
 * @since 1.0
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public abstract interface ConnectionListener {
	public abstract void onConnectionChange( ConnectionState state );
}
