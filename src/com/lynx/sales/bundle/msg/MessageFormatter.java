package com.lynx.sales.bundle.msg;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.lynx.sales.bundle.R;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class MessageFormatter {

	private static  MessageFormatter instance;
	private Context context;
		
	
	private MessageFormatter( Context context ) {
		this.context = context;			
	}
	
	
	public static MessageFormatter getInstance( Context cxt ){
		if( instance != null )
			return instance;
		
		synchronized( MessageFormatter.class ){
			instance = new MessageFormatter( cxt );
		}	
		return  instance;
	}
	

	public Message format( String message ){
		Map< String,String  > keyValues = extractResponseKeys( message );
		return new DefaultMessageHandler().getMessage(keyValues);				
	}
		
	
	
	public Message format( String message,FilterHandler filter ){
		Map< String,String > keyValues = extractResponseKeys(message);
		
		Log.e("*********", "keyValues: "+keyValues.toString() );
		String target  = keyValues.get( "message" );
		String code    = keyValues.get( "type" );
		
		if( null != filter && filter.accept(target, code) ){
			return filter.getHandler().getMessage(keyValues);
		}
		
		return new DefaultMessageHandler().getMessage(keyValues);
	}
	
	
	
	private Map< String,String > extractResponseKeys( String message ){
		Map< String,String > keyValues = new HashMap<String, String>();
	
		message = message.replaceAll("\"", "").replaceAll( "\\{", "").replaceAll("\\}", "");
		
		for( String set: message.split( "," ) ){		
			String pair[] = set.split( ":" );
			keyValues.put(pair[0],pair[1]);
		}		
		
		return keyValues;
	}	
	
	

	public static class Message{
		public final String type;
		public final String message;
		public final String subject;		
	
		
		public Message(  String message, String type,String subject ){
			this.type = type;
			this.message = message;
			this.subject = subject;
		}
		
		@Override
		public String toString() {
			return subject;
		}
	}
	
	
	public static abstract class FilterHandler{
		public abstract boolean accept( String message,String type );
		public MessageHandler getHandler(){
			return null;
		}
	}
	
	
	
	public static abstract class MessageHandler{
		public abstract Message getMessage( Map<String,String> keyValues );
	}
	
	
	private  class DefaultMessageHandler extends MessageHandler{
		
		public static final String SUCCESS 	= "S";	
		public static final String ERROR   	= "E";	
		public static final String INFO    	= "I";
		
		private  HashMap<String, HashMap<String, String> > messages = new HashMap<String, HashMap<String,String>>();
		
		public DefaultMessageHandler(){
			messages.put( SUCCESS, new HashMap<String, String>() );
			messages.put( ERROR,   new HashMap<String, String>() );
			messages.put( INFO,    new HashMap<String, String>() );	
			
			initSuccess();
			initInfo();
			initErrors();				
		}		
		
		@Override
		public Message getMessage(Map<String, String> keyValues) {			
			String msg = messages.get( keyValues.get( "type" ) ).get( keyValues.get( "message" ) );			
			return new Message( keyValues.get( "message" ), keyValues.get( "type" ),msg );
		}
		
		private  void initSuccess(){
			HashMap<String, String > success = messages.get(SUCCESS);
			success.put( "001", context.getResources().getString( R.string.S_001 ) );
		}
		
		
		private  void initErrors(){
			HashMap<String, String > errors = messages.get(ERROR);
			errors.put( "001", context.getResources().getString( R.string.E_001 ) );
			errors.put( "002", context.getResources().getString( R.string.E_002 ) );
			errors.put( "003", context.getResources().getString( R.string.E_003 ) );
			errors.put( "004", context.getResources().getString( R.string.E_004 ) );
		}
		
		
		private  void initInfo(){
			HashMap<String, String > info = messages.get(INFO);
			info.put( "001", context.getResources().getString( R.string.I_001 ) );
			info.put( "002", context.getResources().getString( R.string.I_002) );		
		}			
	}		

}


















