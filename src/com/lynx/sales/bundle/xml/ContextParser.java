package com.lynx.sales.bundle.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import android.content.Context;

import com.lynx.sales.bundle.db.Query;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public abstract class ContextParser {

	protected InputStream stream;
	
	public ContextParser( Context context,int rawSource ) throws IOException{
		stream = context.getResources().openRawResource(rawSource);
	}
	
	public abstract List< Query > parse() throws IOException;	
}
