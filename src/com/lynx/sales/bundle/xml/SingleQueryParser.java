package com.lynx.sales.bundle.xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import android.content.Context;
import android.sax.Element;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;

import com.lynx.sales.bundle.db.Query;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class SingleQueryParser extends ContextParser{
	
	public static final long UNBOUNDED_DEPTH = 1L << 7;
	
	private Query query;
	private long depth = 1;
    private long partialDepth = 1;
    private boolean sttoped;
	
	
	public SingleQueryParser( Context context,int resource ) throws IOException{
		super( context,resource );
	}
	
	@Override
	public List<Query> parse() throws IOException {
		
		final ArrayList< Query > queries = new ArrayList<Query>();
		
		RootElement context = new RootElement("query-selector");
		Element element = context.getChild("queries");
		
		element.setStartElementListener( new StartElementListener() {			
			@Override
			public void start(Attributes arg0) {
				if( partialDepth++ > depth && depth <= UNBOUNDED_DEPTH )
					sttoped = true;
				/**
				 * This could be carrying problem if and only if the handler caller,
				 * call this method for the last element and when the depth is changed to UNBOUNDED_DEPTH 							
				 */
			}
		});
		
		element.getChild( "query" ).setEndTextElementListener( new EndTextElementListener() {			
			@Override
			public void end(String statement ) {
				if( !sttoped ) {
					query = new Query( statement );					
					queries.add( query );
					query = null;
				}
			}
		});		
		
		try {			
			Xml.parse(stream, Xml.Encoding.UTF_8, context.getContentHandler() );			
		} catch (SAXException e) {
			e.printStackTrace();
		}
		
		return queries;
	}
	
	
	public long getDepth() {
		return depth;
	}
	
	public void setDepth(long depth) {
		this.depth = depth;
	}
}
