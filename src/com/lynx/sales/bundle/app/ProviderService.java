package com.lynx.sales.bundle.app;

import org.lyxar.envelope.Broker;

/**
 * <p>
 * Since application identify it's suited broker, api recognized that,
 * and allow to you to retrieve that broker by only call {@link #getSystemProvider()}
 * </p>
 * <p>
 * Handled broker is registered by <code>application</code> at start up  
 * time, and detached in shutdown time, that's mean, if you register self
 * own broker to be a candidate to convey as system broker, you need place registration
 * statement in place that are reached at system start up time.
 * </p>
 * @since 1.0
 *   
 * @author Jansel V. Vanwolf
 *
 */
public class ProviderService {
	private static final ProviderService INSTANCE = new ProviderService();
	
	private Broker defaultBroker;
	
	private ProviderService(){		
	}
	
	public static ProviderService getInstance(){
		return INSTANCE;
	}
	
	public void register( Broker broker ){
		this.defaultBroker = broker;
	}	
	
	public Broker getSystemProvider(){
		return  defaultBroker;
	}
}
