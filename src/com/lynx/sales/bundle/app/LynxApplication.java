package com.lynx.sales.bundle.app;


import org.lyxar.envelope.Broker;
import org.lyxar.envelope.sp.BrokersServices;

import android.app.Application;
import android.text.TextUtils;

import com.lynx.sales.bundle.dto.model.UserRetrieval;
import com.lynx.sales.bundle.dto.prefs.BrokersPreferences;
import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.lynx.sales.bundle.entities.UserEntity;
import com.testflightapp.lib.TestFlight;
/**
 * <p>
 * General purpose application entity that represent running 
 * application state.
 * </p>
 * <p>
 * 	The application can be retrieved by only casting the managed
 *  android instance, that is, the application registered in
 *  <code>AndroidManifest.xml</code> file.  
 * </p>
 * <blockquote>
 * <pre>
 * LynxApplication app = (LynxApplication) context.getApplication();
 * </pre>
 * </blockquote>
 * 
 * <p>
 * Once you retrieve the lynx application you can get user logged to application
 * calling the {@link #getUser()} method, that return instance of {@link UserEntity}
 * </p>
 * <p>
 * application entity try to load the application configured default provider, after
 * the provider is loaded is placed as shared instance in {@link ProviderService} and you can 
 * retrieve it calling {@link ProviderService#getSystemProvider()} after call singleton 
 * method {@link ProviderService#getInstance()} 
 * </p>
 * <p>
 * It's completely possible register self own {@link Broker} only 
 * calling {@link ProviderService#register(Broker)}, but that not make sense
 * since the intention of the api to auto handle all of your possible brokers. 
 * </p>
 * 
 * @see UserEntity
 * @see ProviderService
 * 
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public class LynxApplication extends Application {

	private UserEntity user;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		if( UserPreferences.isSynchronized(getApplicationContext())){
			if (user == null ){
				user = getDiskUser();
			}		
		}
		
		if( BrokersPreferences.isBrokersResolved(getApplicationContext())){			
			String defaultBroker = BrokersPreferences.getDefaultBrokerService(getApplicationContext());
			if( !TextUtils.isEmpty(defaultBroker) ){
				Broker broker = BrokersServices.defaultBrokerService(getApplicationContext()).getBrokerById( defaultBroker );
				ProviderService.getInstance().register(broker);
			}
		}
		
		TestFlight.takeOff(this, "6ddce307-caf0-4e02-b787-1a95a4688b9c");
	}

	public UserEntity getUser() {
		return user;
	}
	
	public void setUser(UserEntity user) {
		this.user = user;
	}
	
	public void reloadUser(){
		this.user = getDiskUser();
	}
	
	public UserEntity getDiskUser(){
		return UserRetrieval.getUser(getApplicationContext());
	}
}
