package com.lynx.sales.bundle.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.lynx.sales.bundle.provider.agent.ProviderAgent;
import com.lynx.sales.bundle.provider.agent.sp.ContentDelegates;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class SalesContentProvider extends ContentProvider{
		
	private ProviderAgent agent;
	
	@Override
	public boolean onCreate() {		
		agent = NULL_PROVIDER;
		return true;
	}		
	
	
	@Override	
	public int delete(Uri  uri, String selection, String[] selectionArgs) 
	{				
		if(agent == null) 
		{
			agent = ContentDelegates.defaultDelegate(getContext()).lookup( uri.getPath() ); 
		}
		else 
		{
			if( !agent.resolve(uri) )
				agent = ContentDelegates.defaultDelegate(getContext()).lookup( uri.getPath() ); 
		}
		
		int count = 0;
		
		if(agent != null) 
		{
			count = agent.delete( uri, selection,selectionArgs );
		}
		
		
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}
		
	
	
	@Override
	public String getType(Uri uri) {
		if( !agent.resolve(uri) )
			agent = ContentDelegates.defaultDelegate(getContext()).lookup( uri.getPath() );
		return   agent.getType( uri );
	}	
	

	
	@Override
	public Uri insert(Uri uri, ContentValues values) {		
		
		if(agent == null) 
		{
			agent = ContentDelegates.defaultDelegate(getContext()).lookup( uri.getPath() ); 
		} 
		else 
		{
			if( !agent.resolve(uri) )
				agent = ContentDelegates.defaultDelegate(getContext()).lookup( uri.getPath() ); 
		}
		
		Uri inserted = agent.insert( uri,values );
		
		getContext().getContentResolver().notifyChange( inserted, null);
		return inserted;
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments,String sortOrder) 
	{		
		Cursor cursor = null;
		/*
		boolean resolved = agent.resolve(uri);
		if( !resolved )
		{
		*/
			agent = ContentDelegates.defaultDelegate(getContext()).lookup( uri.getPath() ); 
			if(agent == null)
			{
				Log.e("cursor null agent", uri.getPath());
			} else {
				cursor = agent.query(uri, projection, selection, selectionArguments, sortOrder);
			}			
		//}		
		return cursor;
	}
	
	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] seelctionArgs) {
		if( !agent.resolve(uri) )
			agent = ContentDelegates.defaultDelegate(getContext()).lookup( uri.getPath() ); 
		
		int updated = agent.update(uri, values, selection, seelctionArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		
		return updated;
	}

	private static final ProviderAgent NULL_PROVIDER = new ProviderAgent() {
		@Override
		public int update(Uri uri, ContentValues values, String selection,String[] seelctionArgs) {
			return 0;
		}		
		@Override
		public boolean resolve(Uri uri) {
			return false;
		}		
		@Override
		public Cursor query(Uri uri, String[] projection, String selection,
				String[] selectionArguments, String sortOrder) {
			return null;
		}		
		@Override
		public Uri insert(Uri uri, ContentValues values) {
			return null;
		}		
		@Override
		public String getType(Uri uri) {
			return null;
		}		
		@Override
		public int delete(Uri uri, String selection, String[] seelctionArgs) {
			return 0;
		}
		@Override
		public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments,
				String sortOrder, int start, int limit) {
			// TODO Auto-generated method stub
			return null;
		}
	};
}

