package com.lynx.sales.bundle.provider;

import android.net.Uri;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public final class SalesContract {

	
	public static final class Sales{
		
		public static final String  PROVIDER_NAME			= 		SalesContentProvider.class.getCanonicalName();
		
		public static final String  PROVIDER_AUTHORITY		=		"com.lynx.sales.bundle.authority.sales.provider";
		
		public static final Uri 	CONTENT_URI				= 		Uri.parse( "content://" + PROVIDER_AUTHORITY );		
	}
		
}
