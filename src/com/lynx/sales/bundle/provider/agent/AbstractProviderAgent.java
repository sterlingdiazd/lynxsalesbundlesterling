package com.lynx.sales.bundle.provider.agent;

import android.content.Context;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf)
 *
 */
public abstract class AbstractProviderAgent implements ProviderAgent{
	
	protected Context context;
	
	public AbstractProviderAgent( Context context ){
		this.context = context;
	}
	
	public Context getContext() {
		return context;
	}
	
	public void setContext(Context context) {
		this.context = context;
	}
}
