package com.lynx.sales.bundle.provider.agent;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
/**
 * <p>
 * The content providers are an essential part of the process of 
 * storage and therefore requires special attention in implementation .
 * </p>
 * <p>
 * Whenever required to create a new content provider class must be added
 * to handle the responsibility of representing the different URIs 
 * <code>agent.properties</code> configuration file , so that the API can identify 
 * which class is responsible for managing certain types of URIs.
 * </p>
 * <p>
 * The identification of which class is most appropriate to resolve a request , 
 * is the first of all registered will return true when calling the {@link ProviderAgent#resolve(Uri)} method.
 * </p>
 * <p>
 * The configuration file maps the {@link ProviderAgent} <code>agent.properties</code> 
 * specific for each set of URIs , the proper way to mappear different URIs to one 
 * {@link ProviderAgent} is separating each URI fragment by "; " then the special 
 * characters in the URI are escaped by "\" .
 *</p>
 *<p> 
 * URI fragments can contain regular expressions to indicate separation on other URIs , 
 * the API might be able to resolve these regular expressions and call the appropriate {@link ProviderAgent} .
 * </p>
 * <p>
 * As a note, each AgentProvider must return true or false if it is possible to resolve 
 * the URI that was requested , so you need to maintain a matching between URIs that could accept.
 * </p>
 * <p>This is he example of a registration of user {@link ProviderAgent}</p>
 * <blockquote>
 * <pre>
 * /users;/users/{id\:(\\d+)}=com.own.UserAgentProvider
 * </pre>
 * </blockquote>
 * 
 * @see AbstractProviderAgent
 * 
 * @since 1.0
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public abstract interface ProviderAgent {

	public abstract boolean resolve( Uri uri );
	
	
	public abstract String getType(Uri uri);
	
	
	public abstract Uri insert(Uri uri, ContentValues values);
	
	
	public  abstract int update(Uri uri, ContentValues values, String selection, String[] seelctionArgs);
		
	
	public abstract Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments,String sortOrder);
	
	public abstract Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments,String sortOrder, int start, int limit);

	
	public abstract int delete(Uri  uri, String selection, String[] seelctionArgs);
	
}
