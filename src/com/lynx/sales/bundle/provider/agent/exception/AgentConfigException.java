package com.lynx.sales.bundle.provider.agent.exception;

import java.util.MissingResourceException;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
@SuppressWarnings("serial")
public class AgentConfigException extends MissingResourceException{
	
	public AgentConfigException( String msg,String className,String resource ){
		super( msg, className,resource ); 
	}
	
}
