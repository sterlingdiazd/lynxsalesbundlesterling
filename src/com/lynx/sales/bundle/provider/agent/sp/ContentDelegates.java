package com.lynx.sales.bundle.provider.agent.sp;

import android.content.Context;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class ContentDelegates {
	
	private static ContentDelegate instance;
	
	public static ContentDelegate defaultDelegate( Context cxt ){		
		synchronized ( ContentDelegates.class ) {
			if( null != instance )
				return instance;
		}
		return ( instance = new rowk.lynx.sales.bundle.provider.agent.KContentDelegate( cxt ) );		
	}
	
}
