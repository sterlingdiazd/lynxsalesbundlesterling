package com.lynx.sales.bundle.provider.agent.sp;

import com.lynx.sales.bundle.provider.agent.ProviderAgent;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public abstract class ContentDelegate {

	public abstract ProviderAgent lookup(  String descriptor );
	
}
