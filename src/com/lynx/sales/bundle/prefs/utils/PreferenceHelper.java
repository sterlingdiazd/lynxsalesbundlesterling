package com.lynx.sales.bundle.prefs.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public final class PreferenceHelper {

	
	public static void setBoolean(Context context,String preference, String key,Boolean value ){
		getEditor(context, preference).putBoolean(key, value).commit();
	}
		
	public static boolean getBoolean(Context context,String preference, String key ){
		SharedPreferences prefs = context.getSharedPreferences(preference, Context.MODE_PRIVATE );
		return prefs.getBoolean(key, false);
	}
	
	
		
	public static void setInt(Context context,String preference, String key,int value ){
		getEditor(context, preference).putInt(key, value).commit();
	}
	
	public static int getInt( Context context,String preference, String key ){
		SharedPreferences prefs = context.getSharedPreferences(preference, Context.MODE_PRIVATE );
		return prefs.getInt(key, -1 );		
	}


	public static void setString(Context context,String preference, String key,String value ){
		getEditor(context, preference).putString(key, value).commit();
	}
	
	public static String getString( Context context,String preference, String key ){
		SharedPreferences prefs = context.getSharedPreferences(preference, Context.MODE_PRIVATE );
		return prefs.getString(key, "" );		
	}
	
	
	
	public static void invalidate( Context context, String preference, String key ){
		getEditor(context, preference).remove(key).commit();
	}
	
	
	private static Editor getEditor( Context context,String preference ){
		SharedPreferences prefs = context.getSharedPreferences( preference, Context.MODE_PRIVATE );
		return prefs.edit();				
	}

}
