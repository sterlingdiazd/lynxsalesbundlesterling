package com.lynx.sales.bundle.http.request;

import java.util.Date;

import android.content.Context;

import com.lynx.sales.bundle.dto.model.ScheduleDataModel;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.prefs.utils.PreferenceHelper;

public class ScheduleLocalTaskRequest extends Requestor{

	private ScheduleJob.Type type;
	private String responsible;
	private  String guardLimit;
	private String networkMode;
	private int jobID;
	private Context context;
	
	public ScheduleLocalTaskRequest(Context cxt, PhaseHandler handler) {
		super(cxt, handler);
		context = cxt;
		type = ScheduleJob.Type.BILL;
		responsible = "";
	}
	
	public ScheduleLocalTaskRequest( Context cxt,String responsible,ScheduleJob.Type type, String guardLimit, String networkMode, PhaseHandler handler ){
		this( cxt,handler );
		this.responsible = responsible;
		this.type = type;
		this.guardLimit = guardLimit;
		this.networkMode = networkMode;
	}
	

	@Override
	protected ResponseStub handleRequest(ServiceStub stub, String... params) throws ContextException {
		
		String strUrl = stub.getUrl();
		if( params != null && params.length >0 )			
			strUrl += stub.resolveParams(params);
		
		ScheduleJob job = ScheduleJob.builder()
				.setDate( new Date().toString() )
				.setQuery( strUrl )
				.setType( type )
				.setResponsible( responsible )
				.setStatus( ScheduleJob.Status.PENDING )
				.setGuardLimit(guardLimit)
				.setNetworkMode(networkMode)
				.build();
		
		jobID = ScheduleDataModel.saveScheduleJob( getContext(), job);
		PreferenceHelper.setString(context, "preferences", "jobID", jobID+"");
		return null;
	}

	public int getJobID() {
		return jobID;
	}

	
	

}
