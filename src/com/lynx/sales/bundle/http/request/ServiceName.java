package com.lynx.sales.bundle.http.request;


/**
 * Names of actual registered services names
 * 
 * @see Requestor
 * @see ServiceInjector 
 * @see ServiceLocator
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public enum ServiceName {
	LOGIN{
		@Override
		public String toResource() {
			return "/login";
		}
	},
	ITINERARY{
		@Override
		public String toResource() {
			return "/itinerary";
		}
	},
	PRODUCTS{
		@Override
		public String toResource() {
			return "/products";
		}
	},
	PRODUCT_THUMB{
		@Override
		public String toResource() {
			return "/product_thumb";
		}
	},
	ORDER_CREATION{
		@Override
		public String toResource() {
			return "/create_order";
		}
	},
	SELLER_INFO{
		@Override
		public String toResource() {
			return "/seller_info";
		}
	},
	CLIENT_INFO{
		@Override
		public String toResource() {
			return "/client_info";
		}
	},
	CLIENT_SALES_AREA{
		@Override
		public String toResource() {
			return "/client_sales_area";
		}
	},
	COMPANY { 
		@Override
		public String toResource() {
			return "/company_info";
		}
	},
	ITINERARY_CLIENTS { 
		@Override
		public String toResource() {
			return "/itinerary_clients";
		}
	},
	PRODUCTS_BY_CLIENT { 
		@Override
		public String toResource() {
			return "/producs_by_client";
		}
	},
	CUSTOMER { 
		@Override
		public String toResource() {
			return "/customer";
		}
	},
	DELETE { 
		@Override
		public String toResource() {
			return "/delete";
		}
	}, 
	PREFERENCES { 
		@Override
		public String toResource() {
			return "/preferences";
		}
	},
	CONDITIONS { 
		@Override
		public String toResource() {
			return "/conditions";
		}
	};
	
	public abstract String toResource();
	
}
