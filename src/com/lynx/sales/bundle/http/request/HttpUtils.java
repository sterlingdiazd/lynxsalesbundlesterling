package com.lynx.sales.bundle.http.request;

import java.util.Arrays;

/**
 * <p>
 * 	Since URL are a sensitive fragment, the execution of that part must
 *  accomplish with the standard industry to allow execution, see <a href="http://tools.ietf.org/html/rfc3986">RFC3986</a>
 *  for more detail, the url must be encoded and decoded as needed.
 * </p>
 * <p>
 * 	If some error is raised while the parse execution, then <code>exception</code> is raised.
 * </p>
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public class HttpUtils {

    private static String keychar[] = { " ", "\\|" };
    private static String codechar[] = { "+", "%7c" };
    
    /**
     * This method map a string to key value pairs by some literal as specificaion.
     * <blockquote>
     * <pre>
     *  String query = "key1=?&key2=?";
     *  String formatted = matchSymbol( query,"&",new String[]{ "value1","value2" } );
     *  System.out.println( formatted ); //key1=value1&key2=value2 
     * </pre>
     * </blockquote>
     * @param source the string to be parsed
     * @param symbol the character that specify end on key value pair
     * @param values values to be attached to values.
     * @return
     */ 
    public static String matchSymbol(String source, String symbol,
                    String[] values) {

            String[] params = source.split(symbol);
            for (int i = 0; i < params.length; ++i) {
                    params[i] = params[i].trim();
                    params[i] = params[i].replaceAll("\\?", values[i]);
            }
            return Arrays.asList(params).toString().replaceAll("^\\[|\\]$", "").replaceAll("(, |,| ,| , )", "&");
    }

    /**
     * Encode url to have special characters mapped to recognized by standard
     * 
     * @param url to be encoded
     * @return url encoded
     */
    public static String URLEncode(String url) {
            String result = url;
            for (int i = 0; i < keychar.length; ++i)
                    result = result.replaceAll(keychar[i], codechar[i]);
            return result;
    }

    /**
     * Decode url character from url 
     * 
     * @param url to be decoded
     * @return url decoded
     */
    public static String URLDecode(String url) {
            String result = url;
            for (int i = 0; i < keychar.length; ++i)
                    result = result.replaceAll(codechar[i], keychar[i]);
            return result;
    }
}