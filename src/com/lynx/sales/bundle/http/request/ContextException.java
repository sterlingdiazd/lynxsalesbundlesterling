package com.lynx.sales.bundle.http.request;

/**
 * <p>
 * While network process can raised problem, when this occurs inside of 
 * {@link Requestor} context that error is mapped to {@link ContextException} 
 * for the system know which problem has occurred.
 * </p>
 * <p>
 * 	This <code>exception</code> must be raised, if you <code>override<code> 
 * {@link Requestor#handleRequest(ServiceStub, String...)} method to handle some specific 
 *  data fetch process,then the exception is handled by {@link Requestor#request(ServiceStub, String...)}
 *  calling the appropriated lifecycle method {@link PhaseHandler#onError(ErrorStub)} on registered instance of {@link PhaseHandler}.
 * </p>
 * 
 * @see Requestor
 * @see PhaseHandler
 * @see ServiceStub
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
@SuppressWarnings("serial")
public class ContextException extends Exception{
	private String reason;
	
	public ContextException( String reason ){
		super( reason );
		this.reason  = reason;
	}
	
	public String getReason(){
		return reason;
	}
}
