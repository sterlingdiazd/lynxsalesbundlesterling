package com.lynx.sales.bundle.http.request;

import android.content.Context;
import android.os.AsyncTask;

/**
 * <p>
 *  Support for large congestion process is intent of this class.
 *  All class that need to be managed by the system as requestor process, must extend directly
 *  or indirectly {@link Requestor} class.
 * </p>
 * <p>
 *  Once {@link Requestor} is created is ready to use and receive execute call, note that 
 *  {@link Requestor#request(ServiceStub, String...)} could receive multiple calls. when {@link PhaseHandler}
 *  take place.
 * </p>
 * 
 * <blockquote>
 * <pre>
 *  Requestor requestor = new RequestorImp( context, new PhaseHandler(){
 *  ...
 *  });
 *  requestor.request( serviceStub,params);
 * </pre>
 * </blockquote>
 * 
 * @see PhaseHandler
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public abstract class Requestor {	
	
	protected PhaseHandler handler;
	private Context context;
	
	private static final ResponseStub NULL_RESPONSE = new ResponseStub("NULL");
	
	public Requestor( Context cxt,PhaseHandler handler ){
		this.context = cxt;
		this.handler = handler;
	}
		
	public Context getContext() {
		return context;
	}
	
	public void request( final ServiceStub stub,String...params ){
		
		new AsyncTask< String, Void, ResponseStub >() {
			
			@Override
			protected void onPreExecute() {
				if( handler != null )
					handler.onBegan();
			};
			
			@Override
			protected ResponseStub doInBackground(String... params) {
				try{
					return handleRequest(stub, params);
				}catch( ContextException ce ){
					if( handler != null )
						handler.onError( new ErrorStub( ce.toString() ) );
				}
				return NULL_RESPONSE;
			}
			
			@Override
			protected void onPostExecute(ResponseStub result) {
				if( handler != null && NULL_RESPONSE != result )
					handler.onCompleted( result );
			};
			
		}.execute(params);
	}	
	
	
	protected abstract ResponseStub handleRequest( ServiceStub stub,String...params ) throws ContextException;
}
