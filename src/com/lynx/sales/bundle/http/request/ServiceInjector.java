package com.lynx.sales.bundle.http.request;

import java.util.Map;

import org.lyxar.envelope.Broker;
import org.lyxar.envelope.Service;
import org.lyxar.envelope.sp.BrokerService;

import android.util.Log;

import com.lynx.sales.bundle.app.ProviderService;
import com.lynx.sales.bundle.http.request.ServiceStub.ParamResolver;

/**
 * <p>
 * Since you need to have accessible services on {@link BrokerService}
 * you need to register, as service that you can letter retrieve as 
 * handled services.
 * </p>
 * <p>
 * You can access from {@link Broker} to all services loaded, but the most scalable
 *  way to do this is to register the services that can be served by a 
 *  provider in specific, then it is possible to recover these services 
 *  through {@link ServiceLocator}.
 * </p>
 * <p>
 * You can't inject services outside of this class, since the services injected
 * need to be handled by broker registered, then you need define some constants in
 * {@link ServiceName} and register with {@link ServiceInjector#registerService(Map, ServiceName, Broker)}
 * </p>
 * 
 * @see Broker
 * @see ServiceLocator
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
final class ServiceInjector {
	
	
	public static void injectServices( Map<ServiceName,ServiceStub> services ){
		Broker broker = ProviderService.getInstance().getSystemProvider();

		registerService(services, ServiceName.COMPANY, broker);
		registerService(services, ServiceName.LOGIN, broker);
		registerService(services, ServiceName.ITINERARY, broker);
		registerService(services, ServiceName.ORDER_CREATION, broker);
		registerService(services, ServiceName.ITINERARY_CLIENTS, broker);
		//registerService(services, ServiceName.PRODUCTS_BY_CLIENT, broker);
		registerService(services, ServiceName.PRODUCTS, broker);
		registerService(services, ServiceName.SELLER_INFO, broker);
		registerService(services, ServiceName.CUSTOMER, broker);
		registerService(services, ServiceName.CLIENT_INFO, broker);
		registerService(services, ServiceName.CLIENT_SALES_AREA, broker);
		registerService(services, ServiceName.PRODUCT_THUMB, broker);
		registerService(services, ServiceName.DELETE, broker);
		registerService(services, ServiceName.CONDITIONS, broker);

	}	
	
	
	private static void registerService( Map<ServiceName,ServiceStub> services,ServiceName serviceName,Broker broker ){
		
		final Service service = broker.getService( serviceName.toResource());
		
		//Log.e( "***********", "Searching...."+serviceName+"="+serviceName.toResource() );
		//Log.e( "**********", "NULLLLLLL: "+broker+","+broker.getHost()+","+service.getPath()+","+service.getURLString() );
		
		if(service != null)
		{
			String url = broker.getHost()+service.getPath()+service.getURLString()+"?";
			
			services.put( serviceName, new ServiceStub( url, new ParamResolver() {		
				@Override
				public String resolveParams(String... params) {
					return HttpUtils.matchSymbol( service.getQueryString() ,"&", params );	
				}
			}));	
		}
				
	}
}
