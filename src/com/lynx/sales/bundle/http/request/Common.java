package com.lynx.sales.bundle.http.request;

import java.util.Collection;

public final class Common {

	public static final class Columns{
		
		public static final String ID 					= "id";
		public static final String EXTERNAL_ID 			= "external_id";
		public static final String NAME 				= "name";
		public static final String SALES_ORG 			= "sales_organization";
		public static final String DOC_TYPE 			= "doc_type";	
		public static final String ID_USER 				= "id_user";
		public static final String ID_ROUTE				= "route_id";
		public static final String STORE 				= "store";
		public static final String CREATION_DATE 		= "creation_date";
		public static final String SELLER_ID			= "seller_id";
		public static final String DATE_FROM 			= "date_from";
		public static final String DATE_TO 				= "date_to";
		public static final String DAY_OF_WEEK 			= "day_of_week";
		public static final String CREATED_BY 			= "created_by";
		public static final String MODIFIED_BY 			= "modified_by";
		public static final String MODIFIED_HOUR 		= "modified_hour";
		public static final String DESCRIPTION 			= "description";		
		public static final String CREATED_DATE 		= "created_date";
		public static final String ITINERARY_ID 		= "itinerary_id";
		public static final String ADDRESS_1	 		= "address1";
		public static final String ADDRESS_2	 		= "address2";
		public static final String PHONE		 		= "phone";
		public static final String LOCATION		 		= "location";
		public static final String CREDIT		 		= "credit";
		public static final String OPEN			 		= "open";
		public static final String AVAILABLE	 		= "available";
		public static final String CURRENT_DATE 		= "current_date";
		public static final String SOCIETIES 			= "societies";
		public static final String SALES_ORG_2 			= "sales_org";
		public static final String DIST_CHANNEL			= "dist_channel";
		public static final String SECTOR 				= "sector";
		public static final String PAYMENT_COND 		= "payment_cond";
		public static final String INTERLOCUTORS 		= "interlocutors";
		public static final String THUMB 				= "thumb";
		public static final String INTERLOCUTOR 		= "interlocutor";
		public static final String SOCIETY 				= "society";
		public static final String DIST_CHANNEL_PRODUCTS= "distribution_channel";
		public static final String PRICE 				= "price";
		public static final String SALES_UNIT 			= "sales_unit";
		public static final String QUANTITY_IN_SALES 	= "quantity_in_sales_unit";
		public static final String UNIT_BASE 			= "unit_base";
		public static final String STOCK 				= "stock";
		public static final String OFFER 				= "offer";
		public static final String CENTERS 				= "centers";
		public static final String EANCODE 				= "eancode";
		public static final String UNITS 				= "units";
		public static final String CLIENT_ID			= "id_client";
		public static final String LAZZY_CLIENT_ID		= "lazzy_client_id";	
		public static final String STATUS				= "status";
		public static final String CONTACT_NAME			= "contact_name";
		public static final String CONTACT_LAST_NAME	= "contact_last_name";
		public static final String RNC_OR_ID			= "rnc_or_id";

			
	}
	
	
	public static final class Functions{
		public static final String join( String separator,Object...objects ){
			StringBuilder sb = new StringBuilder( "" );
			
			if( null != objects ){
				String token = "";
				for( Object item : objects ){
					sb.append( token ).append( item.toString() );
					token = separator;
				}					
			}
			return sb.toString();
		}		
		
		public static final String join( String separator, Collection< String > objects ){
			return join( separator, objects.toArray() );
		}
	}
}
