package com.lynx.sales.bundle.http.request;

import java.util.HashMap;
import java.util.Map;


/**
 * 
 * <p>
 * After loading the default provider, the provider offers this service
 * are recorded in a {@link ServiceLocator} and then the application may be able 
 * to get these services as {@link ServiceStub} where {@link Requestor} is able to understand
 * </p>
 * 
 * <p>
 * 	ServiceStub login = ServiceLocator.lookup( ServiceLocator.LOGIN );
 *  Requestor requestor = new RequestorImp( context, new PhaseHandler(){
 *  ...
 *  });
 *  requestor.request( login,params);
 * </p>
 * 
 * @see ServiceStub
 * @see Requestor
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public final class ServiceLocator {
			
	private static Map< ServiceName,ServiceStub > services;
	
	static{
		services = new HashMap<ServiceName, ServiceStub>();
		ServiceInjector.injectServices(services);
	}
	
	
	public static ServiceStub lookup( ServiceName name ){
		return services.get( name );
	}	
}
