package com.lynx.sales.bundle.http.request;

/**
 * Plain response returned by the fetch process on {@link Requestor}
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public class ResponseStub {

	private String response;
	
	public ResponseStub( String response ){
		this.response = response;
	}
	
	public String getResponse(){
		return response;
	}
	
	@Override
	public String toString() {
		return response;
	}
}
