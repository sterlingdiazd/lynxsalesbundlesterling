package com.lynx.sales.bundle.http.request;

/**
 * 
 * @author Jansel
 *
 */
public class ServiceStub {
	
	private String url;
	private ParamResolver parser;
	
	
	ServiceStub( String url,ParamResolver parsable ){
		this.url =  url;
		this.parser = parsable;
	}
		
	public String getUrl(){
		return url;
	}
	
	
	public String resolveParams( String...params ){
		if( parser != null )
			return parser.resolveParams(params);
		return "";
	}
	
	
	public static interface ParamResolver{
		public abstract String resolveParams( String...params );
	}
}
	
