package com.lynx.sales.bundle.http.request;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.graphics.SumPathEffect;
import android.util.Log;

import com.lynx.sales.bundle.R;
import com.lynx.sales.bundle.app.LynxApplication;
import com.lynx.sales.bundle.dto.model.ScheduleDataModel;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.testflightapp.lib.TestFlight;

/**
 * Nunca hacer esto
 */
public class SynchronizeRequest extends Requestor {

	private static final String TAG = SynchronizeRequest.class.getSimpleName();
	public static Map<String, SynchronizeRequestDelegate<String>> delegates = new HashMap();
	private Activity activity;
	private boolean isReSync;
	private ScheduleJob job;
	/*
	static {
		delegates = new HashMap<String, SynchronizeRequestDelegate<String>>();
	}
	*/
	public SynchronizeRequest(Activity activity, PhaseHandler handler) {
		super(activity, handler);
		this.activity = activity;
	}

	public SynchronizeRequest(Activity activity, boolean isReSync,
			PhaseHandler handler) {
		this(activity, handler);
		this.isReSync = isReSync;
	}

	public SynchronizeRequest(Activity activity, boolean isReSync, ScheduleJob job,
			PhaseHandler handler) {
		this(activity, handler);
		this.isReSync = isReSync;
		this.job = job;
	}
	
	@Override
	protected ResponseStub handleRequest(ServiceStub stub, String... params)
			throws ContextException {
		orchestrateAllService(params);
		return null;
	}

	public static void registerDelegate(SynchronizeRequestDelegate<String> delegate) 
	{
		delegates.put(delegate.getClass().getCanonicalName(), delegate);
	}

	private void orchestrateAllService(String... params) throws ContextException 
	{
		if (isReSync && job != null) 
		{
			submitJobs(job);
		}
		else if (isReSync) 
		{
			submitJobs();
		}
		else 
		{
			//cleanAllData();
			for (SynchronizeRequestDelegate<String> delegate : delegates.values())
				delegate.performSynchronization(handler, params);
		}

	}

	private String submitNewClient(ClientEntity client) throws ContextException {
		Log.e(TAG, "Submiting new clients...");

		if (null != handler)
			handler.onFeedback("Submiting new clients...");

		LynxApplication app = (LynxApplication) activity.getApplication();
		String strUrl = null;
		try {
			strUrl = "http://lynxsalesdb.somee.com/API/SERVICES/CUSTOMERS?action=4"
					+ "&sociedad="
					+ client.getOrganizacion()
					+ "&cedula="
					+ client.getRNC()
					+ "&location="
					+ URLEncoder.encode(client.getLocation(), "UTF-8")
					+ "&address="
					+ client.getAddress1()
					+ "&phone="
					+ client.getPhone()
					+ "&interlocutor="
					+ client.getInterlocutor()
					+ "&contactname="
					+ client.getContactName()
					+ "&contactlastname="
					+ client.getContactLastName() + "&seller=" + app.getUser().getId();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		URI url = URI.create(strUrl);
		HttpGet get = new HttpGet(url);
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response = null;

		String remoteClientID = "";

		try {
			response = httpClient.execute(get);
			if (response == null) {
				TestFlight.log("Missed [ " + strUrl + " ] to be synchronized");
			} else {
				String jsonResponse = EntityUtils
						.toString(response.getEntity());
				JSONObject jsonObject = new JSONObject(jsonResponse);

				if (jsonObject.getString("type").equalsIgnoreCase("S")
						&& jsonObject.getString("message").equalsIgnoreCase(
								"014")) {
					remoteClientID = jsonObject.getString("doc");
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			TestFlight.sendCrash(1L, "Error Trying to get synchronized",
					ex.toString());
			TestFlight.sendsCrashes();
			throw new ContextException(ex.toString());
		}

		return remoteClientID;
	}

	private void submitJobs() throws ContextException {
		

		List<ScheduleJob> jobs = ScheduleDataModel.getShedulesJobs(getContext());

		for (int i = 0; jobs.size() > i; ++i) 
		{
			ScheduleJob job = jobs.get(i);	
			submitJobs(job);
		}
	}

	private void submitJobs(ScheduleJob job) throws ContextException {
		
		if (null != handler)
			handler.onFeedback(activity.getResources().getString(R.string.submiting_pending));

			if (job.getStatus() == ScheduleJob.Status.PENDING) {
				Log.e(TAG,activity.getResources().getString(R.string.submiting_pending) + ": " + job.getQuery());

				String strUrl = job.getQuery();
				
				
				int startURl = strUrl.indexOf("articles=") + 9;
				int endURL = strUrl.indexOf("&code");
				
				String urlFirstPart = strUrl.substring(0, startURl);
				String urlLastPart = strUrl.substring(endURL , strUrl.length());
				
				String articles = strUrl.substring(startURl, endURL);
				String [] articlesArray = articles.split(";");
				String correctArticles = "";
				
				
				for(int x = 0; x < articlesArray.length; x++)
				{
					String currentArticle = articlesArray[x];
					
					int start = currentArticle.indexOf("De:");
					int end = currentArticle.length();
					
					String correctArticle = null;
					if(start != -1)
					{
						/*
						String initString = currentArticle.substring(0, start);
						
						String subS = currentArticle.substring(start+1, end);
						
						int start2 = subS.indexOf(":");
						String subS2 = subS.substring(start2 + 1, subS.length());
						
						int start3 = subS2 .indexOf(":");
						String subS3 = subS2.substring(start3 + 1, subS2.length());
						
						int start4 = subS3.indexOf(":");
						String subS4 = subS3.substring(start4 + 1, subS3.length());
						
						int start5 = subS4.indexOf(":");
						String subS5 = subS4.substring(start5  + 1, subS4.length());
						
						*/
						correctArticle  = currentArticle /*initString + subS5 */ ;
					}
					else
					{
						int startNoOffer = currentArticle.indexOf("null");
						int endNoOffer = currentArticle.length();
						
						if(startNoOffer != -1)
						{
							
						}
						/*
						String initNoOffer = currentArticle.substring(0, startNoOffer);
						String subSNoOffer = currentArticle.substring(startNoOffer, endNoOffer);
						
						int startNoOffer2 = subSNoOffer.indexOf(":");
						String subSNoOffer2 = subSNoOffer.substring(startNoOffer2 + 1, subSNoOffer.length());
						
						int startNoOffer3 = subSNoOffer2.indexOf(":");
						String subSNoOffer3 = subSNoOffer2.substring(startNoOffer3 + 1, subSNoOffer2.length());
						*/
						
						correctArticle = /* initNoOffer + subSNoOffer3 */ currentArticle;
					}
					
					correctArticles += correctArticle + ";";
				}
				
				String fullString = urlFirstPart + correctArticles + urlLastPart;
				//System.out.println(fullString );
				ClientEntity client = CommonUtils.getClientByQuery(activity, fullString);

				if (client.getStatus().equalsIgnoreCase("CREADO")) {
					String externalID = submitNewClient(client);
					client.setKunnr(externalID);
					strUrl = CommonUtils.setClientOnQuery(strUrl,
							client.getKunnr());
				}
				Log.e("url", fullString);
				
				
				URI url = URI.create(fullString );
				HttpGet get = new HttpGet(url);
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = null;

				try {
					response = httpClient.execute(get);
					if (response == null) {
						TestFlight.log("Missed [ " + strUrl
								+ " ] to be synchronized");
					} else {
						String jsonResponse = EntityUtils.toString(response
								.getEntity());
						String status = "";
						
						JSONObject orderResponse = new JSONObject(jsonResponse);
						String type = orderResponse.getString("type");
						
						if(type.equalsIgnoreCase("S") && job.getGuardLimit().equalsIgnoreCase("true")) 
						{
							status = ScheduleJob.Status.PROCCESED.toString();
						} 
						else if (type.equalsIgnoreCase("S") && job.getGuardLimit().equalsIgnoreCase("false")) 
						{
							status = ScheduleJob.Status.WARNING.toString();
						} 
						else if (type.equalsIgnoreCase("E")) 
						{
							status = ScheduleJob.Status.REJECTED.toString();
						}
						else if (jsonResponse.contains("<html>")) 
						{
							status = ScheduleJob.Status.REJECTED.toString();
							jsonResponse = "Hubo un error accediendo al servicio o mostrando la información";
						} 
						else 
						{
							status = ScheduleJob.Status.REJECTED.toString();
						}

						ScheduleDataModel
								.registerResponseForSchedule(getContext(),
										job.getId(), status, jsonResponse);
					}

				} catch (Exception ex) {
					ex.printStackTrace();
					TestFlight.sendCrash(1L, "Error Trying to get synchronized", ex.toString());
					TestFlight.sendsCrashes();
					throw new ContextException(ex.toString());
				}
				
			}
		
	}

	private void cleanAllData() {
		if (null != handler)
			handler.onFeedback(activity.getResources().getString(
					R.string.cleaning_data));
		ScheduleDataModel.onUpgradeDatabase(getContext());
	}

}
