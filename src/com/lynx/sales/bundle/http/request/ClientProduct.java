package com.lynx.sales.bundle.http.request;

import java.io.Serializable;
import java.util.List;

import com.lynx.sales.bundle.entities.ProductEntity;

@SuppressWarnings("serial")
public class ClientProduct implements Serializable{

	private String clientId;
	private List<ProductEntity> products;

	public ClientProduct( String clientId,List< ProductEntity > products ) {
		this.clientId = clientId;
		this.products = products;
	}

	public String getClientId() {
		return clientId;
	}

	public List<ProductEntity> getProducts() {
		return products;
	}
}
