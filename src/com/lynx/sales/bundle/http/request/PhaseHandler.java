package com.lynx.sales.bundle.http.request;

/**
 * <p>
 * When {@link Requestor} is created and {@link Requestor#request(ServiceStub, String...)} is called,
 * the caller must provide {@link PhaseHandler} to track all possible state of the request process.
 * </p>
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public interface PhaseHandler {
	
	/**
	 * Request process ins initiated, and the caller is notified throught this method
	 */
	public abstract void onBegan( );
	
	/**
	 * If some error was raised during the request process, the observer is 
	 * notified via this method, passing cause as {@link ErrorStub} instance
	 * 
	 * @param stub
	 */
	public abstract void onError( ErrorStub stub );
	
	/**
	 * Called after all process is completed
	 * @param response as {@link ResponseStub} object
	 */
	public abstract void onCompleted( ResponseStub response );
	
	/**
	 * Give a feedback while the process is in execution.
	 * @param feed
	 */
	public abstract void onFeedback( String feed );
	
}
