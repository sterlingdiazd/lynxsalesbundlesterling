package com.lynx.sales.bundle.http.request;

public interface SynchronizeRequestDelegate<T> {
	public abstract void performSynchronization(PhaseHandler handler,T...params) throws ContextException;	
}
