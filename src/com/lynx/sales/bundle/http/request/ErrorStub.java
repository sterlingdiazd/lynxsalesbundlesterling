package com.lynx.sales.bundle.http.request;


/**
 * <p>
 * 	Stub that represent error that was raised during {@link Requestor#request(ServiceStub, String...)} 
 *  execution.
 * </p>
 * <p>
 * 	{@link ErrorStub} represent plain {@link ContextException} raised by {@link Requestor#handleRequest(ServiceStub, String...)}
 *  on subclass,  the message is returned on {@link ErrorStub#toString()} method.
 * </p>
 * 
 * @see Requestor
 * @see ContextException
 * 
 * @since 1.0
 * 
 * @author Jansel
 *
 */
public class ErrorStub {

	private String reason;
	
	ErrorStub( String reason ){
		this.reason = reason;		
	}
	
	public String getReason(){
		return this.reason;
	}
	
	@Override
	public String toString() {
		return this.reason;
	}
}
