package com.lynx.sales.bundle.http.request;

import java.util.ArrayList;

import static com.lynx.sales.bundle.dto.model.Constants.*;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.lynx.sales.bundle.entities.CenterEntity;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ClientInfo;
import com.lynx.sales.bundle.entities.Company;
import com.lynx.sales.bundle.entities.DocTypeEntity;
import com.lynx.sales.bundle.entities.ItineraryEntity;
import com.lynx.sales.bundle.entities.LoginResult;
import com.lynx.sales.bundle.entities.RouteEntity;
import com.lynx.sales.bundle.entities.SalesArea;
import com.lynx.sales.bundle.entities.UserEntity;
import com.testflightapp.lib.TestFlight;

public class ResponseBinder {
	
	public static LoginResult bindLogin( ResponseStub stub ){		
		boolean state = false;
		String id=null;
		String name = null;
		String organization = null;
	
		if( stub != null && stub.getResponse() != null ){
			
			try{
				JSONObject res = new JSONObject( stub.getResponse() );
				String code = res.getString( "type" );
				if( code.contains( "S" ) ){
					state = true;			
					
					JSONObject seller = res.getJSONObject( "seller" );
					
					id = seller.getString( "id" );							
					name = seller.getString( "bezei" ) ;					
					organization = seller.getString( "vkbur" ) ;
					
				}				
			}catch( JSONException je ){				
				TestFlight.log( "Error parsing json for loginBind: Reason: "+je.toString() );
				je.printStackTrace();
			}
		}
		return new LoginResult( state,id,name,organization );	
	}
	
	public static boolean bindDelete( ResponseStub stub ){		
		boolean state = false;

	
		if( stub != null && stub.getResponse() != null ){
			
			try{
				JSONObject res = new JSONObject( stub.getResponse() );
				String code = res.getString( "type" );
				if( code.contains( "S" ) ){
					state = true;							
				}				
			}catch( JSONException je ){				
				TestFlight.log( "Error parsing json for loginBind: Reason: "+je.toString() );
				je.printStackTrace();
			}
		}
		return state;	
	}
	
	public static UserEntity bindUser( ResponseStub stub ){
		UserEntity user = new UserEntity();
		if( !TextUtils.isEmpty( stub.getResponse() ) ){
			try{				
				JSONObject jsonObject =  new JSONObject( stub.getResponse() );
				user.setId( 		jsonObject.getString( "id" ) );
				user.setUser( 		jsonObject.getString( "usuario" ) );
				user.setName( 		jsonObject.getString( "bezei" ) );
				user.setSalesOrganization( jsonObject.getString( "vkbur" ) );
				
				JSONArray jsonDocTypes =	jsonObject.getJSONArray( "docTypes" );
				List<DocTypeEntity> doctypes = new ArrayList<DocTypeEntity>();
				
				for( int i=0;i< jsonDocTypes.length(); ++i )
				{
					JSONObject jsonDocType = jsonDocTypes.getJSONObject( i );
					
					DocTypeEntity docTypeEntity = new DocTypeEntity();
					docTypeEntity.setDocType( jsonDocType.getString( "docType" ) );
					docTypeEntity.setDescription( jsonDocType.getString( "description" ) );
					doctypes.add( docTypeEntity );
				}
				user.setDocTypes(doctypes);

				JSONArray jsonCenters = jsonObject.getJSONArray( "centers" );
				List< CenterEntity > centers = new ArrayList<CenterEntity>();
				
				for( int i=0;i< jsonCenters.length(); ++i ){
					JSONObject jsonCenter = jsonCenters.getJSONObject( i );
					
					CenterEntity center = new CenterEntity();
					center.setName( jsonCenter.getString( "center" ) );
					center.setDescription( jsonCenter.getString( "description" ) );
					center.setStore( jsonCenter.getString( "store" ) );
					center.setDocType( jsonCenter.getString( "docType" ) );
					centers.add( center );
				}
				
				user.setCenters( centers );
				user.setCel(jsonObject.getString( "phone1" ));
				user.setTel(jsonObject.getString( "phone2" ));
				
			}catch( JSONException ex ){
				TestFlight.log( "Error parsing json for userBind: Reason: "+ex.toString() );	
				ex.printStackTrace();
			}
		}			
		return user;
	}
	
	public static final Company bindCompany( String response )
	{
		Company company = new Company();
		
		if(!TextUtils.isEmpty( response ) )
		{
			try
			{
				JSONObject jsonResponse = new JSONObject( response );
				company.setId(jsonResponse.getString(ID) );
				company.setName(jsonResponse.getString(NAME) );
				company.setAddress(  jsonResponse.getString(ADDRESS) );
				company.setPhone(jsonResponse.getString(PHONE) );
				company.setRnc( jsonResponse.getString(RNC) );
				company.setFax(jsonResponse.getString(FAX) );
						
			}catch( JSONException ex ){
				TestFlight.log( "Error parsing json for bindClientProducts: Reason: "+ex.toString() );
				ex.printStackTrace();
			}
		}
		return company;
	}
	
	
	public static ItineraryEntity bindItinerary( String response, PhaseHandler handler ){				
		
		ItineraryEntity entItin = new ItineraryEntity();
		
		try{
			JSONObject data = new JSONObject( response );
			JSONArray  itineraries = data.getJSONArray( "itineraries" );
			for( int i=0;i<itineraries.length();++i ){
				JSONObject itineraty = itineraries.getJSONObject( i );
				
				entItin.setId( itineraty.getString( "id"  ) );
				entItin.setCreationDate( itineraty.getString( "creation_date"  ) );
				entItin.setDateFrom( itineraty.getString( "date_from"  ) );
				entItin.setDateTo( itineraty.getString( "date_to"  ) );
				entItin.setDaysOfWeek( itineraty.getString( "day_of_week"  ) );
				entItin.setCreatedBy( itineraty.getString( "created_by"  ) );
				entItin.setModifiedBy( itineraty.getString( "modified_by"  ) );
				entItin.setModifiedHour(  itineraty.getString( "modified_hour"  ) );
				entItin.setDescription( itineraty.getString( "description"  ) );
				
				JSONArray routes = itineraty.getJSONArray( "clients_routes" );
				JSONObject singleRoute = routes.getJSONObject(0);
				
				JSONArray clients = singleRoute.getJSONArray( "clients" );
				List< RouteEntity >  entRoutes = new ArrayList< RouteEntity >();
				
				for( int j=0;j<clients.length();++j ){
					JSONObject jsonClient = clients.getJSONObject(j);										
	
					RouteEntity entRoute = new RouteEntity();	
						
					entRoute.setIdRoute( singleRoute.getString( "route_id" ) );
					entRoute.setCreatedBy( singleRoute.getString( "created_by" ) );
					entRoute.setCreatedDate( singleRoute.getString( "created_date" ) );
					entRoute.setLazzyClientId( jsonClient.getString( "id" ) );
					entRoute.setLazzyClientName( jsonClient.getString( "name" ) );
					entRoutes.add(entRoute);									
				}					
				entItin.setRoutes(entRoutes);
			}
			
		}catch( JSONException ex ){
			
			TestFlight.log( "Error parsing json for itineraryBind: Reason: "+ex.toString() );
		/*	if(handler!=null)
				handler.onError(new ErrorStub(ex.getCause().toString()));*/
			ex.printStackTrace();
			return null;
			
		}	
				
		return  entItin ;
	}	

	
	
	
	public static final ClientEntity bindClient( String value ){			
		
		ClientEntity client = new ClientEntity();
		if( !TextUtils.isEmpty(value) ){
			try{
				//JSONObject response = new JSONObject( value );
				JSONArray  jsonClients = new JSONArray( value );
				if( jsonClients.length() > 0 ){
					
					JSONObject jsonClient = jsonClients.getJSONObject( 0 );
					
					
					client.setKunnr( jsonClient.getString( 		KUNNR) );
					client.setName( jsonClient.getString( 		NAME)  );
					client.setRNC( jsonClient.getString( 			RNC ) );
					client.setInterlocutor( jsonClient.getString( 	INTERLOCUTOR  ) );
					client.setPayment_cond( jsonClient.getString( 	PAYMENT_COND  ) );
					client.setContactName( jsonClient.getString( 	CONTACT_NAME  ) );
					client.setContactName( jsonClient.getString( 	CONTACT_LAST_NAME  ) );
					client.setAddress1( jsonClient.getString( 	ADDRESS_1 )  );
					client.setAddress2( jsonClient.getString( 	ADDRESS_2 )  );
					client.setLocation( jsonClient.getString( 	LOCATION )  );
					client.setPhone( jsonClient.getString( 		PHONE )  );
					client.setCredit( jsonClient.getString( 		CREDIT ) );
					client.setOpen( jsonClient.getString( 		OPEN ) );
					client.setAvailable( jsonClient.getString( 	AVAILABLE ) );
					client.setTodayDate( jsonClient.getString( 	TODAY_DATE)  );
					client.setRamo( jsonClient.getString( 		RAMO )  );
					client.setCanal( jsonClient.getString( 		CANAL)  );
					client.setSector( jsonClient.getString( 		SECTOR  )  );
					client.setOrganizacion( jsonClient.getString( ORGANIZACION)  );
					client.setStatus( jsonClient.getString( 		STATUS)  );
				}
			}catch( JSONException ex ){
				TestFlight.log( "Error parsing json for client: Reason: "+ex.toString() );	
				ex.printStackTrace();
			}
		}
		return client;
	}
	
	
	
	/*
	public static final List< ClientProduct > bindClientProducts( String response ){
		List< ClientProduct > clientProducts = new ArrayList<ClientProduct>();
		
		
		if(!TextUtils.isEmpty( response ) ){
			try{
				JSONObject jsonResponse = new JSONObject( response );
				
					String clientId = jsonResponse.getString( "id" );
					List< ProductEntity > products = new ArrayList< ProductEntity >();
					
					JSONArray jsonProducts = jsonResponse.getJSONArray( "products" );
					for( int j=0;j<jsonProducts.length();++j ){
						JSONObject jsonProduct = jsonProducts.getJSONObject(j);
						
						ProductEntity product = new ProductEntity();
						product.setId( jsonProduct.getString( "id" ) );
						product.setName( jsonProduct.getString( "description" ) );
						product.setThumb( jsonProduct.getString( "thumb" ) );
						product.setSalesOrganization( jsonProduct.getString( "sales_organization" ) );
						product.setDistributionChannel( jsonProduct.getString( "distribution_channel" ) );
						product.setSector( jsonProduct.getString( "sector" ) );
						product.setRamo( jsonProduct.getString( "ramo" ) );
						product.setPrice( jsonProduct.getString( "price" ) );
						product.setSalesUnit( jsonProduct.getString( "sales_unit" ) );
						product.setQuantityInSalesUnit( jsonProduct.getString( "quantity_in_sales_unit" ) );
						product.setUnitBase( jsonProduct.getString( "sales_unit" ) );
						product.setStock( jsonProduct.getString( "stock" ) );
						product.setCredit( jsonProduct.getString( "credit" ) );
						product.setOffer( jsonProduct.getString( "offer" ) );
						product.setCenter( jsonProduct.getString( "center" ) );
						
						 //Comment out this part, when the response come out.  
						
						
						
						products.add(product);
					}
					
					clientProducts.add( new ClientProduct(clientId, products));
										
			}catch( JSONException ex ){
				TestFlight.log( "Error parsing json for bindClientProducts: Reason: "+ex.toString() );
				ex.printStackTrace();
			}
		}
		return clientProducts;
	}
	
	public static final List< ClientProduct > bindMaterials( String response )
	{
		List< ClientProduct > clientProducts = new ArrayList<ClientProduct>();
		
		if(!TextUtils.isEmpty( response ) ){
			try{
				JSONObject jsonResponse = new JSONObject( response );
				
					String clientId = jsonResponse.getString( "id" );
					List< ProductEntity > products = new ArrayList< ProductEntity >();
					
					JSONArray jsonProducts = jsonResponse.getJSONArray( "products" );
					for( int j=0;j<jsonProducts.length();++j ){
						JSONObject jsonProduct = jsonProducts.getJSONObject(j);
						
						ProductEntity product = new ProductEntity();
						product.setId( jsonProduct.getString( "id" ) );
						product.setName( jsonProduct.getString( "description" ) );
						product.setThumb( jsonProduct.getString( "thumb" ) );
						product.setSalesOrganization( jsonProduct.getString( "sales_organization" ) );
						product.setDistributionChannel( jsonProduct.getString( "distribution_channel" ) );
						product.setSector( jsonProduct.getString( "sector" ) );
						product.setRamo( jsonProduct.getString( "ramo" ) );
						product.setPrice( jsonProduct.getString( "price" ) );
						product.setSalesUnit( jsonProduct.getString( "sales_unit" ) );
						product.setQuantityInSalesUnit( jsonProduct.getString( "quantity_in_sales_unit" ) );
						product.setUnitBase( jsonProduct.getString( "sales_unit" ) );
						product.setStock( jsonProduct.getString( "stock" ) );
						product.setCredit( jsonProduct.getString( "credit" ) );
						product.setOffer( jsonProduct.getString( "offer" ) );
						product.setCenter( jsonProduct.getString( "center" ) );
						
						// Comment out this part, when the response come out.  
						 
						
						
						products.add(product);
					}
					
					clientProducts.add( new ClientProduct(clientId, products));
										
			}catch( JSONException ex ){
				TestFlight.log( "Error parsing json for bindClientProducts: Reason: "+ex.toString() );
				ex.printStackTrace();
			}
		}
		return clientProducts;
	}
	*/
	public static final ClientInfo bindClientInfo( String reesponse ){		
		ClientInfo entity = new ClientInfo();	
		
		
		if( ! TextUtils.isEmpty( reesponse ) ){			
			try {				
				JSONObject jsonResposne = new JSONObject( reesponse );
				JSONObject result = jsonResposne.getJSONObject( "result" );
				
				JSONArray societies = result.getJSONArray( "societies" );
				String soc[] = new String[ societies.length() ];
				for( int i=0;i<societies.length();++ i ){
					soc[ i ] = societies.getString(i);
				}				
				
				entity.setSocieties(soc);
				
				List< SalesArea > areas = new ArrayList<SalesArea>();
				
				JSONArray salesAreas = result.getJSONArray( "sales_area" );
				for( int i=0;i<salesAreas.length();++i ){
					JSONObject salesArea = salesAreas.getJSONObject( i );
					
					SalesArea area = new SalesArea();
					area.setSalesOrg( salesArea.getString( "sales_org" ) );
					area.setDistChannel( salesArea.getString( "dist_channel" ) );
					area.setPaymentCond( salesArea.getString( "payment_cond" ) );
					
					//Log.e( "*************", "Sector: "+salesArea.getString( "sector" ) );
					area.setSector( salesArea.getString( "sector" ) );
					
					JSONArray interlocutors = salesArea.getJSONArray( "interlocutors" );
					String inter[] = new String[ interlocutors.length() ];
					
					for( int j=0;j<interlocutors.length();++j ){
						inter[ j ]= interlocutors.getString( j );
					}					
					
					area.setInterlocutors(inter);					
					areas.add( area );
				}
				
				entity.setSalesArea(areas);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}			
		}
		return entity;
	}
}
