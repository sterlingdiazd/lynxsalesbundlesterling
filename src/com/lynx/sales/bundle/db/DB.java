package com.lynx.sales.bundle.db;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public final class DB {
	
	/***
	 * 
	 * Configuration variables for Database Creation.
	 *
	 */
	public static final class Config{
		public static final String 	DB_NAME		=  "lynx.sales.db";
		public static final int		DB_VERSION  =  101; 
	}
	
}
