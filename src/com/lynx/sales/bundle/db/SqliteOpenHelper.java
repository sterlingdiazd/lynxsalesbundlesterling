package com.lynx.sales.bundle.db;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.lynx.sales.bundle.R;
import com.lynx.sales.bundle.dto.model.ArchiveDataModel;
import com.lynx.sales.bundle.dto.model.ClientDataModel;
import com.lynx.sales.bundle.dto.model.CompanyDataModel;
import com.lynx.sales.bundle.dto.model.ItineraryDataModel;
import com.lynx.sales.bundle.dto.model.ProductDataModel;
import com.lynx.sales.bundle.dto.model.ScheduleDataModel;
import com.lynx.sales.bundle.dto.model.UserDataModel;
import com.lynx.sales.bundle.dto.model.UserRetrieval;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.entities.Archive;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.Company;
import com.lynx.sales.bundle.entities.ItineraryEntity;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynx.sales.bundle.xml.ContextParser;
import com.lynx.sales.bundle.xml.SingleQueryParser;

/**
 * 
 * @author Jansel R. Abreu (Vanwolf)
 *
 */
public class SqliteOpenHelper extends SQLiteOpenHelper
{

    private Context context;

    public SqliteOpenHelper(Context context, String name, CursorFactory factory, int version)
    {
	super(context, name, factory, version);
	this.context = context;
    }

    public SqliteOpenHelper(Context cxt, CursorFactory factory)
    {
	this(cxt, DB.Config.DB_NAME, factory, DB.Config.DB_VERSION);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite
     * .SQLiteDatabase)
     * 
     * This method perform Database creation. the queries are retrieved from
     * .xml file called cqueries_bundles and parsed by SimpleQueryParser placed
     * in package com.silm.xml.
     */
    @Override
    public void onCreate(SQLiteDatabase db)
    {
	Database.readable = db;
	Database.writable = db;

	try
	{
	    ContextParser parser = new SingleQueryParser(context, R.raw.cqueries_bundles);
	    List<Query> set = parser.parse();

	    for (Query item : set)
	    {
		String statementCreate = item.getStatement();
		
		int start = statementCreate.indexOf("EXISTS ") + 7;
		int end = statementCreate.indexOf(")");
		String table_name = statementCreate.substring(start, end).trim();
		
		if (table_name.contains("sync_log"))
		{
		    db.execSQL(statementCreate);
		    db.execSQL("insert into sync_log values  (null, null, 'itineraries', 'unsync');");
		    db.execSQL("insert into sync_log values  (null, null, 'clients', 'unsync');");
		    db.execSQL("insert into sync_log values  (null, null, 'products', 'unsync');");
		    db.execSQL("insert into sync_log values  (null, null, 'conditions', 'unsync');");
		} 
		else
		{
		    db.execSQL(statementCreate); 
		}
	    }

	} catch (IOException ioe)
	{
	    ioe.printStackTrace();
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.
     * sqlite.SQLiteDatabase, int, int)
     * 
     * This method perform Database restore. the queries are retrieved from .xml
     * file called dqueries_bundles and parsed by SimpleQueryParser placed in
     * package com.silm.xml.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int version, int arg2)
    {

	Database.readable = db;
	Database.writable = db;

	UserEntity userEntity = null;

	List<ItineraryEntity> itineraries = null;
	try
	{
	    ContextParser parserDrop = new SingleQueryParser(context, R.raw.dqueries_bundles);
	    List<Query> setDrop = parserDrop.parse();

	    ContextParser parserCreate = new SingleQueryParser(context, R.raw.cqueries_bundles);
	    List<Query> setCreate = parserCreate.parse();

	    for (int x = 0; x < setDrop.size(); x++)
	    {
		String statementDrop = setDrop.get(x).getStatement();

		int start = statementDrop.indexOf("EXISTS ") + 7;
		int end = statementDrop.indexOf(";");
		String table_name = statementDrop.substring(start, end).trim();

		if (table_name.contains("sync_log"))
		{
		    db.execSQL(statementDrop);
		    db.execSQL("insert into sync_log values  (null, null, 'itineraries', 'unsync');");
		    db.execSQL("insert into sync_log values  (null, null, 'clients', 'unsync');");
		    db.execSQL("insert into sync_log values  (null, null, 'products', 'unsync');");
		    db.execSQL("insert into sync_log values  (null, null, 'conditions', 'unsync');");
		}

		/*
		 * 
		 * 
		 * if(table_name.contains("company")) { Company company =
		 * CompanyDataModel.getCompany(context);
		 * db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * CompanyDataModel.saveCompany(context, company); } else
		 * if(table_name.contains("doctypes")) {
		 * db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * 
		 * } else if(table_name.contains("users")) { userEntity =
		 * UserRetrieval.getUser(context); db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * 
		 * } else if(table_name.contains("centers")) {
		 * db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * UserDataModel.saveUser(context, userEntity ); } else
		 * if(table_name.contains("clients")) { List<ClientEntity>
		 * clients = ClientDataModel.getClients(context);
		 * db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * ClientDataModel.saveClient(context, clients); } else
		 * if(table_name.contains("itineraries")) { itineraries =
		 * ItineraryDataModel.getSellerItineraries(userEntity.getId(),
		 * context); db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * 
		 * } else if(table_name.contains("routes")) {
		 * db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * ItineraryDataModel.saveItinerary(context, userEntity.getId(),
		 * itineraries); }
		 * 
		 * else if(table_name.contains("products")) {
		 * 
		 * List<ProductEntity> products =
		 * ProductDataModel.getAllProducts(context);
		 * db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * ProductDataModel.bulkSaveAllProducts(context, products); }
		 * 
		 * else if(table_name.contains("units")) {
		 * db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * 
		 * } else if(table_name.contains("pending_jobs_queue")) {
		 * List<ScheduleJob> jobs =
		 * ScheduleDataModel.getShedulesJobs(context);
		 * db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * ScheduleDataModel.saveScheduleJobs(context, jobs); } else
		 * if(table_name.contains("archives")) { List<Archive> archives
		 * = ArchiveDataModel.getArchives(context);
		 * db.execSQL(statementDrop);
		 * db.execSQL(setCreate.get(x).getStatement());
		 * ArchiveDataModel.saveArchives(context, archives); } else
		 */ if (table_name.contains("conditions"))
		{
		    db.execSQL(statementDrop);
		    db.execSQL(setCreate.get(x).getStatement());
		} /*
		   * else if(table_name.contains("preferences")) {
		   * db.execSQL(statementDrop);
		   * db.execSQL(setCreate.get(x).getStatement()); } else {
		   * Log.e("Another table", "Statement: " +
		   * setCreate.get(x).getStatement());
		   * 
		   * }
		   */
	    }
	} catch (IOException ioe)
	{
	    ioe.printStackTrace();
	}

    }
}
