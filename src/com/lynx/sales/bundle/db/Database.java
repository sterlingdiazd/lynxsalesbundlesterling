package com.lynx.sales.bundle.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
/**
 * <p>
 * 	Rarely you must interact with database directly, instead 
 *  you need to register your <code>content provider</code>
 *  and handle all data base operation inside that
 * </p>
 * <p>
 * 	As you need to create table to support you content provider,
 *  you need to provider <code>create</code> and <code>delete</code>
 *  statements for your database tables, that statements are placed
 *  in files called <code>cqueries_bundles.xml</code>  and <code>dqueries_bundles.xml</code>
 * </p>
 * 
 * @since 1.0
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public final class Database {

	public static SQLiteDatabase writable;
	public static SQLiteDatabase readable;
	public static SqliteOpenHelper helper;
	
	
	/**
	 * 
	 * @param context  
	 * @param name
	 * @param factory
	 * @param version
	 * @return Valid SqliteDatabase  for write access if exception does'nt occurs.
	 */
	public static final SQLiteDatabase getWritableDatabase( Context context){
		synchronized( Database.class ){
			if( helper == null )			
				helper = new SqliteOpenHelper( context, null);
		}
		return writable == null ? writable = helper.getWritableDatabase() : writable;
	}
	   
	
	/**
	 * 
	 * @param context
	 * @param name
	 * @param factory
	 * @param version
	 * @return Valid SqliteDatabase  for read access if exception does'nt occurs.
	 */
	public static final SQLiteDatabase getReadableDatabase( Context context ){
		synchronized( Database.class ){
			if( helper == null )			
				helper = new SqliteOpenHelper(context, null);
		}
		return readable == null ? readable = helper.getReadableDatabase() : readable;	
	}
}
