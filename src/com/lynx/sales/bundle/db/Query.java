package com.lynx.sales.bundle.db;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class Query {

	private String statement;
	
	public Query(){		
	}
	
	public Query( String statement ){
		this.statement = statement;
	}
	
	public String getStatement(){
		return this.statement;
	}	
}
